const version = "0.0.1";
const cacheName = `goodboy-pwa-${version}`;
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll([
        `/`,
        `/index.html`,
        "/assets/animate/candy/images/candy_tutorial_bg_1.jpg",
        "/assets/animate/candy/images/candy_tutorial_bg_2.jpg",
        "/assets/animate/candy/images/candy_tutorial_bg_3.jpg",
        "/assets/animate/candy/images/tutorial_candy_game_atlas_1.json",
        "/assets/font/font_hud_cornerstone.png",
        "/assets/font/score_font.png",
        "/assets/image/common/bridge.jpg",
        "/assets/image/common/dash.png",
        "/assets/image/common/gift-texture.png",
        "/assets/image/common/pyramid-down.jpg",
        "/assets/image/common/pyramid-heart.jpg",
        "/assets/image/common/pyramid-top.jpg",
        "/assets/image/common/pyramid-top.png",
        "/assets/image/common/rainbow.jpeg",
        "/assets/image/common/rainbow.png",
        "/assets/image/common/Room_BG.jpg",
        "/assets/image/device/brim.jpg",
        "/assets/image/device/rotate.jpg",
        "/assets/image/displace.jpg",
        "/assets/image/game-ui/game-ui0.json",
        "/assets/image/how_play/how_play0.json",
        "/assets/image/settings/settings0.json",
        "/assets/image/skybox/negx.jpg",
        "/assets/image/skybox/negy.jpg",
        "/assets/image/skybox/negz.jpg",
        "/assets/image/skybox/posx.jpg",
        "/assets/image/skybox/posy.jpg",
        "/assets/image/skybox/posz.jpg",
        "/assets/image/spaceship.jpg",
        "/assets/image/torus1.jpg",
        "/assets/image/torus2.jpg",
        "/assets/image/ui/ui0.json",
        "/assets/image/vignette-health.png",
        "/assets/model/cube.gbo",
        "/assets/model/gift.gbo",
        "/assets/model/particle1.gbo",
        "/assets/model/particle2.gbo",
        "/assets/model/pyramid-down.gbo",
        "/assets/model/pyramid-heart.gbo",
        "/assets/model/pyramid-top.gbo",
        "/assets/model/quad.gbo",
        "/assets/model/spaceship.gbo",
        "/assets/model/torus1.gbo",
        "/assets/model/torus2.gbo",
        "/assets/image/common/standalone/1.png",
        "/assets/image/common/standalone/2.png",
        "/assets/image/common/standalone/3.jpg",
        "/assets/image/preload/loader-bar-empty.png",
        "/assets/image/preload/loader-bar-full.png",
        "/assets/image/preload/loader_fill.png",
        "/assets/image/preload/loader_frame.png",
        "/assets/image/preload/old_browser.jpg",
        "/assets/audio/buttons/game_button.mp3",
        "/assets/audio/buttons/press.mp3",
        "/assets/audio/buttons/roll.mp3",
        "/assets/audio/crash.mp3",
        "/assets/audio/pickup.mp3",
        "/assets/audio/soundtrack.mp3",
        "/assets/audio/woosh.mp3"
      ])
          .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open(cacheName)
      .then(cache => cache.match(event.request, {ignoreSearch: true}))
      .then(response => {
      return response || fetch(event.request);
    })
  );
});
