# GOODBOY PROJECT BOILERPLATE V1.0.0

# Release Notes
[Release Notes](https://bitbucket.org/goodboydigital/fido-template-v2/src/2ca1fd912ddfefc85eb4c3f1cdaac514352a53b6/ReleaseNotes.md?at=develop&fileviewer=file-view-default)

# Prerequire

node version: v7.0.0

npm version: 3.10.8

# Installation

Install all depedencies
```
npm install

```

# Start developement

It starts a developement server and convert all the assets with the task *npm run assets* and after watch all the assets

```
npm start

```

# Build project

```
npm run build

```

## List npm scripts

* npm start
* npm dev
* npm build
* npm deploy
* npm run serve
* npm run serve:dist
* npm run assets
* npm run assets:watch
* npm run audio
* npm run audio:watch
* npm run image
* npm run tiny
* npm run image:watch
* npm run json
* npm run json:watch
* npm run video
* npm run obj
* npm run obj:watch


# audio

## prequire : ffmpeg
```
http://brew.sh/
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew install ffmpeg --with-fdk-aac --with-ffplay --with-freetype
 --with-frei0r --with-libass --with-libvo-aacenc --with-libvorbis
 --with-libvpx --with-opencore-amr --with-openjpeg --with-opus
 --with-rtmpdump --with-schroedinger --with-speex --with-theora --with-tools
```

## scripts

### convert all audio files in the source folder
```
npm run audio
```


### watch audio folder and auto convert the added/changed audio file
```
npm run audio:watch
```


# images

## prerequire : Texture Pack command line tool
https://www.codeandweb.com/texturepacker/documentation

## scripts

### convert/pack all image files in the source folder
```
npm run image
```


### watch image folder and auto convert/pack the image files
```
npm run image:watch
```

### supported folder prefixes :
- {fix} : fixed size for high and low res version
- {manifest} : generate standalone manifest file for this folder
- {tps} : generate a texture pack for this folder


### /!\ Be carefull !

Texture Packer do not provide an option to ignore subfolders so for all {tps} folder they can't have subdirectories

#### wrong structure
* main{tps}
  * [FOLDER]
  * imageFile

OR
* main{tps}
  * [FOLDER]
     * [FOLDER]
     * imageFile
#### good structure

* main{tps}
  * [FOLDER]
  * [FOLDER]
  * [FOLDER]

OR

* main{tps}
  * imageFile
  * imageFile

### manifests
Output manifests located in the `./src/scripts/app/manifests/`. Assets from the folders without `{manifest}` prefix goes into `manifest-all-assets.js` The rest goes to their `manifest-${FOLDER_NAME}.js`

### Optimize all the images with tinypng
```
npm run tiny
```
### /!\ Be carefull ! by default all the image are optimized you can exculde folder with adding parameters {no-tiny} /!\


# Json

## scripts

### minify all json
```
npm run json
```


### watch json folder and autominify the json files
```
npm run json:watch
```


# Video

## scripts

### convert all video .avi / .mov in mp4 & webm with high res and low res.
/!\ long task
```
npm run video
```

# Obj (WIP)

## scripts

### This task copy the content of 'raw-assets/model' in 'dist/assets/model' and optimize all the obj if they have options

## Options

The only option available is the *precision*.
The precision set the number of digits for every vertices.


```
// precision is a number
filename{precision}.obj

```

```
npm run obj
```

### watch model folder and copy all file automatically
```
npm run obj:watch
```



# Editor config

EditorConfig helps developers maintain consistent coding styles between different editors.

## prerequire : Editor config pluggin
* sublime https://github.com/sindresorhus/editorconfig-sublime
* atom https://github.com/sindresorhus/atom-editorconfig



# Eslint

Lint our javascript files

## prerequire : Eslint pluggin
* sublime https://github.com/roadhump/SublimeLinter-eslint
* atom https://atom.io/packages/linter-eslint

# Fido

How to work with fido.js in local version.

* Clone the fido.js's repository on your computer
* Install all depedencies of fido

```
npm install

```
* Link the package fido.js
```
npm link

```

* Link the package pixi.js under fido.js/node_modules/pixi.js
```
npm link

```

Now you have linked Fido.js and PIXI.js LOCALY !


* run command *npm link fido.js* on our template
* run command *npm link pixi.js* on our template

```
npm link packageName

```

Now you can work with fido.js without troubleshooting :)
