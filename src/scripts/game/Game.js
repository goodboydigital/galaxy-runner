import * as PIXI from 'pixi.js';
import Signal          from 'signals';

import Controller      from 'control/PointAndClickController';
import Levels          from './Levels';
import Hud             from 'hud/Hud';
import Ticker          from 'fido/system/Ticker';

PIXI.DisplayObject.prototype.depthOffset = 0;

export default class Game {
	constructor(view) {
		this.onGameover = new Signal();
		// this.world = new World();
		this.hud = new Hud(this);

		this.isGameover = false;
		this.paused = true;
		this.score = 0;
		this.counter = 0;

		this.view = new PIXI.Container();
		this.floor = PIXI.Sprite.fromImage(window.ASSET_URL + 'image/common/Room_BG.jpg');
		this.view.addChild(this.floor);

		this.floor.x = -32;
		this.floor.y = -40;

		// not exist anymore use world install
		// this.view.addChild(this.world.view);
		this.view.addChild(this.hud);

		//alert(Levels)
		this.controller = new Controller(this);
	}
	reset(config)
	{
		this.isGameover = false;

		config = config || this.config;
		this.config = config;

		this.counter = 0;
		this.score = 0;

		this.hud.reset();
	}

	start(config)
	{
		this.reset(config);
		this.resume();
	}

	pause()
	{
		if(this.paused) return;

		this.paused = true;
		this.view.interactive = false;
		Ticker.instance.remove(this.update, this);
	}

	resume()
	{
		if(!this.paused) return;

		this.paused = false;
		this.view.interactive = true;
		Ticker.instance.add(this.update, this, 1);
	}

	update()
	{
		this.counter += Ticker.instance.deltaTime;

			// this.world.update();
		this.hud.update();
	}


	gameover ()
	{
		if(this.isGameover) return;
		this.isGameover = true;
		this.pause();
		this.hud.showGameover();
	}
}
