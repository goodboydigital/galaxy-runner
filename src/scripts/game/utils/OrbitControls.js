var PixiTrackpad = require('fido/ui/PixiTrackpad');
import * as PIXI from 'pixi.js'
import {mat4} from 'gl-matrix';

export default class OrbitControls
{
	constructor(target)
	{
		target.interactive = true;
		target.hitArea = new PIXI.Rectangle(0,0,100000,100000);

		this.viewMatrix = mat4.create();
		this.rotationX = mat4.create();
		this.rotationY = mat4.create();
		this.rotation = mat4.create();

		this.trackpad = new PixiTrackpad({target:target});
		this.trackpad.unlock()
	}

	update()
	{
		const view = this.viewMatrix;
		const rotationX = this.rotationX;
		const rotationY = this.rotationY;
		const rotation = this.rotation;

		mat4.identity(view)
		mat4.translate(view, view, [0, 0, -50]);

		mat4.rotateY(rotationY,rotationY, this.trackpad.speed * 0.01);
		mat4.rotateX(rotationX,rotationX, this.trackpad.speedY * 0.01);

		mat4.multiply(rotation, rotationY, rotationX);

		mat4.multiply(view, view, rotation);

		this.trackpad.update();
	}
}