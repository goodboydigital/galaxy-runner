import { mat3, mat4, vec3 } from 'gl-matrix';
import BasicEntity     from 'odie/BasicEntity';
import BasicGame from 'odie/BasicGame'
import PoolSystem	   from 'odie/components/pool/PoolSystem';
import Device from 'fido/system/Device';

import Body from './components/Body';
import Bridge2 from './Bridge2';
import FiltersSystem from './systems/FiltersSystem';
import HudSystem	   from './systems/HudSystem';
import IntroOverlay from './IntroOverlay';
import LevelFeed from './LevelFeed';
import Levels from './Levels';
import * as PIXI from 'pixi.js';
import Player from './entities/Player';
import Skybox from './entities/Skybox';
import DelayedCalls from 'app/utils/DelayedCalls';
import Spring from 'fido/physics/DoubleSpring';

//import OrbitControl from './src/utils/OrbitControls';

export default class RushGame extends BasicGame {
	constructor(stage)
	{
		super(stage);
		this.stage = stage;

		this.register('onCollected');
		this.register('onHit');
		this.register('onBreakEnemy');
		this.register('onDash');
		this.register('onDashStop');

		var levels = Levels;//();
		this.view3d.camera.transform.position.z = 30;
		this.view3d.camera.transform.position.y =10;

		this.stage.hitArea = new PIXI.Rectangle(-5000,-5000,10000,10000)
		//this.controls = new OrbitControls(this);

		 var cubeTexture = PIXI.CubeTexture.from(ASSET_URL + 'image/skybox/posx.jpg',
                                            ASSET_URL + 'image/skybox/negx.jpg',
                                            ASSET_URL + 'image/skybox/posy.jpg',
                                            ASSET_URL + 'image/skybox/negy.jpg',
                                            ASSET_URL + 'image/skybox/posz.jpg',
                                            ASSET_URL + 'image/skybox/negz.jpg')


		this.skybox = new Skybox(cubeTexture);
		this.player = new Player();

		this.addChild(this.player);
		this.addChild(this.skybox);

		this.addSystem(PoolSystem);
		this.addSystem(LevelFeed);

		if(Device.instance.desktop)
		{
	 		this.addSystem(FiltersSystem, {stage: stage});

		}


		this.addSystem(HudSystem, {stage: this.stage });



		this.bridge = new Bridge2();

		this.addChild(this.bridge);

		this.delayedCalls = new DelayedCalls();

		const camera = this.view3d.camera;

		this.overlay = new IntroOverlay(this)
		stage.addChild(this.overlay);

		camera.addScript('script', {

			constructor()
			{
				this.entity.uniforms.uniforms.inverseView = mat3.create();
				this.entity.uniforms.uniforms.cubeTexture = cubeTexture;
				this.entity.uniforms.uniforms.cameraPos = vec3.create(0,0,0);
				this.spring = new Spring();
				this.springDry = new Spring();
				this.springDry.damp = 0.7;
        		this.springDry.springiness = 0.59;

				this.follow = new BasicEntity()
				.add(Body)

				this.mat = mat4.create();
			},

			addedToGame()
			{
				this.entity.game.addChild(this.follow);
			},

			shake(dir,amount)
		    {
		        dir = dir || Math.random() * Math.PI * 2;
		        amount = amount || 10;


	            this.springDry.dx = Math.sin(dir) * amount;
	            this.springDry.dy = Math.cos(dir) * amount;
		    },

			onHit()
			{
				this.spring.dx = Math.random() * 4 + 2;
				this.spring.dy = Math.random() * 4 + 2;
			},

			start(){

				this.entity.transform.custom = true;

				this.entity.lens.angle = Math.PI /3;

				this.entity.lens.updateProjection(this.entity.game.width, this.entity.game.height);
         		//this.entity.transform.localTransform = this.entity.game.player.transform.localTransform;
         		this.rot = 0;
				this.tick = 0;
         	},

			update(){

				this.spring.update();
				this.springDry.update();
				this.follow.body.position.z = this.entity.game.player.body.position.z;
				this.follow.body.position.x += (this.entity.game.player.body.position.x - this.follow.body.position.x) * 0.1;

				var playerMat = this.follow.transform.localTransform;

				var cameraMatrix = this.entity.transform.localTransform;
				mat4.copy(cameraMatrix, playerMat);
         		mat4.identity(this.mat, this.mat);

				let extraX = 0;
				let extraY = 0;
				let extraZ = 0;

				this.tick++;
				if(this.entity.game.dashed && this.tick % 2 === 0)
				{
					extraX = Math.random() * 2 - 2/2;
					extraY = Math.random() * 2 - 2/2;
					extraZ = Math.random() * 2 - 2/2;
				}
				this.mat[12] = 0 + this.spring.x + this.springDry.x + extraX;
				this.mat[13] = 20 + this.spring.y + this.springDry.y + extraY;
				this.mat[14] = 34 + extraZ;

         		mat4.rotateX(this.mat, this.mat, -0.2);
         		mat4.rotateZ(this.mat, this.mat, Math.sin(this.rot) * 0.2 );

         		this.rot += 0.001;

         		mat4.multiply(cameraMatrix, cameraMatrix, this.mat)

         		mat3.normalFromMat4(this.entity.uniforms.uniforms.inverseView,  cameraMatrix);
				mat3.invert(this.entity.uniforms.uniforms.inverseView,  this.entity.uniforms.uniforms.inverseView);

				this.entity.uniforms.uniforms.cameraPos[0] = cameraMatrix[12];
				this.entity.uniforms.uniforms.cameraPos[1] = cameraMatrix[13];
				this.entity.uniforms.uniforms.cameraPos[2] = cameraMatrix[14]

         	//	mat4.invert(this.entity.uniforms.uniforms.inverseView, cameraMatrix);

			}
		})


		this.signals.onHit.add(this.onHit, this);
		this.signals.onBreakEnemy.add(this.onBreakEnemy, this);
		this.signals.onDash.add(this.onDash, this);
		this.signals.onDashStop.add(this.onDashStop, this);
		this.signals.onCollected.add(this.onCollected, this);
		/*
		const camera = this.game.view3d.camera;
			camera.transform.custom = true;

			this.controls.update();
			mat4.invert(camera.transform.localTransform, this.controls.viewMatrix)

			this.game.update(1);
		 */
	}

	begin()
	{
		this.hud.container.visible = true
		this.hud.container.alpha = 0;
		TweenLite.to(this.hud.container, 0.3, {alpha:1})
		this.levelFeed.active = true;

		this.player.begin();
		this.bridge.builder.reset();
	}

	resize(w, h)
    {
    	super.resize(w, h);
    	this.overlay.resize(w, h);
    }

	onDash()
	{
		if(this.dashed) return;

		this.dashed = true;
		this.delayedCalls.add(()=>{
			this.signals.onDashStop.dispatch();
		}, 6);

		this.hud.onDash();
		this.player.onDash();

		Easings.to(this.view3d.camera.lens, 2, {
			angle: Math.PI / 1.5,
			ease: Easings.easeInOutCirc,
			onUpdate:()=>{
				this.view3d.camera.lens.updateProjection(this.view3d.game.width, this.view3d.game.height);
			}
		});

	}

	onDashStop()
	{
		this.dashed = false;
		this.hud.onDashStop();
		this.player.onDashStop();

		Easings.to(this.view3d.camera.lens, 1, {
			angle: Math.PI / 3,
			ease: Easings.easeInOutCirc,
			onUpdate:()=>{
				this.view3d.camera.lens.updateProjection(this.view3d.game.width, this.view3d.game.height);
			}
		});
	}

	onCollected()
	{
	}

	onBreakEnemy()
	{
		this.view3d.camera.script.shake(null, Math.random() * 4);
	}

	// didnt want to create a signal just for this function, called from player finishCrash()
	resurect()
	{
		let o = {
			n: 1
		}

		Easings.to(o, 2,
		{
			n: 0,
			ease: Easings.easeOutCirc,
			onUpdate:()=>{
				this.bridge.builder.updatePercentageRed(o.n);
			}
		})
	}

	onHit()
	{
		if(this.player.invicible)
		{
			return;
		}

		let o = {
			n: 0
		}

		Easings.to(o, 1,
		{
			n: 1,
			ease: Easings.easeOutCirc,
			onUpdate:()=>{
				this.bridge.builder.updatePercentageRed(o.n);
			}
		})

		this.view3d.camera.script.onHit();
		this.hud.onHit();
		this.player.onHit();
		this.bridge.builder.reset();
	}


}
