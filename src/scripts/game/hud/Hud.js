import * as PIXI from 'pixi.js';
import Signal          from 'signals';

// import GameObjectPool  from 'fido/game/GameObjectPool');
import Button          from 'fido/ui/buttons/AbstractButton';
import Utils           from 'fido/utils/Utils';
//  import Pop             from "../view/Pip");

export default class Hud extends PIXI.Container {
	constructor(game) {
		super();
		this.game = game;

	}
	onPausePressed()
	{
		this.game.reset();
		return;
	}

	reset()
	{
	}


	update()
	{
	}

	showGameover()
	{
	//   this.addChild(this.gameoverText)
		//  this.gameoverText.explode("GAMEOVER");
	}


	onGameoverComplete()
	{
		//  this.removeChild(this.gameoverText);
		this.game.onGameover.dispatch();
	}

	resize(w, h)
	{
		this.lives.position.x = (1024 / 2 - w/2) - 32 + 10 + 100;
		this.lives.position.y = h - 90;

		this.score.position.y = h - 90;
		this.score.position.x = (1024 / 2 + w/2) - 32 -10;

		this.gameoverText.x = w/2 +32+10;
		this.gameoverText.y = h/2; //- 40;
	}
}
