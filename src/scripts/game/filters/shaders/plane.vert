// plane.vert

precision highp float;
attribute vec3 aPosition;
attribute vec2 aUV;

uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

varying vec2 vUV;

void main(void) {
    gl_Position = uProjectionMatrix * uViewMatrix * vec4(aPosition, 1.0);
    vUV = aUV;
}