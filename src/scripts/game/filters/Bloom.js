// Bloom.js
import vs from './shaders/pass.vert';
import fsThreshold from './shaders/threshold.frag';
import fsBloom from './shaders/bloom.frag';
import fsCompose from './shaders/bloomCompose.frag';

class Bloom {
	constructor(mWidth, mHeight, mNumMips = 5, mStartingScale = 0.5) {
		this._width = mWidth;
		this._height = mHeight;
		this._numMips = Math.min(mNumMips, 5);
		this._startingScale = mStartingScale;

		this._init();
	}

	_init() {
		let width = Math.round(this._width*this._startingScale);
		let height = Math.round(this._height*this._startingScale);

		// this.renderTexture = new PIXI.RenderTexture.create(width, height, 0, 1);

		this._fbos = [];
		for(let i=0; i<this._numMips; i++) {
			console.log('level', i, width, height);
			const fboV = new PIXI.RenderTexture.create(width, height, 1, 1);
			const fboH = new PIXI.RenderTexture.create(width, height, 1, 1);

			console.log('baseTexture', fboV.baseTexture);
			this._fbos.push({fboV, fboH});

			width = Math.round(width/2);
			height = Math.round(height/2);
		}

		const scale = 1;
		this._fboThreshold = new PIXI.RenderTexture.create(this._width * scale, this._height * scale, 0, 1);
		// this._fboThreshold.baseTexture.frameBuffer.enableDepth()
		this._fboCompose = new PIXI.RenderTexture.create(this._width * scale, this._height * scale, 0, 1);


		//	geometry
		const positions = [
			-1, -1, 0,
			 1, -1, 0,
			 1,  1, 0,
			-1,  1, 0
		];

		const uv = [
			0, 0,
			1, 0,
			1, 1,
			0, 1
		];

		const geometry = new PIXI.mesh.Geometry()
		.addAttribute('aPosition', positions)
		.addAttribute('aUV', uv)
		.addIndex([0, 1, 2, 0, 2, 3]);

		//	SHADERS
		const uniformsThreshold = {
			luminosityThreshold:.9,
			smoothWidth:0.03,
			defaultOpacity:0,
			defaultColor:[0, 0, 0]
		}

		this.shaderThreshold = PIXI.Shader.from(vs, fsThreshold, uniformsThreshold);
		this.meshThreshold = new PIXI.mesh.RawMesh(geometry, this.shaderThreshold);


		//	bloom
		const kernelSizeArray = [3, 5, 7, 9, 11];
		width = Math.round(this.width*this._startingScale);
		height = Math.round(this.height*this._startingScale);

		this._shadersBloom = [];
		this._meshBloom = [];
		for(let i=0; i<this._numMips; i++) {
			let kernelSize = kernelSizeArray[i];
			let fs = fsBloom.replace(/\${kernelRadius}/g, kernelSize);
			const uniforms = {
				texSize:[width, height, 0]
			}
			const shader = PIXI.Shader.from(vs, fs, uniforms);
			this._shadersBloom.push(shader);

			width = Math.round(width/2);
			height = Math.round(height/2);
			const mesh = new PIXI.mesh.RawMesh(geometry, shader);
			this._meshBloom.push(mesh);
		}

		//	compose

		this.uniformsCompose = {
			bloomStrength:0.7,
			bloomRadius:0.1,
		}
		let fs = fsCompose.replace(/\${NUM_MIPS}/g, this._numMips);
		let strBloom = '';
		for(let i=0; i<this._numMips; i++) {
			strBloom += (i > 0 ? '\t' : '') + `color += lerpBloomFactor(bloomTintColors[${i}].a) * vec4(bloomTintColors[${i}].rgb, 1.0) * texture2D(blurTexture${i+1}, vUV);\n`;
		}

		fs = fs.replace(/\${BLOOMS}/g, strBloom);

		console.log('fs :', fs);

		let tintColor = [];
		for(let i=0; i<this._numMips; i++) {
			const {fboH} = this._fbos[i];
			this.uniformsCompose[`blurTexture${i+1}`] = fboH;
			tintColor = tintColor.concat([1, 1, 1, 1.0-0.2*i]);
		}

		this.uniformsCompose.bloomTintColors = tintColor;
		this.shaderCompose = PIXI.Shader.from(vs, fs, this.uniformsCompose);
		this.meshCompose = new PIXI.mesh.RawMesh(geometry, this.shaderCompose);
	}


	render(renderer, texture) {
		this.shaderThreshold.uniforms.texture = texture;
		this.shaderCompose.uniforms.texture = texture;

		renderer.render(this.meshThreshold, this.textureThreshold, true);

		let inputTexture = this.textureThreshold;


		for(let i=0; i<this._numMips; i++) {
			const {fboV, fboH} = this._fbos[i];
			const mesh = this._meshBloom[i];
			mesh.shader.uniforms.texture = inputTexture;
			renderer.render(mesh, fboV, true);

			mesh.shader.uniforms.texture = fboV;
			renderer.render(mesh, fboH, true);

			inputTexture = fboH;
		}

		this._bloomTexture = inputTexture;

		renderer.render(this.meshCompose, this._fboCompose, true);
	}

	resize(w, h)
	{

		//	bloom
		const kernelSizeArray = [3, 5, 7, 9, 11];
		let width = Math.round(w*this._startingScale);
		let height = Math.round(h*this._startingScale);

		//this._shadersBloom = [];
		//this._meshBloom = [];
		for(let i=0; i<this._numMips; i++) {
			let kernelSize = kernelSizeArray[i];

			const shader = this._shadersBloom[i];
			shader.uniforms.texSize = [width, height, 0]

			this._fbos[i].fboV.resize(width, height);
			this._fbos[i].fboH.resize(width, height);

			width = Math.round(width/2);
			height = Math.round(height/2);
		}

		const scale = 1;
		this._fboThreshold.resize(w, h);
		this._fboCompose.resize(w, h);

		// = new PIXI.RenderTexture.create(this._width * scale, this._height * scale, 0, 1);
		// this._fboThreshold.baseTexture.frameBuffer.enableDepth()
		//this._fboCompose = new PIXI.RenderTexture.create(this._width * scale, this._height * scale, 0, 1);

	}

	get textureThreshold() {
		return this._fboThreshold;
	}

	get textureBloom() {
		return this._bloomTexture;
	}

	get texture() {
		return this._fboCompose;
	}


	get width() {	return this._width;	}

	get height() {	return this._height;	}
}


export default Bloom;