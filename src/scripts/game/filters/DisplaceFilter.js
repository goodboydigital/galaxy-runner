import * as PIXI from 'pixi.js';
import { mat4, vec3 } from 'gl-matrix';

export default class DisplaceFilter extends PIXI.Container
{
    constructor(texture)
    {
        super();

        const rad = Math.PI/180;
        const ratio = 1;


        let view = mat4.create();
        const model = mat4.create();
        const proj = mat4.create();

        let vec = vec3.fromValues(0,0,2.3);
        let center = vec3.create();
        const up = vec3.fromValues(0, 1, 0);
        mat4.identity(view);
		mat4.lookAt(view, vec, center, up);
        mat4.perspective(proj, 45 * rad, ratio, 1, 100);

        let camera = {
            view,
            proj,
            model
        }

        const geometry = PIXI.mesh.Geometry.from(ASSET_URL + 'model/quad.obj')

        const vs = `
            precision highp float;
            attribute vec3 position;
            attribute vec3 normals;
            attribute vec2 uvs;

            uniform mat4 view;
            uniform mat4 proj;
            uniform mat4 model;
            uniform float time;

            varying vec3 vNormal;
            varying vec3 vPos;
            varying vec2 vUV;


            void main() {

            	gl_Position = proj * view * model * vec4(position, 1.);

                vNormal = normals;
                vUV = uvs;
                vPos = position;
            }
        `;

        const fs = `
        precision highp float;

        uniform sampler2D texture;
        varying vec2 vUV;

        uniform vec2 center;
        uniform float strength;
        uniform vec2 texSize;
        uniform float vignetteOffset;
        uniform float vignetteDarkness;
        uniform float sine;
        uniform float cosine;
        uniform vec2 red;
        uniform vec2 green;
        uniform vec2 blue;
        uniform float saturation;
        uniform float saturationConstant;

        float random(vec3 scale, float seed) {
            /* use the fragment position for a different seed per-pixel */
            return fract(sin(dot(gl_FragCoord.xyz + seed, scale)) * 43758.5453 + seed);
        }

        vec3 czm_saturation(vec3 rgb, float adjustment)
        {
            // Algorithm from Chapter 16 of OpenGL Shading Language
            const vec3 W = vec3(0.2125, 0.7154, 0.0721);
            vec3 intensity = vec3(dot(rgb, W));
            return mix(intensity, rgb, adjustment);
        }

        void main() {
            vec4 color = vec4(0.0);
            float total = 0.0;
            vec2 toCenter = center - vUV * texSize;

            float blurAmount = 0.3; // include blur amount.
            vec4 actual = texture2D(texture, vUV );
            //vec4 actual = vec4(1.0,1.0,1.0,1.0);

            float dist = distance( vUV, vec2( 0.5 ) );

            // randomize the lookup values to hide the fixed number of samples
            float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);

            for (float t = 0.0; t <= 20.0; t++) {
               float percent = (t + offset) / 20.0;
               float weight = 4.0 * (percent - percent * percent);
               vec4 sample = texture2D(texture, vUV + toCenter * percent * strength / texSize);

                // switch to pre-multiplied alpha to correctly blur transparent images
               sample.rgb *= sample.a;

               color += sample * weight;
               total += weight;
            }

            color = color / total;

            float mixBlur = 0.08; // the minimum amount of NON-blur to include ( show original image through blur )
            float distMix = smoothstep( 0.8, vignetteDarkness * 0.799, dist *( vignetteOffset + vignetteDarkness ) );
            gl_FragColor = mix( color, actual, max( mixBlur, distMix ) );

            // shift and subtle distort

            vec4 shift;
            float distMixInv = 1.0 - distMix;
            shift.r = texture2D(texture, vUV + ( red * distMixInv ) /texSize.xy ).r;
            shift.g = texture2D(texture, vUV + ( green * distMixInv ) /texSize.xy ).g;
            shift.b = texture2D(texture, vUV + ( blue * distMixInv ) /texSize.xy ).b;
            shift.a = texture2D(texture, vUV ).a;

            gl_FragColor = mix( gl_FragColor, shift, 0.3 );


            float feather = 0.005;
            float satMix = 1.0 - smoothstep( 0.5, feather * 0.499, dist * ( 1.2 + feather ) );

            satMix *= saturationConstant;

            gl_FragColor.rgb = czm_saturation(gl_FragColor.rgb, max( saturation * ( satMix + 1.0 ), 1.0 )  );

            //switch back from pre-multiplied alpha
            gl_FragColor.rgb /= gl_FragColor.a + 0.00001;
        }
        `;

        const shader = new PIXI.Shader.from(vs, fs, {
            view: view,
            proj: proj,
            model: model,
            texture,
            center: {
                x: 1900/2.0,
                y: 1200/2.0
            },
            strength: 0.15,
            texSize: {
                x: 1900.0,
                y: 1200.0
            },
            vignetteOffset: 0.96,
            vignetteDarkness: 0.0,
            sine:  0.3,
            cosine: 0,
            red: {
                x: 20,
                y: 0.0
            },
            green: {
                x: 0.0,
                y: 10.0
            },
            blue: {
                x: -30.0,
                y: 5.0
            },
            saturation: 0,
            saturationConstant: 0
        });


        this.mesh = new PIXI.mesh.RawMesh(geometry, shader);
        this.addChild(this.mesh);
    }
}
