import {mat4, vec3, quat} from 'gl-matrix'
import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';
import mat4Decompose from 'mat4-decompose';
import {wrap} from 'fido/utils/Math2';

import BridgeMaterial from './materials/bridge/BridgeMaterial';
import ColorMaterial from './materials/color/ColorMaterial';


export default class Bridge extends BasicEntity
{
  constructor()
  {
    super();

    let state = new PIXI.State();

    state.depthTest = true;
    state.culling = true;
    state.blend = true;
    state.clockwiseFrontFace = true;
    state.blendMode = PIXI.BLEND_MODES.SCREEN;

    var positions = [];
    var uvs = [];
    var normals = [];
    var random = [];
   // var pos3 = [];
    var width = 50;
    var step = 10;

    this.matricies = [];
    this.decomposed = [];

    const mat = mat4.create();
    const mat2 = mat4.create();
    const matOut = mat4.create();

    mat4.translate(mat, mat, [0, 0, -step]);


    let vec = [-width/2, 0, 0]
    let vec1 = [width/2, 0, 0]

    let vecOut = [-width/2, 0, 0]
    let vec1Out = [width/2, 0, 0]
    //let vec3 =


    for (var i = 0; i < 1000; i++)
    {

        mat4.rotate(mat2, mat, 0.05, vec3.normalize([0,0,0], [Math.cos(i * 0.1) * 0.2, 0.1, Math.sin(i * 0.01)]))

        mat4.multiply(matOut,matOut,mat2)

        var pos = [0,0,0];
        var quaty = [0,0,0,1];

         // translation, scale, skew, perspective, quaternion

        var decompose = mat4Decompose(matOut, pos, null, null, null, quaty);

        this.matricies.push( mat4.clone(matOut) );

        this.decomposed.push({pos, quaty});

        vec3.transformMat4(vecOut, vec, matOut);
        vec3.transformMat4(vec1Out, vec1, matOut);

        positions.push(vecOut[0], vecOut[1], vecOut[2]);
        positions.push(vec1Out[0], vec1Out[1], vec1Out[2]);

        //normals.push()

        uvs.push(0, i * 0.03);
        uvs.push(1, i * 0.03);

        random.push(Math.random(), Math.random(), Math.random());

    }


    this.geometry = new PIXI.mesh.Geometry()
    .addAttribute('position', positions)
    .addAttribute('uvs', uvs)
    .addAttribute('random', random, 1)

    this.material = new BridgeMaterial(0xFFFFFF);

    this.add(View3d, {
        geometry:this.geometry,
        material:this.material,
        draw:5,
        state,
        orderBias:-1
    })


    this.entity = new BasicEntity();

    state = new PIXI.State();
    state.depthTest = false;
    state.culling = true;
    state.blend = true;
    state.clockwiseFrontFace = false;
    state.blendMode = PIXI.BLEND_MODES.SCREEN;

    this.entity.add(View3d, {

          geometry:this.geometry,
          material:this.material,
          draw:5,
          state,
          orderBias:-1
        })

    this.addChild( this.entity);

    this.addScript({

      start(){

        this.pos = 0;
        this.count = 0;
      //  this.rotationSpeed = Math2.random(0.06, 0.1)
      //  this.entity.transform.rotation.z = Math.random() * Math.PI * 2
      },

      buildBridge()
      {

      },

      update()
      {
        this.pos += 2;
        const index = (this.pos/10)|0;

        const m = this.entity.matricies[index];

        if(this.count < index)
        {
            this.count++;
          //  this.geometry.getAttribute('position');


            //positions
            console.log(this.count)
        }
      //  console.log(index)//, m);

      //  mat4.copy(this.entity.cube.transform.localTransform, m);

      //  this.entity.transform.rotation.z += this.rotationSpeed;
      }
    })


    //this.add()
  }


    getMatrixAtPosition(z)
    {
        if(z < -8000)z = -8000;

        z *= -1;

      //  z %= 10 * 10;
//        if(z < 0)z += 10*10

        z *= -1;

        var ratio = (-z/10) % 1;

        let index = Math.floor(-z/10);
        let index2 = Math.ceil(-z/10);

        index = wrap(index, 0, this.decomposed.length);
        index2 = wrap(index2, 0, this.decomposed.length);
     //   index = 0;
       // index2 = 0;

        if(index > this.decomposed.length-1)
        {
            index = this.decomposed.length;
            index2 = this.decomposed.length;
        }

        var decomposed = this.decomposed[index];
        var decomposed2 = this.decomposed[index2];

        var pos = [0,0,0];
        var quaty = [0,0,0, 1];

        vec3.lerp(pos, decomposed.pos, decomposed2.pos, ratio);
        quat.slerp(quaty, decomposed.quaty, decomposed2.quaty, ratio);

       // console.log(pos, decomposed.pos, decomposed2.pos)
       // console.log(ratio)//decomposed.quat);

        var mat = mat4.create();

        mat4.fromRotationTranslation(mat, quaty, pos);


//        console.log(mat);

        return mat;
  //      return this.matricies[index];
    }
}
