import System from 'odie/components/System';

import Asteroid from './entities/Asteroid';
import Levels from './Levels';
import Pickup from './entities/Pickup';
import Dash from './entities/Dash';
import Wall from './entities/Wall';
import Enemy from './entities/Enemy';

const map = {
	1:Wall,
	2:Pickup,
	3:Enemy,
	4:Dash,
}

let playerPos = { x: null, z: null};

export default class LevelFeed extends System
{
	constructor(entity, data)
    {
        super(entity, data);

        this.chunkPos = 0;
        this.detailPos = 0;

        this.distance = 800;
        this.culldistance = 200;
		this.step = 60;
		this.width = 50;

		this.active = false;

		this.elements = [];
	}

	addChunk(chunk, distance)
	{


		const flip = Math.random() < 0.5;

	//	return;
		for (var i = 0; i < chunk.length; i++)
		{
			this.chunkPos -= this.step;

			if(!this.active)continue;

			var data = chunk[i];

			for (var j = 0; j < data.length; j++)
			{
				var id = data[j];

				if(id)
				{
					if(id === 4)
					{
						if(Math.random() < 0.9)
						{
							continue;

						}

					}
					var classType = map[id];
					var loc = j;
					if(flip)loc = 2-loc;

					var x = loc*(this.width/3) - this.width/3;
					var z = this.chunkPos;

					this.addItem(classType, {x, z})
				}
			}
		}
	}

	update()
	{
		var player = this.game.player;
		playerPos.x = player.body.position.x;
		playerPos.z = player.body.position.z;

		var pos = player.body.position.z - this.distance;
		var deadPos = player.body.position.z + this.culldistance;//this.distance;

		if(pos < this.chunkPos)
		{
			this.addChunk(Levels[(Math.random()*Levels.length)|0])
		}

		for (var i = 0; i < this.elements.length; i++) {

			var entity = this.elements[i];
			if(entity.body.position.z > deadPos)
			{
				entity.destroy();
			}
			else if (
				Math.abs(entity.body.position.z - playerPos.z) < (player.radius + entity.radius)
				&& Math.abs(entity.body.position.x - playerPos.x) < (player.radius + entity.radius) // collide!
			)
			{
				if(!entity.collected)
				{
					entity.onCollide(player);
				}
			}
		}

		if(pos - 1000 < this.detailPos)
		{
			this.detailPos -= 100

			var angle = Math.random() * Math.PI * 2;
			var dist = 100 + Math.random() * 200;
			this.addItem(Asteroid, {x:Math.cos(angle) * dist, y:Math.sin(angle) * dist, z:this.detailPos});

		}
		//TODO
		// remove from world..

		/*
		const feed = this.feeds[0];
		//const sonic = this.game.levelLoader.sonic;

		if(feed)
		{
			for(var i = 0; i < 20; i++)
			{

				var platforms = feed.chunk;
				var height = feed.height;
				var index = feed.index;

				var platform =  platforms[index];

				let max = platform.pY;

				// todo..
				if(platform.bot - height > sonic.maxHeight - this.game.height/2 - 200)
				{

					this.addPlatform(platform, null, height);
					feed.index++;

					if(feed.index >= platforms.length)
					{
						this.feeds.shift();
						break;
					}

				}
			}
		}
		//}
		*/
	}

	addItem(classType, options)
	{


//			console.log("platform added")
			const pool = this.game.pool;
			var entity = pool.get(classType, options);


			//entity.transform.position.x = platform.pX;
			//entity.transform.position.y = platform.pY - height;// - ;
		//	entity.culling.top = platform.top;
		//	entity.culling.bottom = platform.bottom;

			this.game.addChild(entity);

			this.elements.push(entity);

	}

	empty()
	{
		this.feeds.length = 0;
	}


}
