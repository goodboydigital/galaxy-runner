// EnemyMaterial.js
import * as PIXI from 'pixi.js'

import vs from './bridge.vert';
import fs from './bridge.frag';

export default class BridgeMaterial extends PIXI.Shader {


    constructor(cubeTexture)
    {
        const texture = PIXI.Texture.from(ASSET_URL + 'image/common/bridge.jpg');
        texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
        texture.baseTexture.scaleMode = PIXI.SCALE_MODES.LINEAR;


        const uniforms = PIXI.UniformGroup.from({
            cubeTexture,
            texture,
            count:0,
            percentageRed: 0
        });

        //console.log('uniforms', uniforms);
        super(PIXI.Program.from(vs, fs), uniforms);

    }


}
