precision highp float;
attribute vec3 position;
attribute vec3 normals;
attribute vec2 uvs;

varying vec3 vNormal;
varying vec2 vUvs;
varying float vFog;
varying vec4 vPosition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {

	vPosition = view * model * vec4(position, 1.);

    gl_Position = projection *  vPosition;

    vFog = (gl_Position.z) / 2000.;
    vFog *= vFog;
    vFog *= vFog;
    vFog *= 6.;
    vFog = clamp(vFog, 0.,1.);

    vUvs = uvs;

    vNormal = normals;
}
