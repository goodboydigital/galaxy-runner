precision highp float;

uniform sampler2D texture;
uniform mat3 inverseView;
uniform samplerCube cubeTexture;
uniform vec3 cameraPos;
uniform float count;
uniform float percentageRed;

//varying vec3 vNormal;
varying vec2 vUvs;
varying float vFog;
varying vec3 vNormal;
varying vec4 vPosition;

void main()
{
	vec4 color =  texture2D(texture, vUvs);
	vec4 color2 =  texture2D(texture, vUvs + vec2( count * 0.0, count * 0.01));

	vec3 incident_eye = normalize(vPosition.xyz);

	vec3 normal = normalize(vNormal);

	vec3 reflected = reflect(incident_eye, normal);
	//reflected = vec3(inverseView * reflected);

	vec4 reflectedColor = textureCube(cubeTexture, reflected);
	reflectedColor *= 0.5;

	//color *= 0.5;
//	/color2 *= 0.5//(sin(count * 0.04)+1.)/2.;

    gl_FragColor = reflectedColor + (color * color2);// mix(color, reflectedColor, 0.5);// * (1.-vFog);//vec4(color, 1.);//0.5 + vec4(vNormal, 1.);//color + (vNormal * 0.3), 1.);
    gl_FragColor.rgb += ( (vNormal + 1.) * 0.5 ) * 0.3;

	gl_FragColor.rgb *= vec3(1. + percentageRed * 2.0 , 1. - percentageRed * 0.5, 1. - percentageRed * 0.5);

    if(vUvs.y < 0.01)
    {
    	gl_FragColor = vec4(1.);
    }

    if(vUvs.y > 1.-0.01)
    {
    	gl_FragColor = vec4(1.);
    }

    gl_FragColor = mix(gl_FragColor, vec4(0.), vFog);// * (1.-vFog);//vec4(color, 1.);//0.5 + vec4(vNormal, 1.);//color + (vNormal * 0.3), 1.);
  //  gl_FragColor = vec4((vNormal + 1.) * 0.5, 1.);// * (1.-vFog);//vec4(color, 1.);//0.5 + vec4(vNormal, 1.);//color + (vNormal * 0.3), 1.);
}
