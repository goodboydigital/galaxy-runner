precision highp float;
attribute vec3 position;
attribute float random;
//attribute vec3 normals;

varying float vNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {

    gl_Position = projection * view * model * vec4(position, 1.);

    vNormal = random;//vec3(1.);//normals;
}
