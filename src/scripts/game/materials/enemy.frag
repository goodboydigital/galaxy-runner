precision highp float;

uniform vec3 color;
varying float vNormal;


const vec3 LIGHT = vec3(.2, .2, 1.0);

float diffuse(vec3 N, vec3 L) {
	return max(dot(N, normalize(L)), 0.0);
}


vec3 diffuse(vec3 N, vec3 L, vec3 C) {
	return diffuse(N, L) * C;
}

void main()
{
	//float d = diffuse(vNormal, LIGHT);
	//d = mix(d, 1.0, 0.65);

	gl_FragColor = vec4(1., vNormal, 0.0, 1.);//vec4(color * d, 1.0);

	// gl_FragColor = vec4(.2, 0.0, 0.0, .2);
}
