precision highp float;

uniform sampler2D diffuse;
uniform float blink;

varying vec2 vUvs;
varying vec3 vNormals;

void main()
{
   // if(blink == 0.0)
  //  {
     //   discard;
   // }
  	float directional = max(dot(vNormals, vec3(0., 0.,1.)), 0.0);
    vec3 vLighting = (vec3(1., 1., 1.) * directional);

   // gl_FragColor = mix(vec4(1.), texture2D(diffuse, vec2(vUvs.x, vUvs.y)), blink);
    vec4 diffuseVec = texture2D(diffuse, vec2(vUvs.x, vUvs.y));
    diffuseVec.rgb += vLighting* vLighting;

    gl_FragColor = mix(vec4(1.), diffuseVec, blink);
}
