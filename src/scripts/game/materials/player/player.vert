precision highp float;
attribute vec3 position;
attribute vec3 normals;
attribute vec2 uvs;

uniform mat3 normal;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

varying vec2 vUvs;
varying vec3 vNormals;

void main() {

    gl_Position = projection * view * model * vec4(position, 1.);
    vUvs = uvs;
    vNormals = normal * normals;
}
