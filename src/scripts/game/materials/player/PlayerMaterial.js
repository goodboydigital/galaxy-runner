import * as PIXI from 'pixi.js'

import frag from './player.frag';
import vert from './player.vert';




export default class DiffuseMaterial extends PIXI.Shader
{
    constructor(diffuse)
    {
    	const uniforms = PIXI.UniformGroup.from({diffuse, blink: 1.0});
        super(PIXI.Program.from(vert, frag), uniforms);
    }
}
