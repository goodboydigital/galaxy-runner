precision highp float;

uniform samplerCube cubeTexture;

varying vec3 vUv;

void main()
{
    vec3 dir = normalize(vUv);
    vec4 samplex2 = textureCube(cubeTexture, vUv);
    gl_FragColor = samplex2;//samplex + samplex2;// vec4(sample.rgb, sample.a);//texture2D(texture, vUv) * alpha;

}