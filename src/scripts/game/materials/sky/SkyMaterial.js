// EnemyMaterial.js
import * as PIXI from 'pixi.js'

import {mat4, vec3, quat} from 'gl-matrix'

import vs from './skybox.vert';
import fs from './skybox.frag';

export default class SkyMaterial extends PIXI.Shader {


    constructor(cubeTexture, opacity = 1)
    {
        const uniforms = PIXI.UniformGroup.from({
            //cubeTexture,
        });

        //console.log('uniforms', uniforms);
        super(PIXI.Program.from(vs, fs), uniforms);

    }


}
