precision highp float;
attribute vec3 position;

uniform mat4 P, V;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

varying vec3 vUv;

void main() {

	mat4 final = view;
	final[3][0] = 0.;
	final[3][1] = 0.;
	final[3][2] = 0.;

    gl_Position =  projection * final * vec4(position, 1.0);
    vUv = position;
}
