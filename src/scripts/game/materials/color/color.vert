precision highp float;
attribute vec3 position;
attribute vec3 normals;

varying vec3 vNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {

    gl_Position = projection * view * model * vec4(position, 1.);

    vNormal = normals;
}
