precision highp float;

uniform vec3 color;
uniform float opacity;
varying vec3 vNormal;

void main()
{
    gl_FragColor = vec4(color, 1.) * opacity;//0.5 + vec4(vNormal, 1.);//color + (vNormal * 0.3), 1.);
}
