// EnemyMaterial.js
import * as PIXI from 'pixi.js'

import vs from './color.vert';
import fs from './color.frag';

class EnemyMaterial extends PIXI.Shader {


    constructor(color = 0xFF0000, opacity = 1)
    {
        const uniforms = PIXI.UniformGroup.from({
            color:PIXI.utils.hex2rgb(color, new Float32Array(3)),
            opacity
        });

        //console.log('uniforms', uniforms);
        super(PIXI.Program.from(vs, fs), uniforms);

        this._color = color;
    }

    set color(color = 0xFF0000)
    {
        this._color = color;
        this.uniforms.color = PIXI.utils.hex2rgb(color, this.uniforms.color);
    }

    get color()
    {
        return this._color
    }

}


export default EnemyMaterial
