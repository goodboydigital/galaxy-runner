precision highp float;
attribute vec3 position;
attribute vec3 normals;
attribute vec2 uvs;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

varying vec2 vUvs;

void main() {

    gl_Position = projection * view * model * vec4(position, 1.);
    vUvs = uvs;
}
