precision highp float;

uniform sampler2D diffuse;

varying vec2 vUvs;

void main()
{
    gl_FragColor = texture2D(diffuse, vec2(vUvs.x, vUvs.y));
}
