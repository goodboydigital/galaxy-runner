import * as PIXI from 'pixi.js'

import frag from './diffuse.frag';
import vert from './diffuse.vert';




export default class DiffuseMaterial extends PIXI.Shader
{
    constructor(diffuse)
    {
    	const uniforms = PIXI.UniformGroup.from({diffuse});
        super(PIXI.Program.from(vert, frag), uniforms);
    }
}
