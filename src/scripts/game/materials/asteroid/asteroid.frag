precision highp float;

// uniform vec3 color;
varying vec3 vNormal;

const vec3 LIGHT = vec3(1.2, .2, 1.0);

float diffuse(vec3 N, vec3 L) {
    return max(dot(N, normalize(L)), 0.0);
}

vec3 diffuse(vec3 N, vec3 L, vec3 C) {
    return diffuse(N, L) * C;
}

void main()
{
        float d = diffuse(vNormal, LIGHT);
        d = mix(d, 1.0, 0.65);
        gl_FragColor = vec4(vec3(.2,.2,.2) * d, 1.0);
}



// precision highp float;
//
// // varying vec3 vNormal;
//
// // const vec3 LIGHT = vec3(.2, .2, 1.0);
// //
// // float diffuse(vec3 N, vec3 L) {
// //     return max(dot(N, normalize(L)), 0.0);
// // }
// //
// // vec3 diffuse(vec3 N, vec3 L, vec3 C) {
// //     return diffuse(N, L) * C;
// // }
//
// void main()
// {
//     //float d = diffuse(vNormal, LIGHT);
//     	//d = mix(d, 1.0, 0.65);
//     gl_FragColor = vec4(1., 1.0, 0.0, 1.);//vec4(color * d, 1.0);
//     // gl_FragColor = vec4(1., 1., 1., 1.);//0.5 + vec4(vNormal, 1.);//color + (vNormal * 0.3), 1.);
// }
