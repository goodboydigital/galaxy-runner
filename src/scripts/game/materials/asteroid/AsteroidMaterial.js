import * as PIXI from 'pixi.js'

import frag from './asteroid.frag';
import vert from './asteroid.vert';




export default class AsteroidMaterial extends PIXI.Shader
{
    constructor()
    {
    	// const uniforms = PIXI.UniformGroup.from();
        super(PIXI.Program.from(vert, frag));
    }
}
