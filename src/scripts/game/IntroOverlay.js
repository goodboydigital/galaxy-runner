import * as PIXI from 'pixi.js';
import Signal          from 'signals';
import Device          from 'fido/system/Device';
import SoundManager from 'fido/sound/SoundManager';

// import GameObjectPool  from 'fido/game/GameObjectPool');
import Button          from 'fido/ui/buttons/AbstractButton';
import Utils           from 'fido/utils/Utils';
//  import Pop             from "../view/Pip");

export default class IntroOverlay extends PIXI.Container {
	constructor(game) {
		super();

		this.logo = PIXI.Sprite.from('logo.png')
		this.logo.scale.set(0.65)

		this.logo.anchor.set(0.5);

		this.bg = new PIXI.Graphics().beginFill(0x0000FF).drawRect(0,0, 100, 100);
		this.bg.blendMode = PIXI.BLEND_MODES.MULTIPLY;
		this.addChild(this.bg);
		this.addChild(this.logo);

		this.playButton = PIXI.Sprite.from('play-button.png');
		this.playButton.anchor.set(0.5);
		this.playButton.interactive = true;

		this.playIcon = PIXI.Sprite.from('play-button-icon.png');
		this.playIcon.anchor.set(0.5);
		this.playButton.addChild(this.playIcon);

		this.playButton.buttonMode = true;

		this.playButton.scale.set(0.65)
		this.playButton.click = this.playButton.tap = ()=>{
			if(Device.instance.android)
					{
				if (document.body.mozRequestFullScreen) {
								// This is how to go into fullscren mode in Firefox
								// Note the "moz" prefix, which is short for Mozilla.
					document.body.mozRequestFullScreen();
				} else if (document.body.webkitRequestFullScreen) {
								// This is how to go into fullscreen mode in Chrome and Safari
								// Both of those browsers are based on the Webkit project, hence the same prefix.
					document.body.webkitRequestFullScreen();
				}
			}

			SoundManager.play('audio/woosh', {volume:0.2});

			TweenLite.to(this.logo.pivot, 0.5, {y:800, ease:Sine.easeIn})
			TweenLite.to(this.playButton.scale, 0.3, {y:0, x:0, ease:Back.easeIn})
			TweenLite.to(this.bg, 0.5, {alpha:0, ease:Sine.easeIn, onComplete:()=>{
			this.game.begin()
			}})
			//game.begin();
		}

		this.game = game;

		this.addChild(this.playButton)
	}

	show()
	{
		this.logo.pivot.y = 800;

		this.playButton.scale.set(0);

		TweenLite.to(this.logo.pivot, 0.5, {y:0, ease:Cubic.easeOut, delay:0.5})
		TweenLite.to(this.playButton.scale, 0.3, {y:0.65, x:0.65, ease:Back.easeOut, delay:0.5})


	}

	onPlayPressed()
	{
	}


	resize(w, h)
	{
		this.logo.x = w/2 - 25;
		this.logo.y = h * 0.33;

		this.bg.scale.x = w/100;
		this.bg.scale.y = h/100;

		this.playButton.x = w/2;
		this.playButton.y = h * 0.8;

	}
}
