import * as PIXI from 'pixi.js';

export default class PointAndClickController {
	constructor(game) {
		this.game = game;

		this.view = this.game.view;
		this.view.hitArea = new PIXI.Rectangle(0, 0, 10 * 96, 7 * 96);

		this.view.interactive = true;
		this.view.buttonMode = true;

		this.view.mousedown = this.view.touchstart = this.onDown.bind(this);
	}

	disable()
			{
		this.view.interactive = false;
	}


	enable()
			{
		this.view.interactive = true;
	}

	onDown(e)
			{
		var local = e.data.getLocalPosition(this.view);
		console.log('Got click : ' + local.x + ' - ' + local.y);
	}
}
