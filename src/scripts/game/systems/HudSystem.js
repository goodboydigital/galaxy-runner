import * as PIXI from 'pixi.js';
import System from 'odie/components/System';
import Ticker          from 'fido/system/Ticker';
import Device      from 'fido/system/Device';
import SoundManager from 'fido/sound/SoundManager';
import IconButton      from 'buttons/IconButton';
import TextHelper           from 'app/utils/TextHelper';
import Save from 'app/system/Save';;




export default class HudSystem extends System {

    constructor(entity, data)
    {
        super(entity, data);
        this.view = new PIXI.Container();
        data.stage.addChild(this.view);


        this.score = 0;
        this.highscore = 0;

        this.container = new PIXI.Container();
        this.container.visible = false;
        this.view.addChild(this.container);

        this.highScoreLabel = PIXI.Sprite.from('highscore.png');

        this.container.addChild(this.highScoreLabel);

        this.highScoreText = TextHelper.getHudHighscoreText("20230");
        this.container.addChild(this.highScoreText);

        this.scoreLabel = TextHelper.getHudText("SCORE: ");
        //this.container.addChild(this.scoreLabel);

        this.scoreText = TextHelper.getHudText("20230");
        this.container.addChild(this.scoreText);


        this.containerButtons = new PIXI.Container();
        this.view.addChild(this.containerButtons);

        // "fake" buttons, mainly managed from the touch controller
		this._btnRight = IconButton.inGameButton();
		this._btnRight.view.visible = false;
		this._btnRight.view.accessible = true;
        this._btnRight.onPress.add(this._onPress, this)
		this._btnRight.view.tabIndex = 10;
		this.containerButtons.addChild(this._btnRight.view);

		this._btnLeft = IconButton.inGameButton();
		this._btnLeft.view.visible = false;
		this._btnLeft.view.accessible = true;
		this._btnLeft.onPress.add(this._onPress, this)
		this._btnLeft.view.buttonMode = true;
		this._btnLeft.view.tabIndex = 10;
        this._btnLeft.view.scale.x *= -1;
		this.containerButtons.addChild(this._btnLeft.view);



		if(!Device.instance.desktop)
		{
			this._btnRight.view.visible = true;
			this._btnLeft.view.visible = true;
		}

        this.entity.game.signals.onCollected.add(this.onPickupCollected, this);
    }

    onDash()
    {
        Easings.to(this.container.scale, 2, {
            x: .6,
            y: .6,
            ease: Easings.easeInOutCirc,
        })

        Easings.to(this.container.position, 2, {
            x: 20,
            y: 20,
            ease: Easings.easeInOutCirc,
        });
    }

    onDashStop()
    {
        Easings.to(this.container.scale, 1, {
            x: 1,
            y: 1,
            ease: Easings.easeInOutCirc,
        });

        Easings.to(this.container.position, 1, {
            x: 0,
            y: 0,
            ease: Easings.easeInOutCirc
        });

    }

    onPickupCollected()
    {
        this.pickups++;
        this.updateScore();
    }

    onHit()
    {
        this.stopped = true;
        if(this.score > this.highscore)
        {
            let score = this.score;
            Save.instance.object.highscore = Math.floor(score);

            Easings.to(this, 1, {
                highscore: score,
                onUpdate:()=>{
                    this.highScoreText.text = Math.floor(this.highscore);
                }
            })
        }

        this.resetScore();

        Easings.to(this, 1, {
            score: 0,
            onUpdate:()=>{
                this.scoreText.text = Math.floor(this.score);
            },
            onComplete: ()=>
            {
                this.stopped = false;
            }
        })
    }

    hide()
    {
        Easings.to(this.view, .2, {
            alpha: 0
        });
    }

    show()
    {
        Easings.to(this.view, .2, {
            alpha: 1
        });
    }

    _onPress(bt)
    {
        // only do that if this is in accessibility mode (otherwise buttons are only decoration)
        if(!window.renderer.plugins.accessibility.isActive || Device.instance.desktop) return;

        if(bt === this._btnLeft)
        {
            this.game.manager.player.controller.skid();
        }
        else
        {
            this.game.manager.player.controller.jump();
        }
    }

    resetScore()
    {
        this.distance = 0;
        this.pickups = 0;
    }

    start()
    {
        this.show();
        this.seconds = 0;
        this.timeElapsed = 0;
        this.score = 0;
        this.distance = 0;
        this.pickups = 0;
        this.tick = 0;
        this.highscore = Save.instance.object.highscore;
        this.highScoreText.text = this.highscore;

        this.updateScore();
    }

    updateScore()
    {

        this.score = this.distance + this.pickups * 50;
        this.scoreText.text = this.score;

        if(this.game.player.hurt)this.score = 0;

        // if(this.score > this.highscore)
        // {
            // this.highScoreText.text = this.score;
        // }
    }

	addChild(el)
	{
		this.view.addChild(el);
	}

	goRight(value)
	{
		if(value)
		{
			this._btnRight._onDown();
		}
		else {
			this._btnRight._onUp();
		}
	}

	// force the button state to be down / up
	goLeft(value)
	{
		if(value)
		{
			this._btnLeft._onDown();
		}
		else {
			this._btnLeft._onUp();
		}
	}

    update()
    {
        /* uncomment to have a timer ! */

        if(!this.stopped)
        {
            this.tick++;
            this.distance++;

            if(this.tick % 20 === 0)
            {
                this.updateScore();
            }
        }

    }


    resize(w, h)
    {
		this._btnLeft.view.position.x = 115;
		this._btnRight.view.position.x = w  - 115;
    	this._btnRight.view.position.y = this._btnLeft.view.position.y = h - 140;

        let offset = Device.instance.desktop ? 60 : 0;
        this.scoreLabel.position.x = 20;
        this.scoreLabel.position.y = 60 //+ 30;
        this.scoreText.position.x = this.scoreLabel.position.x + this.scoreLabel.width ;
        this.scoreText.position.y = this.scoreLabel.position.y ;

        this.highScoreLabel.position.x = 20;
        this.highScoreLabel.position.y = 20 //+ 30;
        this.highScoreText.position.x = this.highScoreLabel.position.x + this.highScoreLabel.width + 10;
        this.highScoreText.position.y = this.highScoreLabel.position.y-7;


    }

}
