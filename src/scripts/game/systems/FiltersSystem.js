import System from 'odie/components/System';

import Bloom from '../filters/Bloom';

export default class FiltersSystem extends System
{
    constructor(entity, data)
    {
        super(entity, data);

        this.view = new PIXI.Container();
        data.stage.addChild(this.view);
        this.rt = new PIXI.RenderTexture.create(1024, 1024, 1, 1);
        this.rt.baseTexture.frameBuffer.enableDepth()


       // this.view.addChild(superFilter);
        this.bloom = new Bloom(1024, 1024);


        var sp = new PIXI.Sprite(this.bloom.texture);
        sp.anchor.y = 1;
        sp.scale.y = -1;
        sp.scale.x *= 0.5;
        sp.scale.y *= 0.5;

        this.view.addChild(sp);
        this.vig = PIXI.Sprite.from(ASSET_URL + 'image/vignette-health.png');
        this.vig.alpha = 1
        this.view.addChild(this.vig);

        this.view3dSystem = this.entity.game.view3d;


    }

    start()
    {
        this.tick = 0;
    }

    update()
    {
        this.tick++;

        this.view3dSystem.setRenderTexture(this.rt);
        this.bloom.render(window.renderer, this.rt);


        // this.view3dSystem.unbindRenderTexture();
    }

    resize(w, h)
    {
        this.vig.width = w;
        this.vig.height = h;

        this.bloom.resize(w * 2, h * 2)
        this.rt.resize(w * 2, h * 2);
    }
}
