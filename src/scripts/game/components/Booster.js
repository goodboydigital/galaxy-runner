import { mat4, quat } from 'gl-matrix';
import { random } from 'fido/utils/Math2';
import BasicEntity     from 'odie/BasicEntity';
import Component from 'odie/components/Component';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';

import ColorMaterial from '../materials/color/ColorMaterial';


const tempMat = mat4.create();
var tempQuat         = quat.create();
var temp3dTransform  = mat4.create();


export default class Booster extends Component
{
    constructor(entity, data = {})
    {
        super(entity, data);


        this.booster = this.createBooster();
        this.booster2 = this.createBooster();

         this.booster.transform.position.set(12, -1.9, -0.8)
         this.booster2.transform.position.set(12, -1.9, 0.8)
    }

    createBooster()
    {

          const state = new PIXI.State();
        state.depthTest = true;
        state.culling = false;
        state.blend = true;
        state.clockwiseFrontFace = false;

        const booster = new BasicEntity()
        .add(View3d, {
          geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/cube.obj'),
          material:new ColorMaterial(0xFFFFFF, 0.5),
          state,
          orderBias:1
        })

        booster.transform.scale.set(20, 0.8, 0.8)

        return booster;
    }

    update()
    {
        var target = random(0.8, 1)
        if(this.entity.hurt)
        {
            target = 0;
        }
        this.booster.view3d.material.uniforms.opacity = target
        this.booster2.view3d.material.uniforms.opacity = target
    }

    start()
    {
        this.entity.modelAlignContainer.addChild(this.booster);
        this.entity.modelAlignContainer.addChild(this.booster2);
    }
}
