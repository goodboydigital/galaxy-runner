import { mat4, vec3 } from 'gl-matrix';
import Component from 'odie/components/Component'
import mat4Decompose from 'mat4-decompose';

import Keyboard from 'fido/input/Keyboard.js'

export default class Builder extends Component
{
    constructor(entity, data)
    {
        super(entity, data);

        this.keyboard = new Keyboard();

        this._enabled = true;

        this.pressed = false;

        this.lane = 0;

    }

    make(matOut, mat2, mat)
    {

       // const mat = matxx;
       // mat2 = mat4.create();
        //matOut = mat4.create();
        const total = 10;
        const width = 50;

   //     mat4.translate(mat, mat, [0, 0, -step]);
        console.log(matOut, mat2, mat)

        for (var i = 0; i < total; i++)
        {

            var attribute = this.entity.geometry.getAttribute('position');
            var positions = attribute.data;

            let vec = [-width/2, 0, 0]
            let vec1 = [width/2, 0, 0]

            let vecOut = [-width/2, 0, 0]
            let vec1Out = [width/2, 0, 0]

            console.log(mat2, mat);
            mat4.rotate(mat2, mat, 0.04, vec3.normalize([0,0,0], [Math.cos(i * 0.1) * 0.2, 0.1, Math.sin(i * 0.01)]))
            mat4.multiply(matOut,matOut,mat2)

            var pos = [0,0,0];
            var quaty = [0,0,0,1];

            var decompose = mat4Decompose(matOut, pos, null, null, null, quaty);
            this.entity.decomposed[i] = {pos, quaty};

            vec3.transformMat4(vecOut, vec, matOut);
            vec3.transformMat4(vec1Out, vec1, matOut);

            const indexStart = i * 6;

            positions[indexStart] = vecOut[0];
            positions[indexStart+1] = vecOut[1];
            positions[indexStart+2] = vecOut[2];

            positions[indexStart+3] = vec1Out[0];
            positions[indexStart+4] = vec1Out[1];
            positions[indexStart+5] = vec1Out[2];

            attribute.update(positions);


        }

    }

}
