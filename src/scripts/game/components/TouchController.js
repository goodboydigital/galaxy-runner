import Component from 'odie/components/Component'
import * as PIXI from 'pixi.js';

var temp = new PIXI.Point();

export default class TouchController extends Component
{
    constructor(entity, data)
    {
        super(entity, data);

        this._enabled = true;

        this.pressedRight = false;
        this.pressedLeft = false;
    }

    set enabled(value)
    {
        if(this._enabled === value)return;

        this._enabled = value;
    }

    get enabled()
    {
        return this._enabled;
    }



    goRight()
    {
        this.entity.game.hud.goRight(true);
        this.pressedRight = true;
    }

    goLeft()
    {
        this.pressedLeft = true;
        this.entity.game.hud.goLeft(true);
    }

    addedToGame()
    {
        const view = this.entity.game.stage
        view.interactive = true;
        this.activate();

        view.touchstart = event => {
            if(this.deactivated) return;
            const pos = event.data.getLocalPosition(view, temp);
            if(pos.x > this.entity.game.width/2)
            {
                this.goRight();
            }
            else
            {
                this.goLeft();
            }

        }

        view.touchend = view.touchendoutside = event => {

            if(this.deactivated) return;

            const pos = event.data.getLocalPosition(view, temp);

            if(pos.x > this.entity.game.width/2)
            {
                this.entity.game.hud.goRight(false);
            }
            else
            {
                this.entity.game.hud.goLeft(false);
            }
        }

    }

    deactivate()
    {
        this.deactivated = true;
    }

    activate()
    {
        this.deactivated = false;
    }

    removedFromGame()
    {

    }

    update()
    {
        if(!this._enabled && this.entity.game)return;


        if(this.pressedRight && this.pressedLeft) return;

        if(this.pressedRight)
        {
            this.pressedRight = false;
            this.entity.movement.slideRight()
        }

        if(this.pressedLeft)
        {
            this.entity.movement.slideLeft()
            this.pressedLeft = false;
        }

    }

}
