import { mat4, vec3, quat } from 'gl-matrix';
import Component from 'odie/components/Component';

const tempMat = mat4.create();
var tempQuat         = quat.create();
var temp3dTransform  = mat4.create();


export default class Body extends Component
{
    constructor(entity, data = {})
    {
        super(entity, data);

        this.position = {x:0, y:0, z:0}

        this.entity.transform.custom = true;

    }

    update()
    {
        var m = this.entity.game.bridge.builder.getMatrixAtPosition(this.position.z);
     //   console.log(m)
        const transform = this.entity.transform;

        transform.position.x = this.position.x;
        transform.position.y = this.position.y;
        transform.position.z = 0;//this.position.z;



         //TODO CACHING!
            //if(!entity.static || transform.dirty)
        transform.dirty = false;

        const quat = tempQuat;

        const rx = transform.rotation.x;
        const ry = transform.rotation.y;
        const rz = transform.rotation.z;

        //TODO cach sin cos?
        const c1 = Math.cos( rx / 2 );
        const c2 = Math.cos( ry / 2 );
        const c3 = Math.cos( rz / 2 );

        const s1 = Math.sin( rx / 2 );
        const s2 = Math.sin( ry / 2 );
        const s3 = Math.sin( rz / 2 );

        quat[0] = s1 * c2 * c3 + c1 * s2 * s3;
        quat[1] = c1 * s2 * c3 - s1 * c2 * s3;
        quat[2] = c1 * c2 * s3 + s1 * s2 * c3;
        quat[3] = c1 * c2 * c3 - s1 * s2 * s3;

        temp3dTransform[0] = transform.position.x;
        temp3dTransform[1] = transform.position.y;
        temp3dTransform[2] = transform.position.z;

        mat4.fromRotationTranslation(transform.localTransform, quat, temp3dTransform);

        temp3dTransform[0] = transform.scale.x;
        temp3dTransform[1] = transform.scale.y;
        temp3dTransform[2] = transform.scale.z;

        mat4.scale( transform.localTransform, transform.localTransform, temp3dTransform);


        mat4.multiply(transform.localTransform, m, transform.localTransform);
    }

    start()
    {
    }
}
