import Component from 'odie/components/Component'
import Keyboard from 'fido/input/Keyboard.js'

export default class KeyboardController extends Component
{
    constructor(entity, data)
    {
        super(entity, data);

        this.keyboard = new Keyboard();

        this._enabled = true;

        this.pressed = false;

        this.leftDown = false;
        this.rightDown = false;

        this.enabled = false;
    }

    set enabled(value)
    {
        if(this._enabled === value)return;

        this._enabled = value;
    }

    get enabled()
    {
        return this._enabled;
    }

    addedToGame()
    {

    }

    removedFromGame()
    {
        // disable stuff!
    }

    update()
    {
        if(!this._enabled && this.entity.game)return;


        if(this.keyboard.isPressed('left'))// || this.keyboard.isPressed('up') )
        {
            if(!this.leftDown)
            {
                this.leftDown = true;
                this.entity.movement.slideLeft()
            }

        }
        else
        {
             this.leftDown = false;
        }

        if(this.keyboard.isPressed('right'))
        {
            if(!this.rightDown)
            {
                this.entity.movement.slideRight()
                this.rightDown = true;
            }
        }
        else
        {
            this.rightDown = false;
        }




//            this.entity.movement.move(0, 0);

/*        var isdown = this.keyboard.isPressed('x');

        if(isdown)
        {

            this.entity.movement.isSpecial = true;

        }

        if(this.pressed !== isdown)
        {
            this.pressed = isdown;

            if(this.pressed)
            {
                this.entity.specialMove.activate();
            }
            else
            {
                this.entity.specialMove.deactivate();
            }
        }

        if(this.keyboard.isPressed('z'))
        {
            this.entity.speak.talk(1);
        }
*/
    }

}
