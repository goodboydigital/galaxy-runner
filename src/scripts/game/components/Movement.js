import Component from 'odie/components/Component'

import Keyboard from 'fido/input/Keyboard.js'

export default class Movement extends Component
{
    constructor(entity, data)
    {
        super(entity, data);

        this.keyboard = new Keyboard();

        this._enabled = true;

        this.pressed = false;

        this.lane = 0;

        this.xTarget = 0
    }

    slideLeft()
    {
        if(this.entity.hurt)return;
        if(this.lane > -1)
        {
            this.lane--;

            this.entity.easeRotation.z = Math.PI/5;
            // this.entity.transform.rotation.z = this.entity.baseRotation.z + Math.PI/5;
        }

    }

    slideRight()
    {
        if(this.entity.hurt)return;
        if(this.lane < 1)
        {
            this.lane++;
            this.entity.easeRotation.z = -Math.PI/5;
            // this.entity.transform.rotation.z = this.entity.baseRotation.z - Math.PI/5;

        }
    }


    update()
    {
        if(!this._enabled && this.entity.game)return;

        this.xTarget += ((this.lane * 50/3 )- this.xTarget) * 0.3;

        this.entity.body.position.x = this.xTarget //+ Math.random() *0.5

    }

}
