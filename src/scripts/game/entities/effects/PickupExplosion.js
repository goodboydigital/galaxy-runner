import * as PIXI from 'pixi.js';
import BasicEntity from 'odie/BasicEntity';
import ColorMaterial from '../../materials/color/ColorMaterial';
import View3d from 'odie/components/view3d/View3d';
import Body from '../../components/Body';

export default class PickupExplosion extends BasicEntity
{
    constructor()
    {
        super();

        const state = new PIXI.State();
        state.depthTest = true;
        state.blend = true;
        state.clockwiseFrontFace = true;

        let pos = [
            [-1, 1, 1],
            [-1, 1, -1],
            [1, 1, 1],
            [1, 1, -1],
            [-1, -1, 1],
            [-1, -1, -1],
            [1, -1, 1],
            [1, -1, -1],
        ]
        this.cubes = [];
        for (var i = 0; i < pos.length; i++) {
            let cube = new BasicEntity().add(View3d, {
                geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/cube.obj'),
                material:new ColorMaterial(0xffffff),
                state
            });


            let p = pos[i];

            cube.speed = {};
            cube.originalPos = p;
            cube.transform.position.x = p[0] * (2.5);
            cube.transform.position.y = p[1] * (2.5);
            cube.transform.position.z = p[2] * (2.5);
            cube.transform.scale.set(3);
            // cube.transform.position.z = Math.floor(i / 2) * 5;

            this.container.add(cube);
            this.cubes.push(cube);
        }
        this.add(Body)
        .addScript('script', {

            init(data)
            {
                this.active = true;
                this.count = 0;
                this.pool = [];
                this.particles = [];

                this.entity.body.position.x = data.x;
                this.entity.body.position.y = data.y;
                this.entity.body.position.z = data.z;

                for (var i = 0; i < this.entity.cubes.length; i++) {
                    let c = this.entity.cubes[i];
                    c.transform.position.x = c.originalPos[0];
                    c.transform.position.y = c.originalPos[1];
                    c.transform.position.z = c.originalPos[2];
                }
            },

            start()
            {
            },

            update()
            {
                if(this.entity.exploding)
                {
                    this.entity.life--;
                    if(this.entity.life < 0)
                    {
                        this.entity.destroy();
                    }
                    for (var i = 0; i < this.entity.cubes.length; i++) {
                        let c = this.entity.cubes[i];
                        c.transform.position.x += c.speed.x;
                        c.transform.position.y += c.speed.y;
                        c.transform.position.z += c.speed.z;
                        c.transform.rotation.x += c.speed.r;
                        c.transform.rotation.y += c.speed.r;
                        c.transform.rotation.z += c.speed.r;

                        c.speed.x *= .9;
                        c.speed.y *= .8;
                        c.speed.z *= .9;

                    }
                }

            }
        })

    }

    explode()
    {
        this.exploding = true;

        // add to put a short life before destroy, otherwise body.
        // --> in body, var m = this.entity.game.bridge.builder.getMatrixAtPosition(this.position.z);
        // --> throws an error cause the depth is too far (var bridge = this.bridges[bridgeIndex+this.spliceOffset] is undefined);
        this.life = 10;

        for (var i = 0; i < this.cubes.length; i++) {
            let speedX = Math.random() * 4 + 2;
            let speedY = Math.random() * 4 + 2;
            let speedZ = Math.random() * 4 + 2;
            let c = this.cubes[i]
            c.speed.x = (c.transform.position.x < 0 ? -1 : 1) * speedX;
            c.speed.y = (c.transform.position.y < 0 ? -1 : 1) * speedY;
            c.speed.z = (c.transform.position.y < 0 ? -1 : -1) * speedZ / 2;
            c.speed.r = Math.random() * .5 - .5/2;
        }
    }
}
