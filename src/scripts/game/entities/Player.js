import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';

import DelayedCalls from 'app/utils/DelayedCalls';
import Device from 'fido/system/Device';

import Body from '../components/Body';
import Booster from '../components/Booster';
//import Easings from '../../app/utils/Easings';
import KeyboardController from '../components/KeyboardController';
import Movement from '../components/Movement';
import PlayerMaterial from '../materials/player/PlayerMaterial';
import TouchController from '../components/TouchController';


export default class Player extends BasicEntity
{
  constructor()
  {
    super();

    const state = new PIXI.State();
    state.depthTest = true;
    state.culling = true;
    state.blend = false;
    state.clockwiseFrontFace = false;

    this.radius = 5;
    this.hurt = false;
    this.invicible = false;

    this.easeSpeed = 5;
    this.speed = 5;

    this.modelAlignContainer = new BasicEntity();
    this.modelAlignContainer.transform.scale.y = 1
    this.container.add(this.modelAlignContainer)

    this.modelAlignContainer.add(View3d, {
      geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/spaceship.obj'),
      material:new PlayerMaterial(PIXI.Texture.from(ASSET_URL + 'image/spaceship.jpg')),
      state,
    })

    this.modelAlignContainer.transform.rotation.y = -Math.PI/2;

    this.transform.scale.set(1.5);

    this.easeRotation = {
        x: 0,
        y: 0,
        z: 0
    }

    this.delayedCalls = new DelayedCalls();


    this.add(Body)
    .add(Movement)
    .add(Booster)
    .add(Device.instance.desktop ? KeyboardController : TouchController, null, 'controller')
    .addScript({

      start(){

        //this.pos = 0;
        this.tick = 0;
        this.entity.easeSpeed = 2;
        this.entity.speed = this.entity.easeSpeed;
        this.tick = 0;
      },

        onHit()
        {

        },

        update()
        {
            if(this.entity.delayInvicible > 0)
            {
                this.entity.delayInvicible--;


                if(this.entity.delayInvicible <= 0)
                {
                    this.entity.invicible = false;
                    this.entity.modelAlignContainer.view3d.material.uniforms.blink += (1 - this.entity.modelAlignContainer.view3d.material.uniforms.blink) * 0.3;
                }
                else {
                    this.entity.modelAlignContainer.view3d.material.uniforms.blink = (Math.sin(this.entity.delayInvicible * 0.2) + 1) / 2;
                }
            }


            // this.entity.easeSpeed = 2;
            this.entity.speed += (this.entity.easeSpeed - this.entity.speed) * .1;
            this.entity.body.position.z -= this.entity.speed;

            console.log(this.entity.hurt);
            if(!this.entity.hurt)
            {
                this.tick++;

                if(this.entity.easeSpeed < 4)
                {
                    if(this.tick % 20 === 0)
                    {
                        this.entity.easeSpeed += .1;
                    }
                }

                if(this.tick % 200 === 0)
                {
                    this.entity.easeSpeed += .1;

                }

     //           console.log(this.entity.easeSpeed );
                this.entity.transform.rotation.z += (this.entity.easeRotation.z - this.entity.transform.rotation.z) * .1;

                this.entity.transform.rotation.x = Math.cos(this.tick/100) * (Math.PI/40) + (Math.PI/40)/2;

                this.entity.modelAlignContainer.transform.position.z = Math.sin(this.tick/50) * 2;
                this.entity.modelAlignContainer.transform.position.x = Math.cos(this.tick/80) * 2;
                //
                this.entity.easeRotation.z *= .9
            }
      }
    })


     this.body.position.y = 6;
  }

  finishCrash()
  {
      this.easeRotation.z = 0;

      this.delayInvicible = 180;

      Easings.to(this.transform.rotation, 1, {
          z:0,
          ease: Easings.easeOutBack,
          onComplete:()=>{
              this.transform.rotation.z = 0;
              this.easeRotation.z = 0;
          }
      });

      Easings.to(this.transform.rotation, 1, {
          y: this.transform.rotation.y - Math.PI/8,
          ease: Easings.easeOutBack,
          onComplete:()=>{
              this.game.resurect();
              this.transform.rotation.y = 0;
          }
      });

      Easings.to(this, 1, {
        delay: 0,
        easeSpeed: 3,
        ease: Easings.easeInSine,
        onComplete:()=>{
            this.hurt = false;
        }
      });
  }

  onHit()
  {
      if(this.onDashed) return;

      this.invicible = true;
      this.hurt = true;
      this.currentSpeed = this.speed;
      this.speedRX = -1;
      this.phase = 0;
      console.log(Easings)
      Easings.to(this.transform.rotation, 1.6, {
          y: Math.PI * 2 * 2 + Math.PI/8,
          ease: Easings.easeOutCirc
      });

      Easings.to(this.transform.rotation, 1.2, {
          z: Math.PI/3,
          ease: Easings.easeInOutCirc
      });

      Easings.to(this, 2, {
          easeSpeed: .1,
          ease: Easings.easeOutSine,
          onComplete: this.finishCrash.bind(this)
      });
  }

  begin()
  {
    this.controller.enabled = true;
  }

  onDash()
  {
      this.onDashed = true;

      if(this.hurt)
      {
          Easings.killTweensOf(this.transform.rotation);
          this.transform.rotation.z = 0;
          this.easeRotation.z = 0;
          this.transform.rotation.y = 0;
          this.hurt = false;
      }

      Easings.to(this, 1, {
          easeSpeed: 10,
          ease: Easings.easeInCirc
      });
  }

  onDashStop()
  {
      this.delayInvicible = 200;
      this.invicible = true;

      Easings.to(this, 2, {
          easeSpeed: 5,
          ease: Easings.easeOutCirc,
          onComplete:()=>{
            this.onDashed = false;
          }
      });
  }

  init(data)
  {
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
