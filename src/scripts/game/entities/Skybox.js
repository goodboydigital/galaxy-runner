import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';

import SkyMaterial from '../materials/sky/SkyMaterial';



export default class Skybox extends BasicEntity
{
  constructor(cubeTexture)
  {
    super();

    const state = new PIXI.State();
    state.depthTest = false;
    state.culling = false;
    state.blend = false;
    state.clockwiseFrontFace = false;


    const size = 10;
    var geometry = new PIXI.mesh.Geometry()
        .addAttribute('position', [-size,  size, -size,
                                   -size, -size, -size,
                                    size, -size, -size,
                                    size, -size, -size,
                                    size,  size, -size,
                                   -size,  size, -size,

                                   -size, -size,  size,
                                   -size, -size, -size,
                                   -size,  size, -size,
                                   -size,  size, -size,
                                   -size,  size,  size,
                                   -size, -size,  size,

                                    size, -size, -size,
                                    size, -size,  size,
                                    size,  size,  size,
                                    size,  size,  size,
                                    size,  size, -size,
                                    size, -size, -size,

                                   -size, -size,  size,
                                   -size,  size,  size,
                                    size,  size,  size,
                                    size,  size,  size,
                                    size, -size,  size,
                                   -size, -size,  size,

                                   -size,  size, -size,
                                    size,  size, -size,
                                    size,  size,  size,
                                    size,  size,  size,
                                   -size,  size,  size,
                                   -size,  size, -size,

                                   -size, -size, -size,
                                   -size, -size,  size,
                                    size, -size, -size,
                                    size, -size, -size,
                                   -size, -size,  size,
                                    size, -size,  size]);


    this.add(View3d, {

      geometry,
      material:new SkyMaterial(cubeTexture),
      state,
      orderBias:-1000,

    })

    //this.transform.scale.set(3);

    this.addScript({

      start(){

        //this.pos = 0;

      },

      update()
      {
      }
    })

  }

  init(data)
  {
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
