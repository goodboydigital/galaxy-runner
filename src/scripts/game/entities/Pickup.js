import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';
import SoundManager from 'fido/sound/SoundManager';
import Body from '../components/Body';
import DiffuseMaterial from '../materials/diffuse/DiffuseMaterial';
import PickupExplosion from './effects/PickupExplosion';

export default class Pickup extends BasicEntity
{
  constructor()
  {
    super();

    this.collected = false;
    this.radius = 5;

    const state = new PIXI.State();
    state.depthTest = true;
    state.culling = true;
    state.blend = false;
    state.clockwiseFrontFace = false;

    this.add(View3d, {

      geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/gift.obj'),
      material:new DiffuseMaterial(PIXI.Texture.from(ASSET_URL + 'image/common/gift-texture.png')),
      state,
    })

    this.transform.scale.set(0.5);

    this.add(Body)
    .addScript({

      start(){

        //this.pos = 0;

      },

      init(data){

        this.entity.body.position.x = data.x;
        this.entity.body.position.z = data.z;
        this.entity.body.position.y = 2.5 * 2
        this.entity.collected = false;
      },


      update()
      {
        this.entity.transform.rotation.y -= 0.1
      }
    })


    //this.add()
  }

  onCollide()
  {
      if(this.collected) return;

      SoundManager.play('audio/pickup', {volume:0.2});

      this.game.signals.onCollected.dispatch(this);
      this.collected = true;

      let pickupExplosion = this.game.pool.get(PickupExplosion, {
         x: this.body.position.x,
         y: this.body.position.y,
         z: this.body.position.z,
      });

      pickupExplosion.transform.scale.set(.8);
      this.game.addChild(pickupExplosion);
      pickupExplosion.explode();

      this.destroy();
  }

  init(data)
  {
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
