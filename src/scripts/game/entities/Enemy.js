import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';
import SoundManager from 'fido/sound/SoundManager';

import Body from '../components/Body';
import ColorMaterial from '../materials/color/ColorMaterial';
import DiffuseMaterial from '../materials/diffuse/DiffuseMaterial';

export default class Enemy extends BasicEntity
{
  constructor()
  {
    super();

    this.onActive = false;
    this.radius = 5;

    const state = new PIXI.State();
    state.depthTest = true;
    state.culling = true;
    state.blend = false;
    state.clockwiseFrontFace = false;

    // 1 and 3 are bad (anchor)
    let i = 2;//Math.floor(Math.random() * 2 + 2);

    // top
    let top = new BasicEntity().add(View3d, {
        geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/pyramid-top.obj'),
        material:new DiffuseMaterial(PIXI.Texture.from(ASSET_URL + 'image/common/pyramid-top.jpg')),//PIXI.Texture.from(ASSET_URL + 'image/' + model + '.jpg')),
        state,
    });
    this.container.add(top);
    this.top = top;


    let heart = new BasicEntity().add(View3d, {
        geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/pyramid-heart.obj'),
        material:new DiffuseMaterial(PIXI.Texture.from(ASSET_URL + 'image/common/pyramid-heart.jpg')),//PIXI.Texture.from(ASSET_URL + 'image/' + model + '.jpg')),
        state,
    });
    this.heart = heart;
    this.container.add(heart);


    let down = new BasicEntity().add(View3d, {
        geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/pyramid-down.obj'),
        material:new DiffuseMaterial(PIXI.Texture.from(ASSET_URL + 'image/common/pyramid-down.jpg')),//PIXI.Texture.from(ASSET_URL + 'image/' + model + '.jpg')),
        state,
    });
    this.down = down;
    this.container.add(down);
    // ball

    // bottom



    // let modelAlignContainer = new BasicEntity();
  //  modelAlignContainer.transform.rotation.y = Math.random() > .49 ? Math.PI/2 : -Math.PI/2;
    // modelAlignContainer.transform.position.y = 0;
    // this.container.add(modelAlignContainer)

    // modelAlignContainer.add(View3d, {
    //   geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/' + model + '.obj'),
    //   material:new ColorMaterial(0xEEEEEE, 1),//PIXI.Texture.from(ASSET_URL + 'image/' + model + '.jpg')),
    //   state,
    // })

//     let blackBox = new BasicEntity();
// //    blackBox.transform.rotation.y = Math.random() > .49 ? Math.PI/2 : -Math.PI/2;
//     blackBox.transform.position.y = 0;
//     this.container.add(blackBox)
//
//     blackBox.add(View3d, {
//       geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/' + model + '.obj'),
//       material:new ColorMaterial(0x0551be, 1),//PIXI.Texture.from(ASSET_URL + 'image/' + model + '.jpg')),
//       state,
//     })

    // blackBox.transform.scale.set(1.1, 0.5, 1.1)
    // blackBox.transform.scale.set(0.7, 0.5, 0.5)
    // blackBox.transform.position.z = 0.5
    // modelAlignContainer.addChild(blackBox);

    // this.transform.scale.set(50/3, 50/3 * 2, 50/3);


    // if(i === 1)
    // {
    // }
    // else
    // {
    //     this.transform.scale.set(1);
    // }

    this.add(Body)
    .addScript({

      start(){

        //this.pos = 0;
        this.speedExplosion = {
            top: {
                x: 3,
                y: 3,
                z: 3,
            },
            bottom: {
                x: -3,
                y: 3,
                z: 3,
            }
        }

      },

      init(data){

          if(!this.speedExplosion)
          {
              this.speedExplosion = {
                  top: {
                      x: 3,
                      y: 3,
                      z: 3,
                  },
                  bottom: {
                      x: -3,
                      y: 3,
                      z: 3,
                  }
              }
          }

         this.entity.heart.transform.scale.set(1);

        this.speedExplosion.top.x = Math.random() * 1;
        this.speedExplosion.top.y = Math.random() * 2;
        this.speedExplosion.top.z = Math.random() * 2;

        this.speedExplosion.bottom.x = -Math.random() * 1;
        this.speedExplosion.bottom.y = Math.random() * 2;
        this.speedExplosion.bottom.z = Math.random() * 2;

        this.tick = Math.random() * 10;
        this.entity.body.position.x = data.x;
        this.entity.body.position.z = data.z;
        this.entity.body.position.y = 0;
        this.entity.onActive = false;
      },


      update()
      {
       // this.entity.body.position.z -= 0.1
       this.tick++;

       if(this.entity.exploding)
       {
           this.entity.top.transform.position.x += this.speedExplosion.top.x;
           this.entity.top.transform.position.y += this.speedExplosion.top.y;
           this.entity.top.transform.position.z += this.speedExplosion.top.z;

           this.speedExplosion.top.x *= .98;
           this.speedExplosion.top.y *= .98;
           this.speedExplosion.top.z *= .98;


           this.entity.down.transform.position.x += this.speedExplosion.bottom.x;
           this.entity.down.transform.position.y += this.speedExplosion.bottom.y;
           this.entity.down.transform.position.z += this.speedExplosion.bottom.z;

           this.speedExplosion.bottom.x *= .98;
           this.speedExplosion.bottom.y *= .98;
           this.speedExplosion.bottom.z *= .98;

           // this.entity.top.transform.position.y += 1;
       }
       else {
           this.entity.top.transform.position.y = Math.cos(this.tick / 10) * 1;
           this.entity.down.transform.position.y = Math.sin(this.tick / 10) * 1;
           this.entity.top.transform.rotation.y -= .05;
           this.entity.down.transform.rotation.y += .05;
       }
      }
    })


    //this.add()
  }

  onCollide()
  {
      if(this.onActive) return;

      if(!this.game.dashed)
      {
          this.game.signals.onHit.dispatch(this);
      }
      else
      {
          this.game.signals.onBreakEnemy.dispatch(this);

      }

      SoundManager.play('audio/crash')

      this.onActive = true;
      this.explode();
  }

  explode()
  {
      this.exploding = true;
      this.heart.transform.scale.set(0);
  }

  init(data)
  {
      this.exploding = false;
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
