import {mat4, vec3, quat} from 'gl-matrix'
import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';
import mat4Decompose from 'mat4-decompose';

import {wrap} from 'fido/utils/Math2';

import BridgeMaterial from '../materials/bridge/BridgeMaterial';

var width = 50;
var step = 10;

export default class BridgeSegment extends BasicEntity
{
    constructor(total)
    {
        super();

        this.total = total;

        let state = new PIXI.State();

        state.depthTest = false;
        state.culling = false;
        state.blend = true;
        state.clockwiseFrontFace = false;
        state.blendMode = PIXI.BLEND_MODES.SCREEN;

        this.decomposed = [];

//        var total = 10;

        var index = [];


        this.geometry = new PIXI.mesh.Geometry()
        .addAttribute('position', new Float32Array(6 * (total+1)))
        .addAttribute('normals', new Float32Array(6 * (total+1)))
        .addAttribute('uvs', new Float32Array(4 * (total+1)))


      // this.sideGeometry = new PIXI.mesh.Geometry()
        //.addAttribute('position', new Float32Array(6 * (total+1)))
        //.addAttribute('uvs', new Float32Array(4 * (total+1)))
      //  .addIndex(index)
       // .addAttribute('random', new Float32Array(3 * total))

        this.material = new BridgeMaterial(window.cube);

        this.add(View3d, {
            geometry:this.geometry,
            material:this.material,
            draw:5,
            state,
            orderBias:-1
        })

        this.entity = new BasicEntity();

        state = new PIXI.State();
        state.depthTest = true;
        state.culling = false;
        state.blend = true;
        state.clockwiseFrontFace = true;
        state.blendMode = PIXI.BLEND_MODES.SCREEN;

        this.entity.add(View3d, {
              geometry:this.geometry,
              material:this.material,
              draw:5,
              state,
              orderBias:-1
        })
        this.addChild(this.entity);


        this.addScript('builder', {

            start(){

                this.pos = 0;
                this.count = 0;

            },

            update()
            {
               // this.entity.material.uniforms.count++;
            },

            build:function(matOut, mat2, mat, windData)
            {

               // const mat = matxx;
               // mat2 = mat4.create();
                //matOut = mat4.create();

           //     mat4.translate(mat, mat, [0, 0, -step]);

                var attribute = this.entity.geometry.getAttribute('position');
                var positions = attribute.data;

              //  var attributeLeft = this.entity.sideGeometry.getAttribute('position');
              //  var positionsLeft = attributeLeft.data;

                var attributeUvs = this.entity.geometry.getAttribute('uvs');
                var uvs = attributeUvs.data;

                var attributeNormals = this.entity.geometry.getAttribute('normals');
                var normals = attributeNormals.data;

                let vecy = [-width/2, 0, 0]
                let vecy2 = [width/2, 0, 0]

                let vecyNormal = [0, 1, 0]

                let vecy3 = [-width/2, 5, 0]
                let vecy4 = [width/2, 5, 0]


                let vecOut = [0, 0, 0]
                let vecOut2 = [0, 0, 0]
                let vecOut3 = [0, 0, 0]
                let vecOut4 = [0, 0, 0]

                let temp = [];
                var seg = 1/this.entity.total;

                for (var i = -1; i < this.entity.total; i++)
                {




                    windData.count++;
                    windData.count++;
                    //[Math.cos(windData.count * 0.1) * 0.2, 0.1, Math.sin(windData.count * 0.01)]
                    windData.direction[0] = Math.sin(windData.count * 0.05);//0//.1;
                    windData.direction[1] = Math.cos(windData.count * 0.005) * 0.5;;
                    windData.direction[2] = Math.sin(windData.count * 0.02)// * 0.4;;
                    vec3.normalize(windData.direction, windData.direction)

 //                    windData.strength = 0;
                    if(i >= 0)
                    {
                     //   count++;
                        mat4.rotate(mat2, mat, windData.strength, windData.direction)
                        mat4.multiply(matOut,matOut,mat2)
                    }

                    var pos = [0,0,0];
                    var quaty = [0,0,0,1];

                    var decompose = mat4Decompose(matOut, pos, null, null, null, quaty);
                    this.entity.decomposed[i] = {pos, quaty};


                    let indexStart = (i+1) * 6;

                    vec3.transformMat4(vecOut, vecy, matOut);
                    vec3.transformMat4(vecOut2, vecy2, matOut);

        //            var matNormal = mat4.clone(matOut)

      //              matNormal[12] = 0;
    //                matNormal[13] = 0;
  //                  matNormal[14] = 0;

//                    vec3.transformMat4(vecOut3, vecyNormal, matNormal);


                    positions[indexStart++] = vecOut[0];
                    positions[indexStart++] = vecOut[1];
                    positions[indexStart++] = vecOut[2];

                    positions[indexStart++] = vecOut2[0];
                    positions[indexStart++] = vecOut2[1];
                    positions[indexStart++] = vecOut2[2];


                    let uvStatrt = (i+1) * 4
                    uvs[uvStatrt] = 0;
                    uvs[uvStatrt++] = (i+1) * seg

                    uvs[uvStatrt++] = 1;
                    uvs[uvStatrt++] = (i+1) * seg

                    vec3.transformQuat(vecOut3, vecyNormal, quaty);


                    let normalStart = (i+1) * 6;

                    // now build the side..
                    normals[normalStart++] = vecOut3[0];
                    normals[normalStart++] = vecOut3[1];
                    normals[normalStart++] = vecOut3[2];

                    normals[normalStart++] = vecOut3[0];
                    normals[normalStart++] = vecOut3[1];
                    normals[normalStart++] = vecOut3[2];
                }

                attribute.update(normals);
                attribute.update(positions);
                attributeUvs.update(uvs);
            }
        })

    }
}
