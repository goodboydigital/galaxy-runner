import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';
import SoundManager from 'fido/sound/SoundManager';


import Body from '../components/Body';
import ColorMaterial from '../materials/color/ColorMaterial';
import DiffuseMaterial from '../materials/diffuse/DiffuseMaterial';

export default class Dash extends BasicEntity
{
  constructor()
  {
    super();

    this.collected = false;
    this.radius = 5;

    const state = new PIXI.State();
    state.depthTest = true;
    state.culling = true;
    state.blend = false;
    state.clockwiseFrontFace = false;

    this.cube = new BasicEntity().add(View3d, {
        geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/cube.obj'),
        material:new ColorMaterial(0xffffff),
        state,
    })
    this.addChild(this.cube)

    const stateDash = new PIXI.State();
    stateDash.depthTest = true;
    stateDash.culling = true;
    stateDash.blend = true;
    stateDash.clockwiseFrontFace = false;

    this.dash = new BasicEntity().add(View3d, {
        geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/quad.obj'),
        material:new DiffuseMaterial(PIXI.Texture.from(ASSET_URL + 'image/common/dash.png')),
        state:stateDash,
    })

    this.cube.transform.scale.set(12, 2, 20);
    this.dash.transform.scale.set(10);
    this.dash.transform.rotation.x = -Math.PI/2;
    this.dash.transform.rotation.z = Math.PI;
    this.dash.transform.position.y = 2;
    this.addChild(this.dash)

    this.add(Body)
    .addScript({

      start(){

        //this.pos = 0;

      },

      init(data){

        this.entity.body.position.x = data.x;
        this.entity.body.position.z = data.z;
        this.entity.body.position.y = 1;
        this.entity.collected = false;
      },


      update()
      {
      }
    })


    //this.add()
  }

  onCollide()
  {
      if(this.collected) return;

      SoundManager.play('audio/woosh', {volume:3});
      SoundManager.play('audio/crash')

      this.game.signals.onDash.dispatch(this);
      this.collected = true;
      this.destroy();
  }

  init(data)
  {
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
