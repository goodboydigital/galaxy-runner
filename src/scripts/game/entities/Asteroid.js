import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';

import Body from '../components/Body';
import ColorMaterial from '../materials/color/ColorMaterial';
import AsteroidMaterial from '../materials/asteroid/AsteroidMaterial';


export default class Asteroid extends BasicEntity
{
  constructor()
  {
    super();

    const state = new PIXI.State();
    state.depthTest = true;
    state.culling = true;
    state.blend = true;
    state.clockwiseFrontFace = false;

    let asteroid = 'particle1';
    this.add(View3d, {

      geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/'+asteroid+'.obj'),
      material:new AsteroidMaterial(0x0000FF),
      state,
      orderBias:-2
    })



    this.add(Body)
    .addScript({

      start(){

        //this.pos = 0;
        this.speedX = Math.random() * 0.035;
        this.speedY = Math.random() * 0.035;

        this.entity.transform.scale.set(10 + Math.random() * 5, 10 + Math.random() * 5, 10 + Math.random() * 5);
      },

      init(data){

        this.entity.body.position.x = data.x;
        this.entity.body.position.y = data.y;
        this.entity.body.position.z = data.z;

        //this.entity.body.position.y = data.y;

        this.entity.transform.rotation.x = Math.random() * Math.PI * 2;
        this.entity.transform.rotation.y = Math.random() * Math.PI * 2;
      },


      update()
      {
        this.entity.transform.rotation.x -= this.speedX
        this.entity.transform.rotation.y -= this.speedY
      }
    })


    //this.add()
  }

  init(data)
  {
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
