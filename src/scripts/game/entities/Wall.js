import BasicEntity     from 'odie/BasicEntity';
import * as PIXI from 'pixi.js';
import View3d from 'odie/components/view3d/View3d';

import Body from '../components/Body';
import ColorMaterial from '../materials/color/ColorMaterial';


export default class Wall extends BasicEntity
{
  constructor()
  {
    super();

    const state = new PIXI.State();
    state.depthTest = true;
    state.culling = true;
    state.blend = false;
    state.clockwiseFrontFace = false;

    this.add(View3d, {

      geometry:PIXI.mesh.Geometry.from(ASSET_URL + 'model/cube.obj'),
      material:new ColorMaterial(0x00FF00),
      state,
    })

    this.transform.scale.set(3);

    this.add(Body)
    .addScript({

      start(){

        //this.pos = 0;

      },

      init(data){

        this.entity.body.position.x = data.x;
        this.entity.body.position.z = data.z;
        this.entity.body.position.y = 3/2;
      },

      update()
      {
        //this.entity.body.position.z -= 0.1
      }
    })


    //this.add()
  }

  init(data)
  {
  }

  start(config)
  {

  }

  pause()
  {

  }

  resume()
  {

  }


  gameover ()
  {
  }
}
