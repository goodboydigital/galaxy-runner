


const data = [

`
-oo
xoo
xoo
xoo
xoo
xoo
xoo
-oo
`,
`
o--
o--
od-
o--
-ox
-ox
-ox
-ox
`,
`
oxo
-x-
oxo
-x-
oxo
---
---
-o-
---
--d
oxo
---
---
-o-
---
---
oxo
-x-
oxo
`,
`
oo-
---
-x-
-x-
---
-oo
---
-x-
-x-
---
oo-
---
-x-
-x-
d-d
-oo
---
-x-
-x-
---
oo-
---
-x-
-x-
---
`,
`
oo-
---
-x-
-x-
---
-oo
---
-x-
-x-
---
oo-
---
-x-
-x-
--d
-oo
---
-x-
-x-
---
oo-
---
-x-
-x-
---
`,
`
o--
---
---
d--
---
-o-
---
---
---
---
o--
---
---
---
---
--o
---
-d-
---
-o-
`,
`
-x-
-x-
-x-
-x-
-x-
-x-
-x-
`,
`
d-x
--x
--x
--x
--x
--x
--x
`,
`
oxo
oxo
oxo
oxo
oxo
oxo
oxo
`,
`
x--
x--
x--
x--
x--
x--
---
-x-
-x-
-x-
-x-
-x-
-x-
ddd
--x
--x
--x
--x
--x
--x
---
-x-
-x-
-x-
-x-
-x-
-x-
---
x--
x--
x--
x--
x--
x--
`,
`
xo-
x--
xo-
xd-
xo-
x--
xo-
x--
xo-
x--
xo-
x--
xo-
x--
xo-
x--
xo-
x--
xo-
x--
`,

`
---
---
`



].map(a => a+`
	---
	-d-
	---
`)

const map = {
	'-':0,
	'o':3,
	'x':2,
	'd':4,
}

const generateLevel = function(data)
{
	var obj = [];

	for (var i = 0; i < data.length; i++)
	{
		var chunkData = data[i];

		chunkData = chunkData.replace(/(\r\n|\n|\r)/gm,"");

		obj.push(parseChunk(chunkData));
	}


	return obj
}

const parseChunk = function(chunkData)
{
	const chunk = [];

	let chunkSplit = chunkData.split('');

	chunkSplit = chunkSplit.map(a => map[a]);

	for (var i = 0; i < chunkSplit.length; i+=3)
	{
		chunk.push([chunkSplit[i], chunkSplit[i+1], chunkSplit[i+2]])
	}

	return chunk;
}

export default generateLevel(data)
