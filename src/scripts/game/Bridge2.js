import {mat4, vec3, quat} from 'gl-matrix'
import BasicEntity     from 'odie/BasicEntity';

import {wrap} from 'fido/utils/Math2';

import BridgeSegment from './entities/BridgeSegment';


export default class Bridge extends BasicEntity
{
    constructor()
    {
        super();

        this.addScript('builder', {

            constructor(){

                this.pool = [];

                this.pos = 0;
                this.count = 0;
                this.step = 30;

                this.tileCount = 0;

                this.turnTick = 0;
                this.numberOfSeg = 75;
                this.segSize = this.step * this.numberOfSeg;

                this.spliceOffset = 0;

                this.maxPos = 0;
                this.dist = 0;

                this.matOut = mat4.create();
                this.mat2 = mat4.create();
                this.mat = mat4.create();

                this.bridges = [];

                this.winddata = {
                    direction:vec3.create(),
                    count:0,
                    strength:0.06,
                };

                this.percentageRed = 0;
                mat4.translate(this.mat, this.mat, [0, 0, -this.step]);
            },

            reset()
            {
                this.winddata.strength = 0.06;
            },

            updatePercentageRed(percentage)
            {
                this.percentageRed = percentage;
            },

            buildBridge()
            {
                const bridge =  this.pool.pop() || new BridgeSegment(this.numberOfSeg);

                bridge.startPos = this.dist;

                bridge.builder.build(this.matOut, this.mat2, this.mat, this.winddata);

                this.count += 100;

                this.entity.addChild(bridge);

                this.bridges.push(bridge)

            },

            update()
            {
                this.turnTick++;

                if(this.turnTick % 100 === 0)
                {
                    this.winddata.strength += 0.0004
                    console.log(this.winddata.strength)
                }
              //  this.winddata.strength = (Math.sin(Math.cos(this.turnTick * 0.1) * 0.3) * 0.7) * 0.1
                this.maxPos = Math.min(this.entity.game.player.body.position.z) - 2000;

                if(this.maxPos <  this.dist)
                {
                   this.dist -= this.segSize;
                   this.buildBridge();
                }

                this.tileCount++;

                for (var i = 0; i < this.bridges.length; i++)
                {
                     this.bridges[i].material.uniforms.count = this.tileCount;

                     if(this.percentageRed !== this.bridges[i].material.uniforms.percentageRed)
                     {
                         this.bridges[i].material.uniforms.percentageRed = this.percentageRed;
                     }

                    if(this.bridges[i].startPos > this.maxPos + 2300 + this.segSize)
                    {

                        this.entity.removeChild(this.bridges[i]);

                        this.pool.push(this.bridges[i])
                        this.bridges.splice(i,1);
                        this.spliceOffset--;
                        i--;
                   //     console.log("DEAD")
                    }
                }

            },

            getMatrixAtPosition(z)
            {
//                return mat4.create();


                // first find which one you are on..
                var bridgeIndex = (-z / this.segSize)|0;
            //    console.log(bridgeIndex);

                var bridge = this.bridges[bridgeIndex+this.spliceOffset];

                // now offset top the bridge..


                var offsetZ = z + (bridgeIndex*this.segSize)



                var ratio = (-offsetZ/this.step) % 1;

                // get bridge..
                let index = Math.floor(-offsetZ/this.step);
                var decomposed = bridge.decomposed[index];

                let index2 = Math.ceil(-offsetZ/this.step);
                var decomposed2 = bridge.decomposed[index2];

                if(index2 > bridge.decomposed.length-1)
                {
                    //index2 = index;
                   // console.log('....' + ratio);
                  //  const nextBridgeIndex = bridgeIndex+1;
                    bridge = this.bridges[bridgeIndex+this.spliceOffset+1];

                    //index2 = Math.ceil((-offsetZ-this.segSize)/10);
                    index2 = 0;

                    decomposed2 = bridge.decomposed[index2];
                  //  console.log('coool',ratio, decomposed.pos, decomposed2.pos);
                }
               /* else if(index === 0)
                {
                    index = 1;
                    decomposed = bridge.decomposed[index];

                   // index = 1;
                    decomposed2 = bridge.decomposed[index+1];
                    console.log("BROKEN")
                }
*/

                var pos = [0,0,0];
                var quaty = [0,0,0, 1];

                vec3.lerp(pos, decomposed.pos, decomposed2.pos, ratio);
                quat.slerp(quaty, decomposed.quaty, decomposed2.quaty, ratio);

               // console.log(pos, decomposed.pos, decomposed2.pos)
               // console.log(ratio)//decomposed.quat);

                var mat = mat4.create();

                mat4.fromRotationTranslation(mat, quaty, pos);


        //        console.log(mat);

                return mat;
            }
        })

        this.builder.buildBridge();
        //alert(this.builder)
    }


}
