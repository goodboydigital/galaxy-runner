(function (PIXI, lib) {

    var MovieClip = PIXI.animate.MovieClip;
    var Sprite = PIXI.Sprite;
    var fromFrame = PIXI.Texture.fromFrame;

    var Graphic1 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 5, loop: false });
        var instance1 = new Sprite(fromFrame("finger"))
            .setTransform(-61, -83.5);
        this.addTimedChild(instance1);
    });

    var Graphic2 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 5, loop: false });
        var instance1 = new Sprite(fromFrame("finger"))
            .setTransform(-61, -83.5);
        this.addTimedChild(instance1);
    });

    var Graphic3 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 51, loop: false });
        var instance1 = new Sprite(fromFrame("finger"))
            .setTransform(-61, -83.5);
        this.addTimedChild(instance1);
    });

    var Graphic4 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 9, loop: false });
        var instance1 = new Sprite(fromFrame("tick"))
            .setTransform(-90.5, -81.5);
        this.addTimedChild(instance1);
    });

    var Graphic5 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 2, loop: false });
        var instance1 = new Sprite(fromFrame("tick"))
            .setTransform(-90.5, -81.5);
        this.addTimedChild(instance1);
    });

    var Graphic6 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 2, loop: false });
        var instance1 = new Sprite(fromFrame("tick"))
            .setTransform(-90.5, -81.5);
        this.addTimedChild(instance1);
    });

    var Graphic7 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 17, loop: false });
        var instance1 = new Sprite(fromFrame("tick"))
            .setTransform(-90.5, -81.5);
        this.addTimedChild(instance1);
    });

    var Graphic8 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 5, loop: false });
        var instance1 = new Sprite(fromFrame("finger"))
            .setTransform(-61, -83.5);
        this.addTimedChild(instance1);
    });

    var Graphic9 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 5, loop: false });
        var instance1 = new Sprite(fromFrame("finger"))
            .setTransform(-61, -83.5);
        this.addTimedChild(instance1);
    });

    var Graphic10 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 52, loop: false });
        var instance1 = new Sprite(fromFrame("finger"))
            .setTransform(-61, -83.5);
        this.addTimedChild(instance1);
    });

    var Graphic11 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 10, loop: false });
        var instance1 = new Sprite(fromFrame("cross"))
            .setTransform(-77, -77.5);
        this.addTimedChild(instance1);
    });

    var Graphic12 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 2, loop: false });
        var instance1 = new Sprite(fromFrame("cross"))
            .setTransform(-77, -77.5);
        this.addTimedChild(instance1);
    });

    var Graphic13 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 2, loop: false });
        var instance1 = new Sprite(fromFrame("cross"))
            .setTransform(-77, -77.5);
        this.addTimedChild(instance1);
    });

    var Graphic14 = MovieClip.extend(function (mode) {
        MovieClip.call(this, { mode: mode, duration: 17, loop: false });
        var instance1 = new Sprite(fromFrame("cross"))
            .setTransform(-77, -77.5);
        this.addTimedChild(instance1);
    });

    lib.tutorial_candy_game = MovieClip.extend(function () {
        MovieClip.call(this, {
            duration: 240,
            framerate: 24
        });
        var instance1 = new Sprite(fromFrame("candy_tutorial_bg_1"));
        var instance2 = new Sprite(fromFrame("candy_tutorial_bg_2"));
        var instance3 = new Sprite(fromFrame("finger"));
        var instance4 = new Graphic1(MovieClip.SYNCHED);
        var instance5 = new Graphic2(MovieClip.SYNCHED);
        var instance6 = new Graphic3(MovieClip.SYNCHED);
        var instance7 = new Graphic4(MovieClip.SYNCHED);
        var instance8 = new Graphic5(MovieClip.SYNCHED);
        var instance9 = new Graphic6(MovieClip.SYNCHED);
        var instance10 = new Graphic7(MovieClip.SYNCHED);
        var instance11 = new Sprite(fromFrame("candy_tutorial_bg_1"));
        var instance12 = new Sprite(fromFrame("candy_tutorial_bg_3"));
        var instance13 = new Sprite(fromFrame("finger"));
        var instance14 = new Graphic8(MovieClip.SYNCHED);
        var instance15 = new Graphic9(MovieClip.SYNCHED);
        var instance16 = new Graphic10(MovieClip.SYNCHED);
        var instance17 = new Graphic11(MovieClip.SYNCHED);
        var instance18 = new Graphic12(MovieClip.SYNCHED);
        var instance19 = new Graphic13(MovieClip.SYNCHED);
        var instance20 = new Graphic14(MovieClip.SYNCHED)
            .setTransform(616.15, 368.15, 1.542, 1.542);
        this.addTimedChild(instance1, 0, 20)
            .addTimedChild(instance2, 20, 100)
            .addTimedChild(instance3, 40, 19, {
                "40": {
                    x: 673.2,
                    y: 346.65
                }
            })
            .addTimedChild(instance4, 59, 5, {
                "59": {
                    x: 734.2,
                    y: 430.15,
                    sx: 1,
                    sy: 1
                },
                "60": {
                    x: 736.231,
                    y: 425.164,
                    sx: 0.938,
                    sy: 0.938
                },
                "61": {
                    x: 738.211,
                    y: 420.178,
                    sx: 0.875,
                    sy: 0.875
                },
                "62": {
                    x: 740.23,
                    y: 415.236,
                    sx: 0.813,
                    sy: 0.813
                },
                "63": {
                    x: 742.211,
                    y: 410.25,
                    sx: 0.751,
                    sy: 0.751
                }
            })
            .addTimedChild(instance5, 64, 5, {
                "64": {
                    x: 744.2,
                    y: 405.15,
                    sx: 0.689,
                    sy: 0.689
                },
                "65": {
                    x: 742.232,
                    y: 410.179,
                    sx: 0.751,
                    sy: 0.751
                },
                "66": {
                    x: 740.225,
                    y: 415.164,
                    sx: 0.813,
                    sy: 0.813
                },
                "67": {
                    x: 738.191,
                    y: 420.112,
                    sx: 0.875,
                    sy: 0.875
                },
                "68": {
                    x: 736.184,
                    y: 425.097,
                    sx: 0.938,
                    sy: 0.938
                }
            })
            .addTimedChild(instance6, 69, 51, {
                "69": {
                    x: 734.2,
                    y: 430.15
                }
            })
            .addTimedChild(instance7, 90, 9, {
                "90": {
                    x: 605.15,
                    y: 373.1,
                    sx: 0.387,
                    sy: 0.387
                },
                "91": {
                    x: 605.177,
                    y: 373.145,
                    sx: 0.487,
                    sy: 0.487
                },
                "92": {
                    x: 605.204,
                    y: 373.139,
                    sx: 0.587,
                    sy: 0.587
                },
                "93": {
                    x: 605.213,
                    y: 373.172,
                    sx: 0.687,
                    sy: 0.687
                },
                "94": {
                    x: 605.19,
                    y: 373.167,
                    sx: 0.788,
                    sy: 0.788
                },
                "95": {
                    x: 605.249,
                    y: 373.2,
                    sx: 0.888,
                    sy: 0.888
                },
                "96": {
                    x: 605.276,
                    y: 373.195,
                    sx: 0.988,
                    sy: 0.988
                },
                "97": {
                    x: 605.285,
                    y: 373.228,
                    sx: 1.089,
                    sy: 1.089
                },
                "98": {
                    x: 605.312,
                    y: 373.222,
                    sx: 1.189,
                    sy: 1.189
                }
            })
            .addTimedChild(instance8, 99, 2, {
                "99": {
                    x: 605.15,
                    y: 373.15,
                    sx: 1.289,
                    sy: 1.289
                },
                "100": {
                    x: 605.175,
                    y: 373.158,
                    sx: 1.263,
                    sy: 1.263
                }
            })
            .addTimedChild(instance9, 101, 2, {
                "101": {
                    x: 605.2,
                    y: 373.1,
                    sx: 1.236,
                    sy: 1.236
                },
                "102": {
                    x: 605.284,
                    y: 373.204,
                    sx: 1.263,
                    sy: 1.263
                }
            })
            .addTimedChild(instance10, 103, 17, {
                "103": {
                    x: 605.15,
                    y: 373.15,
                    sx: 1.289,
                    sy: 1.289
                }
            })
            .addTimedChild(instance11, 120, 20)
            .addTimedChild(instance12, 140, 100)
            .addTimedChild(instance13, 159, 19, {
                "159": {
                    x: 153.2,
                    y: 482.65
                }
            })
            .addTimedChild(instance14, 178, 5, {
                "178": {
                    x: 214.2,
                    y: 566.15,
                    sx: 1,
                    sy: 1
                },
                "179": {
                    x: 216.212,
                    y: 561.195,
                    sx: 0.938,
                    sy: 0.938
                },
                "180": {
                    x: 218.223,
                    y: 556.19,
                    sx: 0.875,
                    sy: 0.875
                },
                "181": {
                    x: 220.231,
                    y: 551.227,
                    sx: 0.813,
                    sy: 0.813
                },
                "182": {
                    x: 222.243,
                    y: 546.222,
                    sx: 0.751,
                    sy: 0.751
                }
            })
            .addTimedChild(instance15, 183, 5, {
                "183": {
                    x: 224.2,
                    y: 541.15,
                    sx: 0.689,
                    sy: 0.689
                },
                "184": {
                    x: 222.22,
                    y: 546.174,
                    sx: 0.751,
                    sy: 0.751
                },
                "185": {
                    x: 220.243,
                    y: 551.157,
                    sx: 0.813,
                    sy: 0.813
                },
                "186": {
                    x: 218.222,
                    y: 556.106,
                    sx: 0.875,
                    sy: 0.875
                },
                "187": {
                    x: 216.245,
                    y: 561.089,
                    sx: 0.938,
                    sy: 0.938
                }
            })
            .addTimedChild(instance16, 188, 52, {
                "188": {
                    x: 214.2,
                    y: 566.15
                }
            })
            .addTimedChild(instance17, 209, 10, {
                "209": {
                    x: 616.15,
                    y: 368.15,
                    sx: 0.303,
                    sy: 0.303
                },
                "210": {
                    x: 616.152,
                    y: 368.192,
                    sx: 0.427,
                    sy: 0.427
                },
                "211": {
                    x: 616.205,
                    y: 368.185,
                    sx: 0.551,
                    sy: 0.551
                },
                "212": {
                    x: 616.207,
                    y: 368.177,
                    sx: 0.675,
                    sy: 0.675
                },
                "213": {
                    x: 616.219,
                    y: 368.175,
                    sx: 0.799,
                    sy: 0.799
                },
                "214": {
                    x: 616.271,
                    y: 368.167,
                    sx: 0.923,
                    sy: 0.923
                },
                "215": {
                    x: 616.274,
                    y: 368.21,
                    sx: 1.046,
                    sy: 1.046
                },
                "216": {
                    x: 616.286,
                    y: 368.208,
                    sx: 1.17,
                    sy: 1.17
                },
                "217": {
                    x: 616.288,
                    y: 368.2,
                    sx: 1.294,
                    sy: 1.294
                },
                "218": {
                    x: 616.341,
                    y: 368.192,
                    sx: 1.418,
                    sy: 1.418
                }
            })
            .addTimedChild(instance18, 219, 2, {
                "219": {
                    x: 616.15,
                    y: 368.15,
                    sx: 1.542,
                    sy: 1.542
                },
                "220": {
                    x: 616.236,
                    y: 368.253,
                    sx: 1.48,
                    sy: 1.48
                }
            })
            .addTimedChild(instance19, 221, 2, {
                "221": {
                    x: 616.3,
                    y: 368.15,
                    sx: 1.418,
                    sy: 1.418
                },
                "222": {
                    x: 616.314,
                    y: 368.177,
                    sx: 1.48,
                    sy: 1.48
                }
            })
            .addTimedChild(instance20, 223, 17);
    });

    lib.tutorial_candy_game.assets = {
        "candy_tutorial_bg_1": "images/candy_tutorial_bg_1.jpg",
        "candy_tutorial_bg_2": "images/candy_tutorial_bg_2.jpg",
        "candy_tutorial_bg_3": "images/candy_tutorial_bg_3.jpg",
        "tutorial_candy_game_atlas_1": "images/tutorial_candy_game_atlas_1.json"
    };
})(PIXI, lib = lib || {});
var lib;
if (typeof module !== 'undefined' && module.exports) {
    module.exports = {
        stage: lib.tutorial_candy_game,
        background: 0xffffff,
        width: 1230,
        height: 720,
        framerate: 24,
        totalFrames: 240,
        library: lib
    };
}