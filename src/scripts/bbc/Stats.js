define(function ( require, exports, module )
{

 var LocalStorage    = require("fido/system/LocalStorage");
 //var GameConfig      = require('com/dangermouse/game/GameConfig');

 var Stats = function()
 {
	this.localStorage = new LocalStorage("com.bbc.worsewitch.game");


	this.tracked = {};
	this.firstClick = true;
	this.debug = false;
	this.levelPlaysHash = this.localStorage.getObject("plays");

	if(!this.levelPlaysHash) {
		this.levelPlaysHash = {};
		this.levelPlaysHash.game_play_count = 0;
		this.levelPlaysHash.minigames = {
			mission1: 0,
			mission2: 0,
			mission3: 0,
			mission4: 0,
			mission5: 0,
			mission6: 0
		};

		this.localStorage.storeObject("plays", this.levelPlaysHash);
	} else {
		this.levelPlaysHash.game_play_count++;
		this.localStorage.storeObject("plays", this.levelPlaysHash);
	}

	this.gmi = require('gmi');

	this.gameLocation = 'game';
	this.gameLevel = '_';
	this.gamePlayCount = this.levelPlaysHash.game_play_count;
}

Stats.prototype.init = function(appName) {
	return;
}

Stats.prototype.track = function(action_name, action_type, data)
{
	if(this.debug) console.log("TRACK ISTATS " + action_name, action_type, data);
	this.gmi.sendStatsEvent(action_name, action_type, data || {});
}

Stats.prototype.trackOnce = function(action_name, action_type, data)
{
 //if(window.og.noTracking)return;

	if(!this.firstClick)return;


	var key = action_name + action_type

	if(this.tracked[key])return;


	this.tracked[key] = true;
	this.gmi.sendStatsEvent(action_name, action_type, data || {});

	if(this.debug) console.log("TRACK ONCE " + action_name, action_type);

	this.firstClick = false;
}


 var map = [
	 ["tutorial"],
	 ["world_1", "world_2", "world_3", "world_4"],
	 ["world_5","world_6"],
			 ["world_7","world_8"]
 ];

 Stats.prototype.trackLevelStart = function()
 {
	 if(window.og.noTracking)return;
	 return;

			 var key = this.gameLevel;//world + " : " + GameConfig.levelIndex;

	 if(!this.levelPlaysHash[key])
	 {
		 this.levelPlaysHash[key] = 0;
	 }

	 this.levelPlaysHash[key]++;

	 this.localStorage.storeObject("plays", this.levelPlaysHash);


	 this.trackWithLevel('started', {
		 game_play_count:this.gamePlayCount,
		 level_play_count:this.levelPlaysHash[key]
	 });
 }

 Stats.prototype.trackMiniGame = function(data, idGame){
	if(window.og.noTracking)return;

	var gameName = idGame.replace('mission', 'minigame');

	this.levelPlaysHash.minigames[idGame]++;
	this.localStorage.storeObject("plays", this.levelPlaysHash);

	data.game_play_count = this.gamePlayCount;
	data.minigame_name = gameName;
	data.minigame_play_count = this.levelPlaysHash.minigames[idGame];
	this.track('game_level', 'minigame_started', data);
 };


 Stats.prototype.trackMiniGameComplete = function(data, idGame) {
 	var gameName = idGame.replace('mission', 'minigame');

 	this.levelPlaysHash.minigames[idGame]++;
 	this.localStorage.storeObject("plays", this.levelPlaysHash);

 	data.game_play_count = this.gamePlayCount;
 	data.minigame_name = gameName;
 	data.minigame_difficulty = 1;
 	data.minigame_play_count = this.levelPlaysHash.minigames[idGame];

 	this.track('game_level', 'minigame_complete', data);
 }

 Stats.prototype.trackWithLevel = function(action_name, data)
 {

	 if(window.og.noTracking)return;

			 data = data || {};

			 data.game_level_name = this.gameLevel;
			 data.game_play_count = this.gamePlayCount;


	 this.gmi.sendStatsEvent('game_level', action_name, data);

	 console.log(">>>>>>>>>>>>>>>>>> TRACK >>>>>>>>>>>>>>>>>")
	 console.log("action_name: " + action_name)
	 console.log("action_type: " + 'game_level')
	 console.log(data)
	 console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
 }

 Stats.prototype.trackClickFromScreen = function(action_name, data)
 {
	data = data || {};

	data.game_screen = this.game_screen;

	this.gmi.sendStatsEvent('game_click', action_name, data);

	if(this.debug) console.log("TRACK" + action_name)
 }

 Stats.prototype.getLevelPlayCount = function()
 {
	 if(!this.levelPlaysHash[key])
	 {
		 this.levelPlaysHash[key] = 0;
	 }

	 return this.levelPlaysHash[key];
 }

 Stats.prototype.trackClickWithLevel = function(action_name, data)
 {
	 if(window.og.noTracking)return;
			 var world = map[GameConfig.storyIndex][GameConfig.worldIndex];

			 if(window.MINI)
			 {
				 world = 'mini';
			 }

			 data = data || {};

			 data.game_level_name = world;
			 data.game_world_level = GameConfig.levelIndex + 1;
			 data.level_play_count = this.getLevelPlayCount();

	 this.gmi.sendStatsEvent(action_name, 'game_click', data);

	 if(this.debug) console.log("TRACK" + action_name)
 }


 module.exports = new Stats();
});
