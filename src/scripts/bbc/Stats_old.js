 define(function ( require, exports, module )
 {

 	var LocalStorage    = require("fido/system/LocalStorage");
 	//var GameConfig      = require('com/dangermouse/game/GameConfig');

 	var Stats = function()
 	{
 		this.localStorage = new LocalStorage("com.bbc.dumping_ground.game");


 		this.echo = null;
 		this.tracked = {};
 		this.firstClick = true;


 		this.levelPlaysHash = this.localStorage.getObject("plays");

 		if(!this.levelPlaysHash)
        {
 			this.levelPlaysHash = {};
            this.localStorage.storeObject("plays", this.levelPlaysHash);
        }
 	}

 	Stats.prototype.init = function(appName)
 	{
 		return;
 		if(window.og.noTracking)return;

 		var Echo = null;

 		Echo = window.echo;// require("echo");
 		//alert("!!")
	    var EchoClient = Echo.EchoClient,   // Echo Client class
	        Enums = Echo.Enums,             // Enums
	        ConfigKeys = Echo.ConfigKeys;   // Key names to use in config

	    var env = window.og.environment;
	    if (env !== "live" && env !== "stage")
	    {
	        Echo.Debug.enable();
	    }

	    var echoConf = {};
	    var istatsPath = (env !== "live") ? env : "bbc";
	    echoConf[ConfigKeys.COMSCORE.URL] = 'http://sa.bbc.co.uk/bbc/'+istatsPath+'/s';
	    echoConf[ConfigKeys.RUM.ENABLED] = false;

	    var echo = new EchoClient(
	        appName,
	        Enums.ApplicationType.WEB,
	        echoConf);

	    if (window.bbccookies && !window.bbccookies.isAllowed("ckpf_whatever"))
	    {
	        // Call this method if the user has opted out of (performance) cookies
	        echo.optOutOfCookies();
	    }

	    this.echo = echo;

	    this.echo.viewEvent(window.og.embedVars.statsCounterName)
	    //"cbbc.games.danger_mouse_game.page");
 	}

 	Stats.prototype.track = function(action_name, action_type, data)
 	{
 		if(window.og.noTracking)return;

 		//return
 		this.echo.userActionEvent(action_name, action_type, data || {});
 		console.log("TRACK" + action_name)
 	}

 	Stats.prototype.trackOnce = function(action_name, action_type, data)
 	{
 		if(window.og.noTracking)return;

 		//return
 		if(!this.firstClick)return;

 		var key = action_name + action_type

 		//if(this.tracked[key])return;

// 		this.tracked[key] = true;
 		this.echo.userActionEvent(action_name, action_type, data || {});

 		console.log("TRACK" + action_name)

 		this.firstClick = false;
 	}


 	var map = [
 		["tutorial"],
 		["world_1", "world_2", "world_3", "world_4"],
 		["world_5","world_6"],
        ["world_7","world_8"]
 	];

 	Stats.prototype.trackLevelStart = function(action_name, data)
 	{
 		if(window.og.noTracking)return;
 		return;

 		var world = map[GameConfig.storyIndex][GameConfig.worldIndex];
        var key = world + " : " + GameConfig.levelIndex;

 		if(!this.levelPlaysHash[key])
 		{
 			this.levelPlaysHash[key] = 0;
 		}

 		this.levelPlaysHash[key]++;

 		this.localStorage.storeObject("plays", this.levelPlaysHash);

 		console.log(this.levelPlaysHash);

 		this.trackWithLevel('started', {
 			game_play_count:this.levelPlaysHash[key]
 		});
 	}

 	Stats.prototype.trackWithLevel = function(action_name, data)
 	{
 		if(window.og.noTracking)return;
        var world = map[GameConfig.storyIndex][ GameConfig.worldIndex ];

        data = data || {};

        if(window.MINI)
        {
        	world = 'mini';
        }

        data.game_level_name = world;
        data.game_world_level = GameConfig.levelIndex + 1;

 		this.echo.userActionEvent(action_name, 'game_level', data);

 		console.log(">>>>>>>>>>>>>>>>>> TRACK >>>>>>>>>>>>>>>>>")
 		console.log("action_name: " + action_name)
 		console.log("action_type: " + 'game_level')
 		console.log(data)
 		console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
 	}

 	Stats.prototype.trackClickWithLevel = function(action_name, data)
 	{
 		if(window.og.noTracking)return;
        var world = map[GameConfig.storyIndex][GameConfig.worldIndex];

        if(window.MINI)
        {
        	world = 'mini';
        }

        data = data || {};

        data.game_level_name = world;
        data.game_world_level = GameConfig.levelIndex + 1;

 		this.echo.userActionEvent(action_name, 'game_click', data);
 		console.log("TRACK" + action_name)
 	}


 	module.exports = new Stats();
});
