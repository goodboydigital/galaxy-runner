
import gmi_platform from './gmi-platform';
let instance;

function getGMI() {
	if(!instance) {
		try {
			instance = gmi_platform.getGMI();
		} catch (e) {
			console.log(e);
			return null;
		}
	}

	return instance;
}


export default getGMI;