 define(function ( require, exports, module )
 {

    var LocalStorage    	= require("fido/system/LocalStorage");
 	var Ticker              = require('fido/system/Ticker');
    var VisibilityChecker   = require('fido/system/VisibilityChecker');
    var Stats   			= require('com/bbc/Stats');

 	//var GameConfig      = require('com/dangermouse/game/GameConfig');

 	var Heartbeat = function()
 	{
 		this.frequency =15;

 		this.trackObj = {heartbeat_period:this.frequency};
 		this.timer = 0;

    	VisibilityChecker.onShow.add(this.onShow, this);
 		VisibilityChecker.onHide.add(this.onHide, this);

 		this.active = false;
 		this.onShow();
 	}

 	Heartbeat.prototype.update = function()
 	{

        var currentTime = new Date();
        var timeElapsed = currentTime - this.lastTime;

        var difference = (timeElapsed) / 1000;

        this.timer += difference;

        if(this.timer >= this.frequency)
        {
        	this.trackObj.game_screen = Stats.gameLocation;
        	this.trackObj.game_level_name = Stats.gameLevel;
        	this.trackObj.game_play_count = Stats.gamePlayCount;

          console.log(this.trackObj);
            Stats.track("timer", "heartbeat", this.trackObj);
        //    console.log(this.frequency + ' SECONDS');
            this.timer -= this.frequency;
        }

        this.lastTime = currentTime;


 	}

 	Heartbeat.prototype.onShow = function()
 	{
 		if(this.active)return;
 		this.active = true;

 		this.lastTime = new Date();
       	Ticker.instance.add(this.update, this);
 	}

	Heartbeat.prototype.onHide = function()
 	{
 		if(!this.active)return;
 		this.active = false;

       	Ticker.instance.remove(this.update, this);
 	}

 	module.exports = new Heartbeat();
});
