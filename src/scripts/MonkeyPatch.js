import * as PIXI from 'pixi.js';

let InteractionManager = PIXI.interaction.InteractionManager;
InteractionManager.prototype.mapPositionToPoint = function(point, x, y)
    {
        let rect;

        // IE 11 fix
        if (!this.interactionDOMElement.parentElement)
        {
            rect = { x: 0, y: 0, width: 0, height: 0 };
        }
        else
        {
            rect = this.interactionDOMElement.getBoundingClientRect();
        }

		let angle = 90 * Math.PI / 180;

		// let c1 = { x: rect.left, y: rect.top};
		// let c2 = { x: rect.width, y: rect.height};
		// let c1R = {};
		// let c2R = {};
		//
		// c1R.x = (c1.x - xc) * Math.cos(angle) + xc  -  (c1.y - yc) * Math.sin(angle);
		// c1R.y = (c1.y - yc) * Math.cos(angle) + yc + (c1.x - xc) * Math.sin(angle);
		//
		// c2R.x = Math.abs((c2.x - xc) * Math.cos(angle) + xc  -  (c2.y - yc) * Math.sin(angle));
		// c2R.y = (c2.y - yc) * Math.cos(angle) + yc + (c2.x - xc) * Math.sin(angle);
		//
        const resolutionMultiplier = navigator.isCocoonJS ? this.resolution : (1.0 / this.resolution);

		if(window.is90degree)
		{
			point.x = ((y - rect.top) * (this.interactionDOMElement.height / rect.width)) * resolutionMultiplier
			point.y = (this.interactionDOMElement.height - ((x - rect.left) * (this.interactionDOMElement.width / rect.height))) * resolutionMultiplier;
		}
		else
		{
			point.x = ((x - rect.left) * (this.interactionDOMElement.width / rect.width)) * resolutionMultiplier;
        	point.y = ((y - rect.top) * (this.interactionDOMElement.height / rect.height)) * resolutionMultiplier;
		}
		// point.y = ((x - rect.left) * (this.interactionDOMElement.width / rect.height)) * resolutionMultiplier;
    }
