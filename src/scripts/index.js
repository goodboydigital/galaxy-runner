import gsap from 'gsap';
import * as PIXI from 'pixi.js';
window.PIXI = PIXI;
import Device from 'fido/system/Device';
import App from 'app/Application';
import gmi from 'gmi';
import style from '../styles/index.scss';
import WebglSupport from 'fido/app/WebglSupport';
import Easings from 'app/utils/Easings';
import MonkeyPatch from './MonkeyPatch';


if (!window.og) {
	window.og = {
		gid: 'countdown-test',
		gameDir: '',
		gameContainerId: 'og-game-holder',
		isFullScreen: window.self === window.top,
		exitGameUrl: 'http://bbc.co.uk/',
		environment: 'test',
		embedVars: {
			statsCounterName: 'testCounterName',
			statsAppName: 'TestAppName'
		}
	};

}
if (!window.og.embedVars) {
	window.og.embedVars = { statsAppName: 'worst-wtich' };
}


window.ASSET_URL = 'assets/';

// useless i think (jordan)
window.config = {
	app: 'com/__PACKAGE_NAME__/app/__APP_ROOT_CLASS_NAME__',
};

// make sure console still works in ie9!
if (!window.console) {
	window.console = { log: function (str) { } };
}
if (!window.console.log) {
	window.console.log = function (str) { };
}

function hasCanvasSupport() {
	var elem = document.createElement('canvas');
	return !!(elem.getContext && elem.getContext('2d'));
}

window.DEVICE_SCALE = window.devicePixelRatio || 1;

window.initGame = function () {
	const webglSupport = new WebglSupport();
	console.log('hello', webglSupport.supported);


	if (webglSupport.supported) {
		window.ASSET_URL = gmi().gameDir + 'assets/';
		window.Easings = Easings.instance;

		let containerId = 'og-game-holder';


		const app = new App();
		window.app = app;

		console.log('Container id :', containerId);

		this.renderer = new PIXI.Renderer(1000,1000, {
            backgroundColor: 0x000000,
        });
        window.renderer = this.renderer;
        this.renderer.view.className = 'experiment';
		document.body.appendChild(this.renderer.view);
		

		// const container = document.getElementById(containerId);
		// container.appendChild(app.view);

		// container.style.background = 'black';

		app.view.style.position = 'absolute';
		app.view.style.top = 0;
		app.view.style.left = 0;

		function resize() {
			const container = document.getElementById('container');
			window.scrollTo(0, 0);
			app.resize(window.innerWidth, window.innerHeight);
		}

		window.addEventListener('resize', function () {
			resize();
		});

		resize();
	}
};

window.initGame()
