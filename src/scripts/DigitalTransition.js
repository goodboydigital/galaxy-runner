import * as PIXI from 'pixi.js';


class DigitalTransition
{
    constructor(){

        this.bg1 = new PIXI.Graphics().beginFill(0x000000).drawRect(0,0,100, 100);
        this.bg2 = new PIXI.Graphics().beginFill(0x000000).drawRect(0,0,100, 100);
        this.white = new PIXI.Graphics().beginFill(0xFFFFFF).drawRect(0,0,100, 100);

        this.white.blendMode = PIXI.BLEND_MODES.ADD;
        this.circle = new PIXI.Graphics().beginFill(0xFF000).drawCircle(0, 0, 100);
    }

    begin(screenManager, currentScreen, nextScreen)
    {
        this.screenManager = screenManager;

        this.currentScreen = currentScreen;
        this.nextScreen = nextScreen;


       // this.nextScreen.visible = false;
      //  screenManager.container.addChild(this.circle);
     //   screenManager.container.mask = this.circle;


        this.container = new PIXI.Container();
        this.container.addChild(this.white);
        this.container.addChild(this.bg1);
        this.container.addChild(this.bg2);
        this.container.alpha = 0;
        this.white.alpha = 0;
        screenManager.container.addChild(this.container);

        this.circle.scale.set(this.targetScale);

        if(this.currentScreen)
        {
            if(this.currentScreen.onHide)this.currentScreen.onHide();
            TweenLite.to(this.container, 1, {alpha:1, ease:Sine.easeOut, onComplete:this.onFadeout.bind(this)});
        }
        else
        {
            this.onFadeout();
        }
    }

    onFadeout()
    {
        if(this.currentScreen)
        {
            if(this.currentScreen.onHidden)this.currentScreen.onHidden();
            this.screenManager.container.removeChild(this.currentScreen);
        }

        //this.nextScreen.alpha = 0;
        this.white.alpha = 1;

        if(this.nextScreen.onShow)this.nextScreen.onShow();
        if(this.nextScreen.resize)this.nextScreen.resize(this.screenManager.w, this.screenManager.h);

        this.screenManager.container.addChildAt(this.nextScreen, 0);

        TweenLite.to(this.bg1.pivot, 1.2, {y:500, ease:Cubic.easeIn, onComplete:this.onFadein.bind(this)});
        TweenLite.to(this.bg2.pivot, 1.2, {y:-500, ease:Cubic.easeIn, onComplete:this.onFadein.bind(this)});
        TweenLite.to(this.white, 1.2, {alpha:0, ease:Cubic.easeIn, onComplete:this.onFadein.bind(this), onComplete:this.onFadein.bind(this)});

    }

    onFadein()
    {
        if(this.nextScreen.onShown)this.nextScreen.onShown();


        this.screenManager.container.removeChild(this.circle);
        this.screenManager.container.mask = null;
        this.screenManager.onTransitionComplete();
    }

    onResize(w, h)
    {
        this.bg1.scale.x = w/100;
        this.bg1.scale.y = h/100/2;

        this.bg2.scale.x = w/100;
        this.bg2.scale.y = h/100/2;

        this.white.scale.x = w/100;
        this.white.scale.y = h/100;

        this.bg2.y = h/2;
        this.w = w;
        this.h = h;
      //  console.log(this.targetScale, w)
        this.targetScale = (w/100) * 1.2;
        this.circle.x = w/2;
        this.circle.y = h/2;
    }
}




export default DigitalTransition;
