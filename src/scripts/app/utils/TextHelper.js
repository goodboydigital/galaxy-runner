import * as PIXI from 'pixi.js';

const hudText = {
    font:'30px Audiowide'
}

const hudHighscoreText = {
    font:'28px Audiowide'
}


export default class TextHelper {

    static getHudText( copy )
    {
        return new PIXI.extras.BitmapText( copy, hudText );
    }

    static getHudHighscoreText( copy )
    {
        return new PIXI.extras.BitmapText( copy, hudHighscoreText );
    }
}
