
import * as PIXI from 'pixi.js';
import Ticker from 'fido/system/Ticker';

export default class DelayedCalls
{
  constructor(ticker){

    this.ticker = ticker || Ticker.instance;
    this.delayedCalls = [];
  }

  clear(){
    this.delayedCalls = [];
    this.ticker.remove(this.update, this);
    this.updating = false;
  }

  add(func, delay, args)
  {
    var obj = {
      func: func,
      args: args,
      tick: 0,
      delay: delay,
      delete: false
    }
    this.delayedCalls.push(obj)

    if(!this.updating && !this.paused) {
      this.ticker.add(this.update, this);
      this.updating = true
    }

    // return the id of the current delay, like that we can track it back if we want from the caller
    return this.delayedCalls.length-1;
  }

  getRemainingTime(id){
    var obj = this.delayedCalls[id];

    if(obj && obj.delay){
      return (obj.delay * 60 - obj.tick);
    }
    else {
      return 0;
    }
  }

  pause()
  {
      // if(this.updating) {
      console.log('DELYAED CALLS pause');
        this.paused = true;
        this.ticker.remove(this.update, this);
        this.updating = false;
      // }
  }

  resume()
  {
      if(this.delayedCalls.length > 0) {
          console.log('DELYAED CALLS resume');
          this.ticker.add(this.update, this);
          this.updating = true;
      }

      this.paused = false;
  }

  update()
  {

    for (var i = 0; i < this.delayedCalls.length; i++) {
      var c = this.delayedCalls[i];
      c.tick++;
      if(c.tick > c.delay * 60){
        c.func(c.args);
        c.delete = true;
      }
    }

    for (var i = 0; i < this.delayedCalls.length; i++) {
      var c = this.delayedCalls[i];
      if(c.delete){
        this.delayedCalls.splice(i, 1);
        i--;
      }
    }

    if(this.delayedCalls.length === 0) {
      this.ticker.remove(this.update, this);
      this.updating = false;
    }
  }

}
