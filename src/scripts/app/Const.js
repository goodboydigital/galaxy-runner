import Device from 'fido/system/Device';

export const GEL_PADDING_TOP = Device.instance.isMobile ? 25 : 30;
export const GEL_PADDING_LEFT = Device.instance.isMobile ? 25 : 30;
export const GEL_PADDING_RIGHT = Device.instance.isMobile ? 25 : 30;
export const GEL_PADDING_BOTTOM = Device.instance.isMobile ? 25 : 40;
export const GEL_PADDING_BETWEEN_BUTTON = Device.instance.isMobile ? 20 : 0;
export const GEL_TITLE_PADDING_TOP = Device.instance.isMobile ? 25 : 30;
