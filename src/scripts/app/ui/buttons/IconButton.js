/**
* @author Mat Groves
*/
import * as PIXI from 'pixi.js';
import DefaultButton from 'fido/ui/buttons/DefaultButton';
import DefaultButtonChristmas from 'app/ui/buttons/DefaultButtonChristmas';
import LabelButton from './LabelButton';


const IconButton = {
	inGameButton : function (iconFrame, accessibleTitle) {
		const button = new DefaultButtonChristmas({
			textures: {
				normal:'direction.png',
				down: 'direction.png',
				hover: 'direction.png',
				icon: iconFrame ? iconFrame : null,
			}
		});
		// const button = new DefaultButton('bg_primary_up.png', 'bg_primary_over.png', 'bg_primary_over.png', iconFrame ? iconFrame : null);
		button.setSize(0.7);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	primary : function (iconFrame, accessibleTitle) {
		const button = new DefaultButton({
			textures: {
				normal:'bg_primary_up.png',
				down: 'bg_primary_over.png',
				hover: 'bg_primary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		});
		// const button = new DefaultButton('bg_primary_up.png', 'bg_primary_over.png', 'bg_primary_over.png', iconFrame ? iconFrame : null);
		button.setSize(0.7);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	primaryMedium : function (iconFrame, accessibleTitle) {
		const button = new DefaultButton({
			textures: {
				normal:'bg_primary_up.png',
				down: 'bg_primary_over.png',
				hover: 'bg_primary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		});
		button.setSize(1.2);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	primaryBig : function (iconFrame, accessibleTitle) {
		const button = new DefaultButton({
			textures: {
				normal:'bg_primary_up.png',
				down: 'bg_primary_over.png',
				hover: 'bg_primary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		});

		button.setSize(1.5);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	secondary : function (iconFrame, accessibleTitle) {
		const button = new DefaultButton({
			textures: {
				normal:'bg_primary_up.png',
				down: 'bg_primary_over.png',
				hover: 'bg_primary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		});

		button.setSize(0.7);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	tertiary : function (iconFrame, accessibleTitle) {
		const button = new DefaultButton({
			textures: {
				normal:'bg_secondary_up.png',
				down: 'bg_secondary_over.png',
				hover: 'bg_secondary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		});

		button.setSize(0.7);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	primaryRect : function(label, iconFrame, accessibleTitle){
		const button = new LabelButton({
			textures: {
				normal:'bg_rect_primary_up.png',
				down: 'bg_rect_primary_over.png',
				hover: 'bg_rect_primary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		},
		label);
		// const button = new LabelButton('bg_rect_primary_up.png', 'bg_rect_primary_over.png', 'bg_rect_primary_over.png', label, iconFrame);
		// button.defaultScale = .6;
		button.setSize(0.7);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	primaryRectInversed : function(label, iconFrame, accessibleTitle){
		// const button = new LabelButton('bg_rect_primary_up.png', 'bg_rect_primary_over.png', 'bg_rect_primary_over.png', label, iconFrame, true);
		const button = new LabelButton({
			textures: {
				normal:'bg_rect_primary_up.png',
				down: 'bg_rect_primary_over.png',
				hover: 'bg_rect_primary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		}, label, true);
		button.setSize(0.7);

		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	secondaryRect : function(label, iconFrame, accessibleTitle){
		// const button = new LabelButton('bg_rect_secondary_up.png', 'bg_rect_secondary_over.png', 'bg_rect_secondary_over.png', label, iconFrame);
		const button = new LabelButton({
			textures: {
				normal:'bg_rect_secondary_up.png',
				down: 'bg_rect_secondary_over.png',
				hover: 'bg_rect_secondary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		}, label);
		// button.defaultScale = .6;
		button.setSize(0.7);
		button.view.accessibleTitle = accessibleTitle;
		return button;
	},
	secondaryRectInversed : function(label, iconFrame, accessibleTitle){
		// const button = new LabelButton('bg_rect_secondary_up.png', 'bg_rect_secondary_over.png', 'bg_rect_secondary_over.png', label, iconFrame, true);
		const button = new LabelButton({
			textures: {
				normal:'bg_rect_secondary_up.png',
				down: 'bg_rect_secondary_over.png',
				hover: 'bg_rect_secondary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		}, label, true);
		button.setSize(0.7);

		button.view.accessibleTitle = accessibleTitle;
		return button;
	},


	/*
	* @param  {String} iconFrame
	* @return {DefaultButton}
	*/
	close : function (iconFrame) {

		// const button = new DefaultButton('redbutton_up.png', 'redbutton_down.png', 'redbutton_over.png', iconFrame ? iconFrame : null);
		const button = new DefaultButton({
			textures: {
				normal:'bg_secondary_up.png',
				down: 'bg_secondary_over.png',
				hover: 'bg_secondary_over.png',
				icon: iconFrame ? iconFrame : null,
			}
		});
		button.setSize(1);


		if(iconFrame)
			{
					// button.icon.pivot.y = 13;
		}

		return button;
	}
};

module.exports = IconButton;
