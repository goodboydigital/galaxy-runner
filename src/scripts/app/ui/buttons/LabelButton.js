import * as PIXI from 'pixi.js';
import Signal          from 'signals';
import DefaultButton   from 'fido/ui/buttons/DefaultButton';
import Utils from 'fido/utils/Utils';
import SoundManager    from 'fido/sound/SoundManager';


class LabelButton extends DefaultButton {

	constructor({ textures, sounds}, labelText, inversed) {

		super({ textures, sounds});
		this.label = new PIXI.Text(labelText, {align:'right',fill:'white', font:'40px helvetica'});
		console.log(this.view.height);
      // this.label = new PIXI.extras.BitmapText(labelText, {align:"right", font:"40px Londrina Solid"});
		this.label.anchor.set(0.5);
		this.label.position.y = 0 ;

		Utils.resizeText(this.label, 150);
		this.view.addChild(this.label);


		// this.view.hitArea = new PIXI.Rectangle(0, 0, 100, 100);


		if(textures.icon){
			this.label.position.x = -20;

			if(inversed){
				this.label.position.x = 20;
			}
		}


		if(inversed){
			this.icon.position.x = -this.view.width/3;
		}
		else {
			this.icon.position.x = this.view.width/3;
		}


	}



}


export default LabelButton;
