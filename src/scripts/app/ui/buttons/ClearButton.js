// ClearButton.js
import * as PIXI from 'pixi.js';
import Signal from 'signals';
import AbstractButton from 'fido/ui/buttons/AbstractButton';

class ClearButton extends AbstractButton {
	constructor(title) {
		const view = new PIXI.Sprite.from('switch_off.png');
		super(view);

		view.accessible  = true;
		view.accessibleTitle  = title;

	}

	reset() {
		this.view.texture = PIXI.Texture.from('switch_off.png');
	}

	tick() {
		this.view.texture = PIXI.Texture.from('switch_on.png');
	}
}


export default ClearButton;
