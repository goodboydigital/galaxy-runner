/**
 * @author Mat Groves
 */

import * as PIXI from 'pixi.js';
import AbstractButton from 'fido/ui/buttons/AbstractButton';
import Signal from 'signals';

export default class ToggleButton extends AbstractButton{
	constructor(value, title) {
		const view = new PIXI.Sprite.from('switch_off.png');

		super(view);
		value = !!value;


		this.on = false;
		view.accessible  = true;
		view.accessibleTitle  = title;
		this.onToggle = new Signal();

		this.toggle(value);
	}
	toggle(value) {
		if(this.on === value)return;

		this.on = value;

		if(value)
		{
			this.view.texture = PIXI.Texture.from('switch_on.png');
			this.onToggle.dispatch(true);
		}
		else
		{
			this.view.texture = PIXI.Texture.from('switch_off.png');
			this.onToggle.dispatch(false);

		}
	}
}
