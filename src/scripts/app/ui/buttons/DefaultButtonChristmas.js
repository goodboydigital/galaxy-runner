
import * as PIXI from 'pixi.js';
import DefaultButton from 'fido/ui/buttons/DefaultButton';

export default class DefaultButtonChristmas extends DefaultButton
{
    constructor(data) {

        super(data);
    }

    _onDown()
    {
        if(this.isOnlyIcon || this._isDown) return;

        this._isDown = true;
        this.view.y += this.offsetDown;


        this.view.texture = PIXI.Texture.from(this.downTexture);
    }

    _onHover()
    {
        if(this.isOnlyIcon) return;
        this.view.texture = PIXI.Texture.from(this.hoverTexture);
    }

    _onMouseOut()
    {
        if(this.isOnlyIcon) return;

        if(this.icon)
        {
            this.icon.y = 0;
        }

        this.view.texture = PIXI.Texture.from(this.normalTexture);
    }

    _onUp()
    {
        if(this.isOnlyIcon || !this._isDown) return;

        this.view.y -= this.offsetDown;
        this._isDown = false;

        this.view.texture = PIXI.Texture.from(this.normalTexture);
    }
}
