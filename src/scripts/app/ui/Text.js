import { Text } from 'PIXI';
module.exports  = {
	h1 : (string) =>  {
		const text = new Text(string, {fill: 'white', fontSize: 32, fontFamily:window.MAIN_FONT, stroke:true, strokeThickness:8, lineJoin:'round'});
		return text;
	},
	h2 : (string) =>  {
		const text = new Text(string, {fill: 'white', fontSize: 25, fontFamily:window.MAIN_FONT, stroke:true, strokeThickness:8, lineJoin:'round'});
		return text;
	},
	h3 : (string) =>  {
		const text = new Text(string, {fill: 'white', align: 'center',fontSize: 18, fontFamily:window.MAIN_FONT, stroke:true, strokeThickness:8, lineJoin:'round'});
		return text;
	},
	p : (string) =>  {
		const text = new Text(string, {fill: 'white', align: 'center',fontSize: 16, fontFamily:window.MAIN_FONT, stroke:true, strokeThickness:8, lineJoin:'round'});
		return text;
	}
};
