import * as PIXI from 'pixi.js';
import FidoApp from 'fido/app/App';
import Ticker from 'fido/system/Ticker';
import Device from 'fido/system/Device';
import SoundManager from 'fido/sound/SoundManager';
import LoaderScreen from 'screens/LoaderScreen';
import Cache from 'fido/loader/Cache';
import Utils from 'fido/utils/Utils';
import TitleScreen from 'screens/TitleScreen';
import GameScreen from 'screens/GameScreen';
import TopMenu from './TopMenu';
import OverlayManager from 'overlay/OverlayManager';
import gmi from 'gmi';
import Save from './system/Save';
import SaveMixin from './system/SaveMixin';
// import Brim from 'app/Brim';
import Assets from './Assets';
import gboLoader from './gboLoader';

const Manifests = require('./manifests/manifest.json');
const ManifestsObj = require('./manifests/manifest-obj.json');
const ManifestsAudio = require('./manifests/manifest-audio.json');

class Application extends FidoApp {
	constructor() {
		var options = {
			orientationMode: 2,
			config: window.ASSET_URL + 'json/config.json',
			loaderScreen: LoaderScreen
		};
		super(options);


		this.safeSize =
			{
				width: 960,
				height: 560
			};

		this.maxSize =
			{
				width: 1224,
				height: 626
			};

		console.log(Manifests.preload);

		const Resource = PIXI.LoaderResource;

		Resource.setExtensionXhrType('gbo', Resource.XHR_RESPONSE_TYPE.BUFFER);

		// Load what we need....
		this.loader = new PIXI.Loader()
		this.loader.use(gboLoader());
		// .addManifest(Manifests.preload, window.ASSET_URL)
		// .addManifest(Manifests.default, window.ASSET_URL)
		// .addManifest(ManifestsObj, window.ASSET_URL)
		// .addAudioManifest(ManifestsAudio, window.ASSET_URL)
		// .addPixiAssets([
		// 	window.ASSET_URL + 'font/score_font.xml',
		// ])
		// .addJson(`${window.ASSET_URL}json/save.json`, 'save');

		for (let index = 0; index < Manifests.preload.length; index++) {
			const element = Manifests.preload[index];
			// console.log(element, 'assets'/+element);

			this.loader.add('assets/' + element)
		}
		for (let index = 0; index < Manifests.default.length; index++) {
			const element = Manifests.default[index];
			this.loader.add('assets/' + element)
		}
		for (let index = 0; index < ManifestsObj.length; index++) {
			const element = ManifestsObj[index];
			this.loader.add('assets/' + element)
		}
		// listen for when everything is up and running..
		// this.onReady.add(this.onAppReady, this);
		this.loader.load(this.onAppReady)

		// startup the app.. when it is ready to go onReady will be fired..
		// this.startup();

		this._hasSetupSave = false;
	}


	onAppReady() {

		//	INIT GMI
		gmi().gameLoaded();

		// this.logo = new PIXI.Sprite.from('goodboy.png');
		// this.logo.anchor.set(1);
		// //this.logo.scale.set(.8);
		// this.logo.interactive = true;
		// this.logo.buttonMode = true;
		// this.logo.tap = this.logo.click = () => {
		// 	window.open('http://www.goodboydigital.com/', '_blank');
		// };

		// this.stage.addChild(this.logo);


		//	INIT SAVE
		// if (!this._hasSetupSave) {
		// 	Save.instance = new Save('com.goodboy.boilerplate');
		// 	SaveMixin(Save.instance);
		// 	const gameSave = Cache.getJson('save');
		// 	Save.instance.load(gameSave.save);

		// 	this._hasSetupSave = true;

		// 	Save.instance.setCredits(Math.floor(Math.random() * 20) + 20);
		// }

		// startup the app.. when it is ready to go onReady will be fired..

		this.gameScreen = new GameScreen(this);

		this.screenManager.addScreen(this.gameScreen, 'game');

		this.topMenu = new TopMenu(this);
		//this.stage.addChild(this.topMenu);

		this.overlayManager = new OverlayManager(this);
		this.stage.addChild(this.overlayManager.view);


		this.screenManager.gotoScreenByID('game');



		SoundManager.play('audio/soundtrack', { loop: true });



		this.resize(this.w, this.h);
	}


	resize(w, h) {
		//todo... make this resize code more flexible!
		this.w = w;
		this.h = h;

		var scale = 1;
		if (Device.instance.isMobile) {
			if (window.devicePixelRatio) {
				scale = window.devicePixelRatio;
			} else {
				scale = window.screen.deviceXDPI / window.screen.logicalXDPI;
			}
		}
		if (scale > 2) scale = 2;

		var ratio = w / (this.safeSize.width) < h / (this.safeSize.height) ? w / (this.safeSize.width) : h / (this.safeSize.height);
		var w2 = Math.min(this.maxSize.width * ratio, w);
		var h2 = Math.min(this.maxSize.height * ratio, h);

		this.renderer.resize((w2 * scale) | 0, (h2 * scale) | 0);

		this.view.style.width = w2 + 'px';
		this.view.style.height = h2 + 'px';

		this.view.style.left = w / 2 - (w2) / 2 + 'px';
		this.view.style.top = h / 2 - (h2) / 2 + 'px';

		this.screenManager.resize(w2 / ratio, h2 / ratio);

		if (this.overlayManager) {
			this.overlayManager.resize(w2 / ratio, h2 / ratio);
			this.overlayManager.view.scale.set(ratio * scale);

		}

		if (this.topMenu) {
			this.topMenu.scale.set(ratio * scale);
			this.topMenu.resize(w2 / ratio, h2 / ratio);
		}

		this.screenManager.container.scale.set(ratio * scale);

		window.is90degree = false;

		if (h > w) {
			this.view.style.webkitTransform = 'scale(' + 1 / (window.innerWidth / h) + ') rotate(' + 90 + 'deg)';
			this.view.style.mozTransform = 'scale(' + 1 / (window.innerWidth / h) + ') rotate(' + 90 + 'deg)';
			this.view.style.msTransform = 'scale(' + 1 / (window.innerWidth / h) + ') rotate(' + 90 + 'deg)';
			this.view.style.oTransform = 'scale(' + 1 / (window.innerWidth / h) + ') rotate(' + 90 + 'deg)';
			this.view.style.transform = 'scale(' + 1 / (window.innerWidth / h) + ') rotate(' + 90 + 'deg)';
			window.is90degree = true;
		}
		else {
			this.view.style.webkitTransform = 'scale(1) rotate(' + 0 + 'deg)';
			this.view.style.mozTransform = 'scale(1) rotate(' + 0 + 'deg)';
			this.view.style.msTransform = 'scale(1) rotate(' + 0 + 'deg)';
			this.view.style.oTransform = 'scale(1) rotate(' + 0 + 'deg)';
			this.view.style.transform = 'scale(1) rotate(' + 0 + 'deg)';

			if (!this.brimInitialised && navigator.userAgent.indexOf('Safari/') !== -1) {

				// Brim.reset(window.ASSET_URL + 'image/device/brim.jpg');
				this.brimInitialised = true;
			}

		}

		if (this.logo) {
			this.logo.position.x = (w2 * scale) - 30;
			this.logo.position.y = (h2 * scale) - 30;
		}

	}

}


module.exports = Application;
