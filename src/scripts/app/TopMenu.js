import * as PIXI from 'pixi.js';
import IconButton      from 'buttons/IconButton';
import SoundManager    from 'fido/sound/SoundManager';
import Device          from 'fido/system/Device';
import getGMI 		   from 'gmi';
let gmi;

import { GEL_PADDING_LEFT, GEL_PADDING_RIGHT, GEL_PADDING_TOP, GEL_PADDING_BETWEEN_BUTTON } from 'Const';

export default class TopMenu extends PIXI.Container {
	constructor(app) {
		super();
		window.topMenu = this;
		this.app = app;
		this.state = 'home';
		this.previousState = this.state;
		/*
		 * All the states
		*/
		this.states = {
			home:[
				'exit', 'mute', 'settings',
			],
			pause:[
				'home','mute','settings'
			],
			tutorial:[
				'back','mute','settings'
			],
			game:[
				'pause',
			],
			gameover:[
				'home', 'pause',
			]
		};

		this.buildButtons();
		this.hideButtons();
	}
	buildButtons() {
		this.buttons = {};


		const pauseButton = IconButton.primary('icon_pause.png', 'pause');
		pauseButton.view.accessible = true;
		pauseButton.view.tabIndex = 6;

		this.addChild(pauseButton.view);
		this.buttons.pause = pauseButton;

		const settingsButton = IconButton.tertiary('icon_settings.png', 'settings');
		settingsButton.view.tabIndex = 5;
		settingsButton.view.accessible = true;

		this.addChild(settingsButton.view);

		this.buttons.settings = settingsButton;


		const muteButton = IconButton.tertiary('icon_sound_on.png', 'sound');
		muteButton.view.tabIndex = 4;
		muteButton.view.accessible = true;
		this.addChild(muteButton.view);
		this.buttons.mute = muteButton;

		const backButton = IconButton.primary('icon_back.png', 'back');
		backButton.view.tabIndex = 3;
		backButton.view.accessible = true;

		this.addChild(backButton.view);
		this.buttons.back = backButton;

		const exitButton = IconButton.primary('icon_exit.png', 'exit');
		exitButton.view.tabIndex = 1;
		exitButton.view.accessible = true;
		this.addChild(exitButton.view);
		this.buttons.exit = exitButton;

		const homeButton = IconButton.primary('icon_home.png', 'home');
		homeButton.view.tabIndex = 2;
		homeButton.view.accessible = true;
		this.addChild(homeButton.view);
		this.buttons.home = homeButton;

		for(const key in this.buttons) {
			this.buttons[key].onPress.add(this.onButtonPressed, this);
		}


		//	mute checking
		gmi = getGMI();
		const settings = gmi.getAllSettings();

		if(settings.muted) {
			SoundManager.mute();
			this.buttons.mute.setIcon('icon_sound_off.png');
		}

		SoundManager.onMuteToggle.add(this._onMuteToggle, this);

	}
	hideButtons() {
		for(const key in this.buttons) {
			this.buttons[key].view.visible = false;
		}
	}
	setState(state) {
		this.hideButtons();
		for(const key in this.states) {
			if(state === key) {
				const buttonsNames = this.states[key];
				for (let i = 0; i < buttonsNames.length; i++) {
					const buttonName = buttonsNames[i];
					this.buttons[buttonName].view.visible = true;
				}
			}
		}
		this.previousState = this.state;
		this.state = state;

	}
	show() {
		this.visible = true;
	}
	hide(mValue) {
		this.visible = !mValue;
	}

	_onMuteToggle() {
		if(SoundManager.isMuted) {
			this.buttons.mute.setIcon('icon_sound_off.png');
		} else {
			this.buttons.mute.setIcon('icon_sound_on.png');
		}

		gmi.setMuted(SoundManager.isMuted);
	}

	onButtonPressed(bt) {
		switch (bt) {
		case this.buttons.mute:
			if(SoundManager.isMuted) {
				SoundManager.unmute();
			} else {
				SoundManager.mute();
			}
			break;
		case this.buttons.pause:
			console.log('pause button');
			this.app.overlayManager.show('pause', {log:Math.random()});
			this.setState('pause');
			break;
		case this.buttons.back:
			console.log('back button');
			break;
		case this.buttons.settings:
			this.app.overlayManager.show('settings');
			console.log('settings button');
			break;
		case this.buttons.home:
			console.log('home button');
			this.app.overlayManager.hide();
			this.app.screenManager.gotoScreenByID('title');
			break;
		case this.buttons.exit:
			console.log('exit button');
			window.location.href = 'http://www.goodboydigital.com/';
			break;
		default:

		}
	}
	resize(width, height){
		const buttonRadiusSmall = this.buttons.mute.view.width / 2;

		const top = buttonRadiusSmall + GEL_PADDING_TOP;
		this.buttons.pause.view.position.set(width - buttonRadiusSmall - GEL_PADDING_LEFT, top);
		this.buttons.mute.view.position.set(width - buttonRadiusSmall * 3 - GEL_PADDING_LEFT - GEL_PADDING_BETWEEN_BUTTON, top);

		const buttonHomeRadius = this.buttons.home.view.width / 2;

		this.buttons.back.view.position.set( buttonHomeRadius + GEL_PADDING_LEFT, buttonHomeRadius + GEL_PADDING_TOP );
		this.buttons.home.view.position.set( buttonHomeRadius + GEL_PADDING_LEFT, buttonHomeRadius + GEL_PADDING_TOP );
		this.buttons.exit.view.position.x = this.buttons.home.view.position.x;
		this.buttons.exit.view.position.y = this.buttons.home.view.position.y;
		this.buttons.settings.view.position.x = this.buttons.pause.view.position.x;
		this.buttons.settings.view.position.y = this.buttons.pause.view.position.y;
	}
}

// export default class TopMenu extends PIXI.Container {
// 	constructor(app) {
// 		super();
// 		this.app = app;
// 		this.muteButton = IconButton.small('icon_sound.png');
// 		this.addChild(this.muteButton.view);
// 		this.muteButton.onPress.add(this.onButtonPressed, this);
//
// 		this.pauseButton = IconButton.small('icon_pause.png');
// 		this.addChild(this.pauseButton.view);
// 		this.pauseButton.onPress.add(this.onButtonPressed, this);
//
// 		this.pauseButton.view.visible = false;
//
// 		SoundManager.sfx.onMuteToggle.add(this.onMuteToggle, this);
// 		SoundManager.music.onMuteToggle.add(this.onMuteToggle, this);
//
// 		this.backButton = IconButton.small('icon_back.png');
// 		this.backButton.view.x = 30 + 52;
// 		this.backButton.view.y = 30 + 52;
// 		this.backButton.onPress.add(this.onButtonPressed, this);
//
// 		this.addChild(this.backButton.view);
//
// 		this.backButton.view.visible = false;
//
// 	}
// 	gameMode()
// 	{
// 		this.pauseButton.view.visible = true;
// 		this.backButton.view.visible = false;
// 	}
//
// 	normalMode()
// 	{
// 		this.pauseButton.view.visible = false;
//
// 		if(Device.instance.mobile)
// 			{
// 			this.showBackButton();
// 		}
// 		else{
// 			this.backButton.view.visible = false;
// 		}
// 	}
//
// 	showBackButton() {
// 		this.backButton.view.visible = true;
// 	}
//
// 	onButtonPressed(bt)
// 	{
// 		if(bt === this.muteButton)
// 			{
//
// 			if(SoundManager.sfx.isMuted && SoundManager.music.isMuted)
// 					{
// 				SoundManager.music.unmute();
// 				SoundManager.sfx.unmute();
// 			}
// 			else
// 					{
// 				SoundManager.music.mute();
// 				SoundManager.sfx.mute();
// 			}
// 		}
// 		else if(bt === this.pauseButton)
// 			{
// 			this.app.overlayManager.show('pause');
// 		}
// 		else if(bt === this.backButton)
// 			{
// 			this.app.screenManager.gotoScreenByID('title');
// 		}
//
// 	}
//
// 	onMuteToggle()
// 	{
// 		if(SoundManager.sfx.isMuted && SoundManager.music.isMuted)
// 			{
// 			this.muteButton.setIcon('icon_mute.png');
// 		}
// 		else
// 			{
// 			this.muteButton.setIcon('icon_sound.png');
// 		}
// 	}
//
// 	onShown()
// 	{
//
// 	}
//
// 	resize(w, h)
// 	{
// 		if(!this.isMobile)
// 			{
// 			const width = 45;
// 			const paddingLeft = 42;
//
// 			this.muteButton.view.position.set( w - width - paddingLeft, 30 + 52);
// 			this.pauseButton.view.position.set( w - width - paddingLeft  - 140, 30 + 52);
// 		}
// 		else
// 			{
// 			const width = 100;
// 			const paddingLeft = 42;
//
// 			this.muteButton.view.position.set( w - width - paddingLeft, 30 + 52);
// 			this.pauseButton.view.position.set( w - width - paddingLeft - 140, 30 + 52);
// 		}
// 	}
// }
