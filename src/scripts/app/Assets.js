// Assets.js

import Device from  'fido/system/Device';


let instance;

class Assets {
	constructor() {
		this._version = !Device.instance.desktop ? 'high' : 'low';
		console.debug('Asset version : ', this._version);
	}


	getManifest(mManifest) {
		return mManifest.map((a)=> {
			return a.replace('high', this._version);
		});
	}


	fromImage(mPath) {

	}
}


if(!instance) {
	instance = new Assets();
}

export default instance;