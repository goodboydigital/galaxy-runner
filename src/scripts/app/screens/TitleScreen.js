import * as PIXI from 'pixi.js';
import Button          from 'fido/ui/buttons/AbstractButton';
import IconButton      from 'buttons/IconButton';
import Device          from 'fido/system/Device';
import { GEL_PADDING_LEFT, GEL_PADDING_RIGHT, GEL_PADDING_BOTTOM } from 'Const';
// import LabelButton     from 'fido/ui/buttons/LabelButton';

export default class TitleScreen extends PIXI.Container {
	constructor(app) {
		super();
		window.title = this;
		this.app = app;

		// title label
		var format =
			{
				font: '52px helvetica',
				fill: '#ffffff',
			};

		this.title = new PIXI.Text('Amazing game', format);
		this.title.anchor.x = .5;
		this.title.anchor.y = .5;
		this.addChild(this.title);

		this.playButton = IconButton.primaryBig('icon_play.png');
		this.playButton.view.accessible = true;
		this.playButton.view.tabIndex = 10;

		this.addChild(this.playButton.view);

		this.helpButton = IconButton.secondaryRect('How to play', 'icon_howtoplay.png', 'how to play');
		this.addChild(this.helpButton.view);
		this.helpButton.view.accessible = true;
		this.helpButton.view.tabIndex = 12;
		// this.helpButton.view.tabIndex = 21;

		this.helpButton.onPress.add(this.onButtonPressed, this);


		this.achivementsButton = IconButton.secondaryRect('Achivements', 'icon_howtoplay.png', 'how to play');
		this.addChild(this.achivementsButton.view);
		this.achivementsButton.view.accessible = true;
		this.achivementsButton.view.tabIndex = 11;
		// this.achivementsButton.view.tabIndex = 26;

		this.achivementsButton.onPress.add(this.onButtonPressed, this);


		this.playButton.onPress.add(this.onButtonPressed, this);

	}

	onButtonPressed(bt)
	{
		if(bt === this.playButton)
			{
			if(Device.instance.android)
					{
				if (document.body.mozRequestFullScreen) {
								// This is how to go into fullscren mode in Firefox
								// Note the "moz" prefix, which is short for Mozilla.
					document.body.mozRequestFullScreen();
				} else if (document.body.webkitRequestFullScreen) {
								// This is how to go into fullscreen mode in Chrome and Safari
								// Both of those browsers are based on the Webkit project, hence the same prefix.
					document.body.webkitRequestFullScreen();
				}
			}

			this.screenManager.gotoScreenByID('game');
		} else if(bt === this.helpButton) {
			this.app.overlayManager.show('tutorial');
		}
		else if(bt === this.achivementsButton) {
			// this.app.overlayManager.show('achivements');
		}

	}
	init() {
		console.log('init screen titleScreen');
	}
	beforeShow(params) {
		console.log('beforeShow titleScreen');
	}
	onShow()
	{
		console.log('on show titleScreen');

		this.title.scale.set(0);
		this.playButton.view.scale.set(0);

		this.app.topMenu.setState('home');

		TweenLite.to(this.title.scale, 1, {x:1, y:1, ease:Elastic.easeOut, delay:0.2});
		TweenLite.to(this.playButton.view.scale, 1, {x:this.playButton.overScale, y:this.playButton.overScale, ease:Elastic.easeOut, delay:0.3});

	}

	onShown()
	{

	}

	resize(w, h)
	{

		this.title.position.set(w/2 , h/2 - 120);
		this.playButton.view.position.set(w/2, h/2 + 100);
		this.helpButton.view.position.x = w - this.achivementsButton.view.width / 2 - GEL_PADDING_RIGHT;
		this.helpButton.view.position.y = h - this.helpButton.view.height / 2 - GEL_PADDING_BOTTOM;

		this.achivementsButton.view.position.x = this.achivementsButton.view.width / 2 + GEL_PADDING_LEFT;
		this.achivementsButton.view.position.y = h - this.achivementsButton.view.height / 2 - GEL_PADDING_BOTTOM;

	}
}
