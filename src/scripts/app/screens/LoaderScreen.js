// import PIXI from 'pixi.js'

import * as PIXI from 'pixi.js';
import Ticker          from  'fido/system/Ticker';
import Device          from  'fido/system/Device';
import Wait            from  'fido/utils/FrameWait';
import Utils           from  'fido/utils/Utils';
import Signal          from  'signals';

export default class LoaderScreen extends PIXI.Container {
	constructor(app) {
		super();
		this.app = app;

		this.count = 0;

		app.loader.onProgress.add(this.onProgress, this);

		this.easeLoad = 0;
		this.targetLoad = 0;

		this.onReady = new Signal();
		this.onComplete = new Signal();

		this.onAssetsLoaded();
	}
	onShown()
	{
			//this.prepreloader.load();
	}

	onAssetsLoaded()
	{
		this.container = new PIXI.Container();

		this.barBg = new PIXI.Sprite.fromImage(window.ASSET_URL + 'image/preload/loader-bar-empty.png');
		this.barBg.position.set(this.app.safeSize.width /2,this.app.safeSize.height /2);
		this.barBg.anchor.x = 0.5;
		this.barBg.anchor.y = 0.5;
		this.bar = new PIXI.Sprite.fromImage(window.ASSET_URL + 'image/preload/loader-bar-full.png');
		this.bar.position.set(0,this.app.safeSize.height /2);
		this.bar.anchor.y = 0.5;

		this.container.addChild(this.barBg);
		this.container.addChild(this.bar);

		Ticker.instance.add(this.update, this);

		this.addChild(this.container);

		this.resize(this.w, this.h);

		this.scaleNumber = 1.3;

		Wait.wait(this.showLoader.bind(this));
	}

	showLoader()
	{
			// TweenLite.to(this.app.bg, 0.6, {
			//     alpha : 1,
			//     ease : Sine.easeOut
			// });

		this.app.loader.load();
	}

	update()
	{
		// this.count += 0.02;

		this.easeLoad += (this.targetLoad - this.easeLoad) * 0.1;
		this.bar.scale.x = this.easeLoad;
		this.bar.position.x = this.w/2 - 1201/2 + 30 * (1 - this.easeLoad);

		if(this.easeLoad > 0.999)
			{
				// this.bar.scale.x = 1;
			Ticker.instance.remove(this.update,this);
			this.onComplete.dispatch();

		}
	}

	onProgress(percent)
	{
		this.targetLoad = percent;
	}

	onShow()
	{

	}

	onHide()
	{
		Ticker.instance.remove(this.update, this);
	}

	resize(w, h)
	{
		this.w = w;
		this.h = h;

		this.barBg.position.x = w/2;
		// this.barBg.width = w;
		// this.bar.width = w;
		this.bar.position.x = w/2 - 1201/2 + 20;
	}
}
