import * as PIXI from 'pixi.js';
import Ticker from 'fido/system/Ticker'

import RushGame from '../../game/RushGame';
import DigitalTransition from '../../DigitalTransition';

//import OrbitControl from './src/utils/OrbitControls';

export default class GameScreen extends PIXI.Container {
	constructor(app) {
		super();
		this.app = app;

		this.game = new RushGame(this);

		this.transition = new DigitalTransition();

	}

	onShow()
	{
		//this.game.start();
		this.game.overlay.show();

		this.app.overlayManager.onShow.add(this.onOverlayShow, this);
		this.app.overlayManager.onHide.add(this.onOverlayHide, this);
		this.app.topMenu.setState('game');


		Ticker.instance.add(()=>{

			this.game.update(1);
		})
	}

	quit()
	{
		this.screenManager.gotoScreenByID('title');
	}

	onGameover()
	{
		this.app.overlayManager.show('gameover');
	}

	onShown()
	{

	}

	onHide()
	{
		this.app.overlayManager.onShow.remove(this.onOverlayShow, this);
		this.app.overlayManager.onHide.remove(this.onOverlayHide, this);
		this.app.view.style['-webkit-filter'] = null;
	}

	onHidden()
	{
		this.game.pause();
		this.app.topMenu.setState('home');
	}

	onOverlayShow()
	{
		this.game.pause();
	}

	onOverlayHide()
	{
		if(this.game.isGameover)
		{
			//this.game.reset();
			//this.game.resume();
		}
		else
		{
			//this.game.resume();
			//this.app.topMenu.setState('game');

		}
	}

	resize(w, h)
	{
		this.game.resize(w, h)
	}
}
