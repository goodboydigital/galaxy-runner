define(function (require, exports, module)
{
	var PIXI            = require('PIXI');
	var Leaderboard     = require('./Leaderboard');
	var PixiTrackpad    = require('fido/ui/PixiTrackpad');
	var Utils           = require('fido/utils/Utils');

	var LeaderboardScreen = function(app)
		{
		this.app = app;

		PIXI.Container.call( this );

		this.bg = new PIXI.Graphics();
		this.bg.beginFill(0x7a7a7a);
		this.bg.drawRect(0, 0, 300, 200);
		this.addChild(this.bg);

		this.hasClose = true;


		this.scoreText = new PIXI.Text('Best Scores',{font : '60px Times New Roman'});
		this.scoreText.anchor.x = 0.5;
		this.addChild(this.scoreText);

				// Don't forget to remove this return statement to change this screen !
		return;

		var bgPanel = new PIXI.Sprite.fromImage(ASSET_URL + 'img/game/main_info_panel.png');
		bgPanel.anchor.set(0.5, 0.5);
		this.addChild(bgPanel);

		var bgPanel = new PIXI.Sprite.fromFrame('leaderboard_BG_area.png');
		bgPanel.anchor.set(0.5, 0.5);
		this.addChild(bgPanel);

		var leaderBoardParameters = {
			frameWidth : bgPanel.width,
			frameHeight : bgPanel.height,
			lineHeight : bgPanel.height / 6,
			padding : {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			}
		};

		this.leaderboard = new Leaderboard(leaderBoardParameters);
		this.leaderboard.alpha = 0;
		this.addChild(this.leaderboard);

		this.hitArea = new PIXI.Rectangle(-(bgPanel.width * 0.5), -(bgPanel.height * 0.5), bgPanel.width-40, bgPanel.height);

		this.leaderboard.position.x = -(bgPanel.width * 0.5) + 30;
		this.leaderboard.position.y = -(bgPanel.height * 0.5) + 4;

		this.myScoreView = new MyScoreView();
		this.addChild(this.myScoreView);
		this.myScoreView.position.y = 120;
		this.myScoreView.position.x = -(bgPanel.width * 0.5) + 30;

		var trackPadParameters = {
			target : this,
			scrollbar : true,
			scrollTarget : this.leaderboard
		};

		this.trackpad = new PixiTrackpad(trackPadParameters);
		this.trackpad.scrollMin = 0;
		this.trackpad.scrollMax = 610;
		this.trackpad.capMovement = true;

		this.showingTotalScore;
	};

	LeaderboardScreen.prototype = Object.create( PIXI.Container.prototype );

	LeaderboardScreen.prototype.onTotalScorePressed = function()
		{
		if(this.buttonTotalScore.state === 0)
				{
			this.buttonTotalScore.toggleOn();
			this.buttonTotalScore.state = 1;
			this.loadTotalScores();
			this.buttonSinglePlay.toggleOff();
			this.buttonSinglePlay.state = 0;
			this.trackpad.easeToPosition(0, 0);
		}
	};

	LeaderboardScreen.prototype.onSinglePlayPressed = function()
		{
		if(this.buttonSinglePlay.state === 0)
				{
			this.buttonSinglePlay.toggleOn();
			this.buttonSinglePlay.state = 1;
			this.loadSinglePlayScores();
			this.buttonTotalScore.toggleOff();
			this.buttonTotalScore.state = 0;
			this.trackpad.easeToPosition(0, 0);
		}
	};

	LeaderboardScreen.prototype.loadTotalScores = function()
		{
		this.loadScores('allatbreak_total');
	};

	LeaderboardScreen.prototype.loadSinglePlayScores = function()
		{
		this.loadScores('allatbreak_overall');
	};

	LeaderboardScreen.prototype.updateTransform = function()
		{
				// this.trackpad.update();
				// this.leaderboard.scrollPosition = this.trackpad.valueY;

		PIXI.Container.prototype.updateTransform.call(this);
	};

	LeaderboardScreen.prototype.loadScores = function(id)
		{
		if(window.Highscores)
				{
			var scope = this;

			window.Highscores.getLeaderboard(
								function(rawData)
								{
									var scoreData = rawData[id];

									var data = [];

										// need to format this shnizzle..
									for (var i = 0; i < scoreData.length; i++) {
										var score = {
											username : scoreData[i].displayName,
											score: scoreData[i].highscore
										};
										data.push(score);
									}

									console.log('SCORES SUCCESFULLY LOADED', data);

									scope.onDataRecieved(data);
								},
								function(data)
								{
									console.log('SCORES FAILED TO LOAD', data);
								},
								[id], 1, 'rank'
						);

			window.Highscores.getUserScore(function(rawData)
						{
				if(rawData[id])
								{
					var scoreData = rawData[id][0];
					var data = {
						username : scoreData.displayName,
						score : scoreData.highscore,
						rank : scoreData.rank
					};
				}
				else
								{
					var data = {
						username : '',
						score : '',
						rank : ''
					};
				}

				scope.onUserDataRecieved(data);
			},
						function(data)
						{
							console.log('USER SCORES FAILED TO LOAD', data);
						},
						[id]);
		}
		else
				{
			this.onDataRecieved();
			this.onUserDataRecieved();
		}
	};

	LeaderboardScreen.prototype.onHide = function()
		{
				// this.leaderboard.alpha = 0;
	};

	LeaderboardScreen.prototype.onShow = function()
		{

				// this.loadScores("allatbreak_overall");

				// this.myScoreView.onShow();
	};

	LeaderboardScreen.prototype.onDataRecieved = function(data)
		{
		if(!data)
				{
			var data = [];

			for (var i = 0; i < 70; i++)
						{
				var score = {
					username : 'Mat' + ( (Math.random() * 1000) | 0 ),
					score : i * 100
				};
				data.push(score);
			}

		}

		this.leaderboard.setData(data);
		this.trackpad.scrollMinY = -(this.leaderboard.scrollHeight - this.leaderboard.viewHeight);
		this.leaderboard.alpha = 1;
	};

	LeaderboardScreen.prototype.onUserDataRecieved = function(data)
		{
		if(!data)
				{
			data = {
				username : '',
				score : '',
				rank : ''
			};
		}

		this.myScoreView.setScore(data);
	};

	LeaderboardScreen.prototype.resize = function(w, h)
		{
		this.w = w;
		this.h = h;

		this.x = w/2;
		this.y = h/2;
	};

	MyScoreView = function(rank)
		{
		PIXI.Container.call(this);

		this.labelNumber = new PIXI.Text('', {
			font: '26px chunkfiveroman',
			fill: '#00365c',
			align: 'left',
			dropShadow : true,
			dropShadowColor : '#a9e8eb',
			dropShadowDistance : 2
		});

		this.labelName = new PIXI.Text('', {
			font: '26px chunkfiveroman',
			fill: '#00365c',
			align: 'left',
			dropShadow : true,
			dropShadowColor : '#a9e8eb',
			dropShadowDistance : 2
		});

		this.labelScore = new PIXI.Text('', {
			font: '26px chunkfiveroman',
			fill: '#00365c',
			align: 'left',
			dropShadow : true,
			dropShadowColor : '#a9e8eb',
			dropShadowDistance : 2
		});

		this.labelName.position.x = 70;

		this.labelScore.anchor.x = 1;
		this.labelScore.position.x = 580;

		this.addChild(this.labelNumber);
		this.addChild(this.labelName);
		this.addChild(this.labelScore);
	};

	MyScoreView.constructor = ScoreView;

	MyScoreView.prototype = Object.create( PIXI.Container.prototype );

	MyScoreView.prototype.setScore = function(data)
		{
		if(data.username !== '') // Only display if its legit.
				{
			this.labelName.text = data.username;
			this.labelScore.text = Utils.formatScore(data.score).toUpperCase();
			this.labelNumber.text = data.rank + 1;
		}
	};

	MyScoreView.prototype.onShow = function()
		{
		this.labelNumber.alpha = 0;
		this.labelName.alpha = 0;
		this.labelScore.alpha = 0;

		TweenLite.to(this.labelNumber, 1, {
			alpha: 1,
			delay: 0.25
		});

		TweenLite.to(this.labelName, 1, {
			alpha: 1,
			delay: 0.5
		});

		TweenLite.to(this.labelScore, 1, {
			alpha: 1,
			delay: 0.75
		});
	};

	module.exports = LeaderboardScreen;

});
