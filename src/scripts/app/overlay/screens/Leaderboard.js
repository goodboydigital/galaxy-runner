import * as PIXI from 'pixi.js';
import Utils 	from 'fido/utils/Utils';

export default class Leaderboard extends PIXI.Container {
	constructor(parameters) {
		super();

		this.list = [];
		this.realHeight = 0;
		this.viewHeight = parameters.frameHeight || 40 * 6;
		this.viewWidth = parameters.frameWidth || 800;
		this.scoreViewHeight = parameters.lineHeight || 40;
		this.padding = typeof parameters.padding === 'object' ? parameters.padding : {'top' : 0, 'right' : 0, 'bottom' : 0, 'left' : 0};

		this.maskGraphic = new PIXI.Graphics().beginFill(0xFF0000).drawRect(0, 0, this.viewWidth, this.viewHeight-6);
		this.addChild(this.maskGraphic);
		this.mask = this.maskGraphic;

		this.realHeight = this.viewHeight + (this.scoreViewHeight * 2);

		var length = Math.floor( this.realHeight / this.scoreViewHeight);

		for (var i=0; i < length; i++)
		{
			var scoreView = new ScoreView(i);
			this.addChild(scoreView);
			scoreView.position.y = i * this.scoreViewHeight;
			this.list.push(scoreView);
		}
	}
	setData(data)
	{
		this.data = data;

		for (var i = 0; i < this.list.length; i++)
		{
			var scoreItem = this.list[i];
			scoreItem.id = 999999999999;
		}

		this.alpha = 0;

		TweenLite.to(this, 0.3, {alpha:1});

		this.scrollHeight = data.length * this.scoreViewHeight + this.padding.top + this.padding.bottom;

		this.updateScrollPosition();
	}

	updateScrollPosition()
	{
		if(!this.data) return;


		for (var i = 0; i < this.list.length; i++)
		{
			var scoreItem = this.list[i];

			scoreItem.position.y = (i * this.scoreViewHeight) + this._scrollPosition + this.padding.top;
			scoreItem.position.y += this.scoreViewHeight;

			var id = Math.floor(scoreItem.position.y / this.realHeight);
			id *= this.list.length;
			id -= i;
			id *= -1;

			if(scoreItem.id != id)
			{
				scoreItem.id = id;
				scoreItem.visible = (scoreItem.id >=0 && scoreItem.id < this.data.length);
				if(scoreItem.visible)
				{
					scoreItem.labelNumber.text = scoreItem.id+1;
					scoreItem.setScore(this.data[id]);
				}
			}

			scoreItem.position.y %= this.realHeight;
			if(scoreItem.position.y < 0) scoreItem.position.y += this.realHeight;

			scoreItem.position.y -= this.scoreViewHeight;

		}
	}
}



Object.defineProperty(Leaderboard.prototype, 'scrollPosition', {
	get: function() {
		return this._scrollPosition;
	},
	set: function(value) {
		this._scrollPosition = value;
		this.updateScrollPosition();
	}
});


class ScoreView extends PIXI.Container {
	constructor(id) {
		super();
		var label = (id + 1) + '.';

		this.labelNumber = new PIXI.Text(label, {
			font: '23px chunkfiveroman',
			fill: '#FFFFFF',
			align: 'left',
			dropShadow : true,
			dropShadowColor : '#666666'
		});

		this.labelName = new PIXI.Text('0', {
			font: '22px chunkfiveroman',
			fill: '#FFFFFF',
			align: 'left',
			dropShadow : true,
			dropShadowColor : '#666666'
		});

		this.labelScore = new PIXI.Text('0', {
			font: '22px chunkfiveroman',
			fill: '#FFFFFF',
			align: 'left',
			dropShadow : true,
			dropShadowColor : '#666666'
		});

		this.labelName.position.x = 70 ;

		this.labelScore.anchor.x = 1;
		this.labelScore.position.x = 580;

		this.addChild(this.labelNumber);
		this.addChild(this.labelName);
		this.addChild(this.labelScore);
	}

	setScore(data)
	{
		this.labelName.text = data.username;
		this.labelScore.text = Utils.formatScore(data.score).toUpperCase();
	}
}
