import * as PIXI from 'pixi.js';
import SoundManager    from 'fido/sound/SoundManager';
import IconButton      from 'buttons/IconButton';
import Cache           from 'fido/loader/Cache';
import LabelButton     from 'fido/ui/buttons/LabelButton';
import { GEL_PADDING_LEFT, GEL_PADDING_RIGHT, GEL_PADDING_BOTTOM, GEL_TITLE_PADDING_TOP } from 'Const';
export default class PauseScreen extends PIXI.Container {
	constructor(app) {
		super();
		window.pause = this;
		this.app = app;

		this.hasClose = false;
		this.bg = new PIXI.Graphics();
		this.bg.beginFill(0x7a7a7a);
		this.bg.drawRect(0, 0, 300, 200);
		// this.bg.anchor.set(0.5);
		this.addChild(this.bg);

		this.title = new PIXI.Text('Paused',{font : '60px helvetica', fill: 'white'});
		this.title.anchor.x = 0.5;


		this.addChild(this.title);

		this.levelButton = IconButton.primaryMedium('icon_levels.png');
		this.levelButton.accessible = true;
		this.levelButton.view.tabIndex = 3;

		this.levelButton.onPress.add(this.onPress, this);
		// this.levelButton.view.x = this.bg.width/4;
		this.levelButton.view.x = 10;
		this.addChild(this.levelButton.view);

		this.playButton = IconButton.primaryBig('icon_play.png');
		this.playButton.accessible = true;
		this.playButton.view.tabIndex = 2;

		this.playButton.view.anchor.set(0.5);
		this.playButton.onPress.add(this.onPress, this);

		this.addChild(this.playButton.view);

		this.restartButton = IconButton.primaryMedium('icon_restart.png');
		this.restartButton.accessible = true;
		this.restartButton.view.tabIndex = 1;

		this.restartButton.onPress.add(this.onPress, this);
		// this.restartButton.view.x = -this.bg.width/4;
		this.restartButton.view.x = 10;
		this.addChild(this.restartButton.view);



		// this.quitButton = IconButton.small('icon_home.png');
		// this.quitButton.onPress.add(this.onPress, this);
		// this.quitButton.view.x = -90;
		// this.quitButton.view.y = 28;
		// this.addChild(this.quitButton.view);

		// this.muteButton = IconButton.small('icon_sound.png');
		// this.addChild(this.muteButton.view);
		// this.muteButton.view.x = 90;
		// this.muteButton.view.y = 158;
		// this.muteButton.onPress.add(this.onPress, this);

		this.helpButton = IconButton.secondaryRectInversed('How to play', 'icon_howtoplay.png', 'show to play');
		this.helpButton.accessible = true;
		this.helpButton.view.tabIndex = 34;
		this.addChild(this.helpButton.view);
		this.helpButton.onPress.add(this.onPress, this);

		this.settingsButton = IconButton.secondaryRectInversed('Achivements', 'icon_howtoplay.png', 'Achivement');
		this.settingsButton.accessible = true;
		this.settingsButton.view.tabIndex = 35;
		this.settingsButton.onPress.add(this.onPress, this);
		this.addChild(this.settingsButton.view);


	}
	onPress(bt)
	{
		if(bt === this.restartButton)
			{
					// restart!
			this.app.gameScreen.game.reset();
			this.app.overlayManager.hide();
		}
		else  if(bt === this.playButton)
			{
			this.app.overlayManager.hide();
			this.app.gameScreen.game.resume();
		}

		else  if(bt === this.helpButton)
			{
			this.app.overlayManager.gotoScreenByID('tutorial');
		}

		else  if(bt === this.settingsButton)
			{
			this.app.overlayManager.gotoScreenByID('scores');
		}
	}
	init() {
		console.log('init pause');
	}
	beforeShow(params) {
		console.log('beforeShow pause',params);
	}
	onShow() {
		console.log('on show pause');
	}
	resize(w, h)
	{
		this.w = w;
		this.h = h;
		this.x = w/2;
		this.y = h/2;

		this.title.position.y = -this.h/2 + GEL_TITLE_PADDING_TOP;

		this.bg.width = this.w/1.4;
		this.bg.height = this.h/1.7;
		this.bg.position.x = -this.bg.width/2;
		this.bg.position.y = -this.bg.height/2;


		this.restartButton.view.position.x = -this.playButton.view.width;

		this.levelButton.view.position.x = this.playButton.view.width;


		this.settingsButton.view.position.x = -this.w/2 + this.settingsButton.view.width/2  + GEL_PADDING_LEFT;
		this.settingsButton.view.position.y = this.h/2 - this.settingsButton.view.height/2  - GEL_PADDING_BOTTOM;

		this.helpButton.view.position.x = this.w/2 - this.helpButton.view.width/2  - GEL_PADDING_RIGHT;
		this.helpButton.view.position.y = this.settingsButton.view.position.y;
	}

}
