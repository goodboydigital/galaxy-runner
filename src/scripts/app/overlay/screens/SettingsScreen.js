import * as PIXI from 'pixi.js';

import SoundManager     from 'fido/sound/SoundManager';
import ToggleButton     from 'buttons/ToggleButton';
import ClearButton      from 'buttons/ClearButton';
import AbstractButton   from 'fido/ui/buttons/AbstractButton';
// import SettingsManager  from 'com/witch/app/system/SettingsManager';

class SettingsScreen extends PIXI.Container // app is HouseScreen for this overlay
{
	constructor(app) {
		super(app);
		window.ss = this;
		this.app = app;
		this.hasClose = false;
		this.hasCleared = false;
		this.noRenderBlur = true;

		this._bg1 = new PIXI.Sprite.fromFrame('settings_page1.png');//
		this._bg1.anchor.set(0.5);
		this._bg1.position.y = 19;
		this.addChild(this._bg1);

		// this._bg2 = new PIXI.Sprite.fromFrame('settings_page2.png');//
		// this._bg2.anchor.set(0.5);
		// this._bg2.position.y = 19;
		// this.addChild(this._bg2);
		// TODO update
		this._btnAudio = new ToggleButton( false, 'audio' );
		this._btnMotion = new ToggleButton(false, 'motion');
		// this._btnAudio = new ToggleButton( !SettingsManager.muted.enabled, 'audio' );
		// this._btnMotion = new ToggleButton(SettingsManager.movement.enabled, 'motion');
		this._btnClear = new ClearButton('clear');

		this._btnAudio.view.position.set(180, -120);
		this._btnAudio.view.accessible = true;
		// this._btnAudio.view.tabIndex = 80;
		this._btnMotion.view.position.set(180, 5);
		// this._btnMotion.view.tabIndex = 81;
		this._btnClear.view.position.set(180, 130);
		// this._btnClear.view.tabIndex = 82	;

		this._btnAudio.onPress.add(this.onPress, this);
		this._btnMotion.onPress.add(this.onPress, this);
		this._btnClear.onPress.add(this.onPress, this);

		this.addChild(this._btnAudio.view);
		this.addChild(this._btnMotion.view);
		this.addChild(this._btnClear.view);

		// this.closeButton = IconButton.close();
		// this.closeButton.onPress.add(this.goBack, this);
		// this.addChild(this.closeButton.view);

		window.audioButton = this._btnAudio;
		window.motionButton = this._btnMotion;

		// SettingsManager.muted.onToggle.add(this.onAudioToggle, this);
		// SettingsManager.movement.onToggle.add(this.onMotionToggle, this);


		function getInvButton(width=80, height=80) {
			const g = new PIXI.Graphics();
			g.beginFill(0x00FFFF, .0);
			g.drawRect(0, 0, width, height);
			g.endFill();
			const btn = new AbstractButton(g);
			return btn;
		}

		this._btnClose = getInvButton();
		this.addChild(this._btnClose.view);
		this._btnClose.view.position.set(this._bg1.width/2-this._btnClose.view.width-18, -this._bg1.height/2+this._btnClose.view.height/2-3);
		// this._btnClose.view.tabIndex = 40;
		this._btnClose.view.accessible = true;
		this._btnClose.view.accessibleTitle = 'close';

		this._btnClose.onPress.add(this.goBack, this);

		// this._btnPrev = getInvButton();
		// this.addChild(this._btnPrev.view);
		// this._btnPrev.view.x = -this._bg1.width/2 + 15;
		// this._btnPrev.view.y = -this._btnPrev.view.height/2 + 22;
		// this._btnPrev.onPress.add(this.prev, this);

		// this._btnNext = getInvButton();
		// this.addChild(this._btnNext.view);
		// this._btnNext.view.x = this._bg1.width/2 -this._btnNext.view.width - 18;
		// this._btnNext.view.y = -this._btnNext.view.height/2 + 22;
		// this._btnNext.onPress.add(this.next, this);

		this._showPage(0);
	}


	next() {	this._showPage(1);	}
	prev() {	this._showPage(0);	}

	_showPage(index) {
		if(this._pageIndex === index) return;
		this._pageIndex = index;
		if(index == 0) {
			this._bg1.visible = true;
			// this._bg2.visible = false;
			this._btnAudio.view.visible = true;
			this._btnMotion.view.visible = true;
			this._btnClear.view.visible = true;
			// this._btnNext.view.visible = true;
			// this._btnPrev.view.visible = false;
		} else {
			this._bg1.visible = false;
			// this._bg2.visible = true;
			this._btnAudio.view.visible = false;
			this._btnMotion.view.visible = false;
			this._btnClear.view.visible = false;
			// this._btnNext.view.visible = false;
			// this._btnPrev.view.visible = true;
		}
	}

	// SettingsScreen.prototype = Object.create( DumpOverlay.prototype );

	onAudioToggle(value) {
		this._btnAudio.toggle(!value);
	}

	onMotionToggle(value) {
		this._btnMotion.toggle(value);
	}

	onShow() {
		this._showPage(0);
		this.app.topMenu.hide(true);
		this._btnClear.reset();
		this._btnClear.enable();
	}

	onHide() {
		this.app.topMenu.hide(false);
	}


	onPress(bt)
	{
		SoundManager.sfx.play('settings_slider');

		if(bt === this._btnAudio ) {
			// if( !SettingsManager.muted.enabled ) {
			// 	SettingsManager.muted.enable();
			// } else {
			// 	SettingsManager.muted.disable();
			// }

		} else if(bt === this._btnMotion) {
			// if( !SettingsManager.movement.enabled ) {
			// 	SettingsManager.movement.enable();
			// } else {
			// 	SettingsManager.movement.disable();
			// }
		}
		else if(bt === this._btnClear) {
			this._btnClear.tick();
			this._btnClear.disable();
			window.location.reload();

			// alfrid.Scheduler.delay(()=> {
			// 	this.app.gameScreen.game.resetSettings();
			// }, null, 1000);

			this.app.gameScreen.game.player.resetAll();
			// this.app.gameScreen.game.resetSettings();


			this.screenManager.hide();
			this.app.screenManager.gotoScreenByID('title');
		}


	}

	goBack() {
		if(this.app.screenManager.currentScreen.id === 'title'){
			this.app.overlayManager.goBack();
		} else {
			this.screenManager.show('pause');
		}
	}

	resize(w, h) {
		// this._bg1.position.x = w/2;
		// this._bg1.position.y = h/2;
		this.x = w/2;
		this.y = h/2;
		// console.log('Resizing Settings screen:', w, h);
	}
}

export default SettingsScreen;
