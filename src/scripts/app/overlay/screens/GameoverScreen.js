import * as PIXI from 'pixi.js';

import IconButton      from 'buttons/IconButton';
import Cache           from 'fido/loader/Cache';
import LabelButton     from 'fido/ui/buttons/LabelButton';
import { GEL_TITLE_PADDING_TOP } from 'Const';

export default class PauseScreen extends PIXI.Container {
	constructor(app) {
		super();
		this.app = app;

		// this.bg = new PIXI.Sprite.fromFrame('overlay_box.png');//
		// this.bg.anchor.set(0.5);
		// this.addChild(this.bg);

		this.bg = new PIXI.Graphics();
		this.bg.beginFill(0x7a7a7a);
		this.bg.drawRect(0, 0, 300, 200);
		// this.bg.anchor.set(0.5);
		this.addChild(this.bg);

		this.hasClose = false;


		this.title = new PIXI.Text('WHOOPS !',{font : '60px helvetica', fill: 'white'});
		this.title.position.y = this.bg.position.y - this.bg.height/2 + GEL_TITLE_PADDING_TOP;
		this.title.anchor.x = 0.5;

		this.title.y = -this.bg.height * 0.5 + 5;

		this.addChild(this.title);
		//
		// this.playButton = IconButton.big('icon_play.png');
		// this.playButton.onPress.add(this.onPress, this);
		// this.playButton.view.x = 90;
		// this.playButton.view.y = -28;
		// this.addChild(this.playButton.view);

		this.restartButton = IconButton.primaryBig('icon_restart.png');
		this.restartButton.onPress.add(this.onPress, this);
		this.restartButton.view.x = 0;
		this.restartButton.view.y = 0;
		this.addChild(this.restartButton.view);


	}
	onPress(bt)
	{
		if(bt === this.restartButton)
			{
					// restart!
			this.app.gameScreen.game.reset();
			this.app.overlayManager.hide();
		}
// 		else  if(bt === this.playButton)
// 			{
// //            this.app.overlayManager.hide();
// //          this.app.gameScreen.game.resume();
//
//
// 			this.app.gameScreen.game.reset();
// 			this.app.overlayManager.hide();
// 		}

	}
	init() {
		console.log('init function');
	}
	onShow() {
		this.app.topMenu.setState('gameover');
	}
	onHide() {
		this.app.topMenu.setState(this.app.topMenu.previousState);

	}

	resize(w, h)
	{
		this.w = w;
		this.h = h;
		this.x = w/2;
		this.y = h/2;

		this.title.position.y = -this.h/2 + GEL_TITLE_PADDING_TOP;


		this.bg.width = this.w/1.4;
		this.bg.height = this.h/1.7;
		this.bg.position.x = -this.bg.width/2;
		this.bg.position.y = -this.bg.height/2;
	}
}
