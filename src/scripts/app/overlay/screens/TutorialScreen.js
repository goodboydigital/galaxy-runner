import * as PIXI from 'pixi.js';


import SoundManager from 'fido/sound/SoundManager';
import IconButton from 'buttons/IconButton';
import Ticker from 'fido/system/Ticker';
import Carousel from 'fido/ui/Carousel';
import Dots from 'fido/ui/Dots';
import Device from 'fido/system/Device';

import { GEL_PADDING_LEFT, GEL_PADDING_RIGHT, GEL_PADDING_TOP, GEL_PADDING_BETWEEN_BUTTON, GEL_TITLE_PADDING_TOP } from 'Const';

export default class TutorialScreen extends PIXI.Container {
	constructor(app) {
        // super(app,'title_how_to_play.png');
		super();

		this.app = app;
		this.hasClose = false;
		this.panels = [];
		window.tuto = this;


	}
	init() {

		if(this.inited)return;
		this.inited = true;
		const thumbs =
[
	{
		thumb_desktop:'how-to-1.jpg',
		thumb_mobile:'how-to-1.jpg',
	},
	{
		thumb_desktop:'how-to-2.jpg',
		thumb_mobile:'how-to-2.jpg',
	},
	{
		thumb_desktop:'how-to-3.jpg',
		thumb_mobile:'how-to-3.jpg',
	},
].map(function(item){

	if(Device.instance.desktop)
            {
		item.thumb = item.thumb_desktop;
		item.copy = item.copy_desktop;
	}
	else
            {
		item.thumb = item.thumb_mobile;
		item.copy = item.copy_mobile;
	}

	return item;
});

		if(Device.instance.desktop){
			//
		}
		else {
			//
		}



		for (let i = 0; i < thumbs.length; i++)
        {
			const data = thumbs[i];

			const panel = new PIXI.Container();

			const sprite = PIXI.Sprite.from(data.thumb);
			panel.addChild(sprite);

			sprite.anchor.set(0, 0.5);
			panel.y = 150;
			panel.pivot.x = -105;



			this.panels.push(panel);
		}

        // create a carousel..
		const carouselOptions = {
			itemWidth : 650,
			padding : 160,
			useMask : true,
			bounds : new PIXI.Rectangle(-300, 0, 600, 300),
			dots : false,
		};

		this.carousel = new Carousel(this.panels, carouselOptions);
		this.carousel.y = -146;

		// this.bg = new PIXI.Sprite.from('overlay_box.png');
		this.bg = new PIXI.Graphics();
		this.bg.beginFill(0x7a7a7a);
		this.bg.drawRect(0, 0, 300, 200);
		// this.bg.anchor.set(0.5);
		this.addChild(this.bg);

		this.addChild(this.carousel);

		// const dotTexture = PIXI.Texture.from('pagination_off.png');

        // add some dots to it!
        // const dotOptions = {
        //     size : this.panels.length,
        //     step : 50,
        //     padding : 25,
        //     interactive : true,
        //     alpha : 0.4,
        //     texture : dotTexture
        // };

        // this.dots = new Dots(dotOptions);
        // this.addChild(this.dots);
        // this.dots.x = 0
        // this.dots.y = 215;
        // this.dots.scale.x = 0.9;


		// this.title = new PIXI.extras.BitmapText('HOW TO play JOJO', {align:'center', font:'38px Londrina Solid'});
		this.title = new PIXI.Text('How to play',{font : '60px helvetica', fill: 'white'});

		this.title.anchor.x = 0.5;
		this.title.position.y = this.bg.position.y - this.bg.height/2 + GEL_TITLE_PADDING_TOP;
		this.addChild(this.title);


		this.leftArrow = IconButton.tertiary('icon_prev.png', 'previous');
		// this.leftArrow.view.tabIndex = 25;

		this.leftArrow.talk = 'dm_interface_elements_vo_narrator_back_t1';
		this.addChild(this.leftArrow.view);

		this.rightArrow = IconButton.tertiary('icon_next.png', 'next');
		this.rightArrow.talk = 'dm_interface_elements_vo_narrator_next_t1';
		this.addChild(this.rightArrow.view);
		// this.rightArrow.view.tabIndex = 26;

		this.leftArrow.view.x =  315-75;
		this.leftArrow.view.y = 0;

		this.rightArrow.view.x = 315-75;
		this.rightArrow.view.y = 0;

        // this.carousel.setDots(this.dots);
		this.carousel.setButtons(this.leftArrow, this.rightArrow);

        // this.backButton = IconButton.small("main_button_next.png");
        // this.addChild(this.backButton.view);
        // this.backButton.view.tabIndex = 9;
        // this.backButton.view.x =  -this.app.safeSize.width/2 + 82;
        // this.backButton.view.y =  -this.app.safeSize.height/2 + 82;




	}
	update() {

	}
	onNextPressed() {
		this.trackpad.nextSlot();
		this.currentPage++;
		this.pageTwoTimeout = Math.round(new Date().getTime() / 1000);
		Ticker.instance.add(this.update, this);
	}
	onPreviousPressed() {
		this.trackpad.previousSlot();
		this.currentPage--;
		Ticker.instance.remove(this.update, this);
	}
	onShow() {
		const delay = 0.1;
//        this.screenManager.app.topMenu.pauseMode();
		this.init();


		// this.screenManager.app.topMenu.pauseMode();
		this.screenManager.app.topMenu.setState('tutorial');

		// clement's idea !
		this.app.topMenu.buttons.back.onPress.add(() => {
			// this.screenManager.app.topMenu.hideBackButton();
			if (this.app.screenManager.currentScreen.game ) {
				// this.screenManager.app.topMenu.setState('tutorial');
				this.app.overlayManager.gotoScreenByID('pause');
				// this.screenManager.app.topMenu.showHomeButton();

			} else if (this.app.screenManager.currentScreen.id === 'title'){
				this.app.overlayManager.hide();
			} else {
				// this.screenManager.app.topMenu.pauseMode();
				// this.screenManager.app.topMenu.showHomeButton();
				this.app.overlayManager.gotoScreenByID('pause');
			}
		});
		//        this.screenManager.app.topMenu.
		// for(var i = 0; i < this.pages[this.currentPage].length; i++)
		// {
		//     var panel = this.pages[this.currentPage][i];
		//     panel.scale.set(0);
		//     TweenLite.to(panel.scale, 0.5, {
		//         x : 1,
		//         y : 1,
		//         delay : i * delay,
		//         ease : Bounce.easeOut
		//     });
		// }

	}

	onHide() {
		this.screenManager.app.topMenu.setState(this.screenManager.app.topMenu.previousState);
	}

	onPress(bt) {

	}
	resize(w,h) {
		this.w = w;
		this.h = h;
		this.x = w/2;
		this.y = h/2;

		this.title.position.y = -this.h/2 + GEL_TITLE_PADDING_TOP;

		this.bg.width = this.w/1.4;
		this.bg.height = this.h/1.7;

		this.carousel.y = -this.bg.height/2 + 34;


		this.bg.position.x = -this.bg.width/2;
		this.bg.position.y = -this.bg.height/2;

		this.leftArrow.view.position.x = -w/2 + this.leftArrow.view.width/2 + GEL_PADDING_LEFT;
		this.rightArrow.view.position.x = w/2 - this.rightArrow.view.width/2 - GEL_PADDING_RIGHT;
	}

}
