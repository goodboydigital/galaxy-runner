import * as PIXI from 'pixi.js';
// TODO update path
import IconButton      from 'buttons/IconButton';
import Shop            from 'app/Shop';
import Cache           from 'fido/loader/Cache';

export default class ShopScreen extends PIXI.Container {
	constructor(app) {
		super();
		this.app = app;


		this.shop = new Shop(app);

		this.addChild(this.shop);

		this.shop.position.set(-300,-271);

		this.hasClose = false;

		this.closeButton = IconButton.close('icon_close.png');

		this.closeButton.onPress.add(this.close, this);
		this.closeButton.view.x = 140;
		this.closeButton.view.y = -271 + 27;

		this.addChild(this.closeButton.view);

	}
	onShow()
	{
		this.shop.onNeedsUpdate();
	}

	close()
	{
		this.app.overlayManager.goBack();
	}

	onShown(){
			//TweenLite.to(this.shop.scale, 0.4, {x:1, y:1, ease:Elastic.easeOut, delay:0.2});
	}

	onHidden() {
		this.shop.hideViewPanel();
	}

	resize(w, h)
	{
		this.w = w;
		this.h = h;
		this.x = w/2;
		this.y = h/2;
	}

}
