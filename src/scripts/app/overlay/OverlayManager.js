import * as PIXI from 'pixi.js';
import Signal              from 'signals';
import ScreenManager       from 'fido/app/ScreenManager';
import IconButton          from 'buttons/IconButton';
import PauseScreen         from './screens/PauseScreen';
import GameoverScreen      from './screens/GameoverScreen';
import TutorialScreen      from './screens/TutorialScreen';
import SettingsScreen      from './screens/SettingsScreen';


export default class OverlayManager extends ScreenManager {
	constructor(app) {
		super();
		this.app = app;
		this.view = new PIXI.Container();

		this.pauseScreen        = new PauseScreen(app);
		this.gameoverScreen     = new GameoverScreen(app);
		this.tutorialScreen     = new TutorialScreen(app);
		this.settingsScreen     = new SettingsScreen(app);

		this.addScreen(this.pauseScreen,    'pause');
		this.addScreen(this.gameoverScreen, 'gameover');
		this.addScreen(this.tutorialScreen, 'tutorial');
		this.addScreen(this.settingsScreen, 'settings');


		this.addScreen(new PIXI.Container(), 'empty');

		this.onShow = new Signal();
		this.onHide = new Signal();

		this.black = new PIXI.Graphics().beginFill(0x000000).drawRect(0, 0, 100, 100);
		this.black.alpha = 0.5;
		this.black.interactive = true;
		this.black.hitArea = new PIXI.Rectangle(0, 0, 10000, 10000);

		this.view.visible = false;

		this.closeButton = IconButton.close('icon_close.png');

		this.closeButton.onPress.add(this.goBack, this);
		this.closeButton.view.x = 10000;
		this.closeButton.view.y = 120;

		// this.view.addChild(this.black);
		this.view.addChild(this.container);
		this.view.addChild(this.closeButton.view);

		this.app.screenManager.container.filterArea = new PIXI.Rectangle(0, 0, 10000, 10000);

		setTimeout(()=> {
			// this.show('settings');
		}, 2000);

	}
	goBack()
	{
		this.history.pop();
		var prev = this.history.pop();

		if(prev)
			{
			this.gotoScreen(prev);
		}
		else
			{
			this.hide();
		}
	}

	show(screenId, params)
	{
		this.history = [];
		this.gotoScreenByID(screenId, params, true);
		this.onShow.dispatch();
		this.view.visible = true;
		this.view.alpha = 0;
		TweenLite.to(this.view, 0.3, {
			alpha : 1
		});
		this.closeButton.view.scale.set(0);
	}

	gotoScreen(screen, params, instant, forceRefresh)
	{

		if(screen.hasClose === undefined || screen.hasClose === true)
			{
			this.closeButton.view.interactive = true;

			TweenLite.to(this.closeButton.view.scale, 0.3, {
				x : this.closeButton.defaultScale,
				y : this.closeButton.defaultScale,
				ease : Back.easeOut,
				delay : 0.4
			});
		}
		else
			{
			this.closeButton.view.interactive = false;
			TweenLite.to(this.closeButton.view.scale, 0.3, {
				x : 0,
				y : 0,
				ease : Back.easeOut
			});
		}

		ScreenManager.prototype.gotoScreen.call(this, screen, params, instant, forceRefresh);
	}

	hide()
	{
		this.history = [];
		this.closeButton.view.interactive = false;
		this.gotoScreenByID('empty');

		TweenLite.to(this.closeButton.view.scale, 0.3, {
			x : 0,
			y : 0,
			ease : Back.easeOut
		});

		TweenLite.to(this.view, 0.3, {
			alpha : 0,
			onComplete : function()
					{
				this.view.visible = false;
				this.onHide.dispatch();
			}.bind(this)
		});
	}

	resize(w, h)
	{
		this.closeButton.view.x = w/2 + 320 - 5;
		this.closeButton.view.y = h/2 - 180 - 12;
		this.black.scale.set(w / 100, h / 100);
		ScreenManager.prototype.resize.call(this, w, h);
	}

	onFadeout()
	{
		if(this.currentScreen)
			{
			if(this.currentScreen.onHidden)this.currentScreen.onHidden();
			this.container.removeChild(this.currentScreen);
		}

		this.currentScreen = this.nextScreen;
		this.currentScreen.alpha = 0;

		if(this.currentScreen.onShow)this.currentScreen.onShow();
		if(this.currentScreen.resize)this.currentScreen.resize(this.w, this.h);

		TweenLite.to(this.currentScreen, 0.4, {alpha:1, onComplete:this.onFadein.bind(this)});

		this.container.addChild(this.currentScreen);
	}
}
