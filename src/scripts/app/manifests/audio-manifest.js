// audio-assets-template.js

const assets = [
	'audio/crash.mp3',
	'audio/pickup.mp3',
	'audio/soundtrack.mp3',
	'audio/woosh.mp3',
	'audio/buttons/game_button.mp3',
	'audio/buttons/press.mp3',
	'audio/buttons/roll.mp3'
];

export default assets;
