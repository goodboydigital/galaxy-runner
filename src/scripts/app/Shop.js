import * as PIXI from 'pixi.js';
import IconButton     from 'buttons/IconButton';
import BuyButton      from 'buttons/BuyButton';

import Item           from 'fido/shop/Item';
import SimpleBouncer  from 'fido/physics/SimpleBouncer';
import Cache          from 'fido/loader/Cache';

export default class Shop extends PIXI.Container{
	constructor(app) {
		super();
		this.app = app;
		this.slotPanel = new PIXI.Container();

		this.viewPanel = new PIXI.Container();
		this.viewPanel.y = 60;
		this.viewPanel.x = 30;

		this.gridContainer = new PIXI.Container();

		this.system = this.app.shopSystem;

		this.bouncer = new SimpleBouncer();
		this.bouncer.max = 5;

		this.currentItem = null;

		// When something happens in the system
		// (a change in the amount of money), we need to update the view aka this screen
		this.system.onChange.add(this.onNeedsUpdate,this);


		this.leftPanelWidth = 466;
		this.leftPanelHeight = 546;

		this.sign = new PIXI.Text('Shop',{font : '60px Times New Roman'});

		this.slotBg = new PIXI.Sprite.fromFrame('shop_main_panel.png');

		this.slotPanel.addChild(this.slotBg);

		this.slotPanel.addChild(this.sign);

		this.sign.anchor.x = 0.5;

		this.sign.position.set(this.leftPanelWidth * 0.5, 0);

		this.addChild(this.viewPanel);
		this.addChild(this.slotPanel);


		this.coin = new PIXI.Sprite.fromFrame('coin_symbol.png');
		this.coin.x = 20;
		this.coin.y = this.leftPanelHeight - 80;
		this.bouncer.x = this.coin.y;
		this.bouncer.floor = this.coin.y;
		this.slotPanel.addChild(this.coin);

		this.wallet = new PIXI.Text('',{font : 'bold 30px arial'});
		this.wallet.x = 80;
		this.wallet.anchor.y = 0.5;
		this.wallet.y = this.leftPanelHeight - 80 + this.coin.height * 0.5;
		this.slotPanel.addChild(this.wallet);

		this.createItems();
		this.createViewPanel();

		return this;

	}
	createItems() {

		this.slotPanel.addChild(this.gridContainer);

		this.gridContainer.x = 60;
		this.gridContainer.y = 90;

		var json = Cache.getJson('shopData');

		this.system.loadFromJson(json);

		for (var j = 0; j < this.system.getNumGroups(); j++) {

			var group = this.system.getGroupAt(j);

			for (var i = 0; i < group.getNumItems(); i++) {

				var item = group.getItemAt(i);

				item.view.x = 92 * i;
				item.view.y = 90 * j;

				item.view.onPress.add(this.onItemPressed,this);

				this.gridContainer.addChild(item.view);
			}
		}

		this.updateFromSave();

	}

	updateFromSave() {

		this.system.updateFromSave();


	}

	createViewPanel() {

		this.viewBkg = new PIXI.Sprite.fromFrame('item_panel.png');
		this.viewPanel.addChild(this.viewBkg);

		this.buyButton = new BuyButton();
		this.buyButton.view.x = 240;
		this.buyButton.view.y = 320;
		this.buyButton.view.anchor.set(0.5);
		this.buyButton.setInteractive(false);

		this.previewImage = new PIXI.Sprite.fromFrame('item_diamond_1.png');
		this.previewImage.x = 240;
		this.previewImage.y = 30;

		this.description = new PIXI.Text('Description yeah',{});
		this.description.x = 240;
		this.description.anchor.x = 0.5;
		this.description.y = 220;

		this.viewPanel.addChild(this.description);

		this.viewPanel.addChild(this.previewImage);
		this.viewPanel.addChild(this.buyButton.view);

		this.buyButton.onPress.add(this.onBuyButtonPressed,this);
	}

	hideViewPanel() {

		TweenLite.to(this.viewPanel,0.4,{x : 0});
	}

	onItemPressed(btn) {

		TweenLite.to(this.viewPanel,0.6,
			{
				x : 360,
				ease : Quart.easeOut,
				onComplete : function () {
					this.buyButton.setInteractive(true);
				},
				onCompleteScope : this
			});

		this.currentItem = btn.item;

		this.updateViewPanel();
	}

	updateViewPanel() {

		this.previewImage.texture = PIXI.Texture.fromFrame(this.currentItem.imageUrl);
		this.previewImage.anchor.x = 0.5;

		this.description.scale.set(1);
		this.description.text = this.currentItem.description;

		var newScale = 240 / this.description.width;
		newScale = Math.min(1,newScale);

		this.description.scale.set(newScale);

		if(!this.system.canBuyItem(this.currentItem))
			{
			console.log('heya');
			this.buyButton.greyScale();
		}
		else{
			this.buyButton.colourMode();
		}

		this.buyButton.setPrice(this.currentItem.price);
	}

	onBuyButtonPressed(btn) {

		var item = this.currentItem;

		if(this.system.buyItem(item))
			{
			this.buyButton.setInteractive(false);
		}
		else{
			this.bouncer.bounce = 0.9;
			this.bouncer.x -= 15;
		}
	}

	updateTransform() {

		this.bouncer.update();

		this.coin.y = this.bouncer.x;

		PIXI.Container.prototype.updateTransform.call(this);

	}

	onNeedsUpdate() {

		this.updateWallet();
	}

	updateWallet()
	{
		this.wallet.text = this.system.getMoney();
	}
}
