// SettingsManager.js
// TODO gmi stuff
import Signal from 'signals';
import getGMI from 'gmi';
let gmi = getGMI();
class DynamicProperty {
	constructor(value, prop, func) {
		this.onToggle = new Signal();
		if(!gmi) gmi = getGMI();
		console.log(gmi);

		if(func)
		{
			// this is to cover the 3 hardcoded properties in the site
			this.enabled = gmi.getAllSettings()[prop];
		}
		else
		{
			// if cookies are disabled
			if(gmi.getAllSettings().gameData)
			{
				this.enabled = gmi.getAllSettings().gameData[prop];
			}
			else
			{
				this.enabled = value;
			}
		}
		this.func = func;
		this.prop = prop;

	}
	enable()
	{
		if(this.enabled) return;

		this.update(true);
	}

	disable()
	{
		if(!this.enabled) return;

		this.update(false);
	}

	update(bool)
	{
		this.enabled = bool;

		if(this.func)
		{
			if(this.prop === 'muted')
			{
				gmi.setMuted(bool);
			}
			else if(this.prop === 'motion')
			{
				gmi.setMotion(bool);

				window.motion = bool ? 1 : 0;
			}
		}
		else
		{
			gmi.setGameData(this.prop, bool);
		}

		this.onToggle.dispatch(bool);
	}
}


module.exports = {
	// movement:new DynamicProperty(true, 'motion',  gmi.setMotion),
	// flipped:new DynamicProperty(false, 'flipped'),
	// muted:new DynamicProperty(true, 'muted', gmi.setMuted),
	audio:new DynamicProperty(true, 'audio')

};
