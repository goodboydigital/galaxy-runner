import LocalStorage    from 'fido/system/LocalStorage';
import Signal          from 'signals';


const GameSave = function()
{
	this.levelStates = [1, 0, 0, 0, 0, 0, 0];
	this.levelBestScores = [0, 0, 0, 0, 0, 0, 0];
	this.tutorials = [0, 0, 0, 0, 0, 0, 0];
	this.totalScore = 0;
};


class GameSaveManager {
	constructor() {
		this.onLoaded = new Signal();
		this.localStorage = new LocalStorage('com.oneeyedrhino.filmclub');
		this.save  = null;
	}
	loadGame()
	{
		this.loadGameFromLocalStorage();
	}

	loadGameFromLocalStorage()
	{
		var save = this.localStorage.getObject('save');

		if(!save)
			{
			save = new GameSave();
			this.localStorage.storeObject('save', save);
		}

		this.save  = save;
		this.onLoaded.dispatch();
	}

	isPB(levelId, score)
	{
		var bestScore = this.save.levelBestScores[levelId];
		return (score > bestScore);
	}

	getPB(levelId)
	{
		var bestScore = this.save.levelBestScores[levelId];
		return bestScore;
	}

	unlockLevel(levelId, levelTarget )
	{
		if(this.save.levelBestScores[levelId-1] >= levelTarget)
			{
			if(this.save.levelStates[levelId] === 0)
					{
				this.save.levelStates[levelId] = 1;
				this.saveToStorage();
				return true;
			}

			return true;
		}

		return false;
	}

	saveScore(levelId, score)
	{
		var bestScore = this.save.levelBestScores[levelId];

		this.save.totalScore = this.save.totalScore || 0;

		this.save.totalScore += score;

		if(score > bestScore)
			{
					// now check and save the BEST SCORE //
			for (var i = 0; i < this.save.levelBestScores.length; i++) {
				var best = this.save.levelBestScores[i];
				if(score > best)
							{
									// new high score!
					if(window.Highscores)
									{
						if(window.UserDetails.isUserSignedIn())
											{
							window.Highscores.setUserScore(function(data){console.log('SCORES SAVED', data);},
																			function(data){console.log('SCORES FAILED TO LOAD', data);},
																			['allatbreak_overall'], [score]);


						}
					}
				}
			}

			this.save.levelBestScores[levelId] = score;
		}

		if(window.Highscores)
			{
			if(window.UserDetails.isUserSignedIn())
					{

				window.Highscores.setUserScore(function(data){console.log('SAVING TOTAL SCORE SAVED', data);},
																			function(data){console.log('TOTAL SCORES FAILED TO LOAD', data);},
																			['allatbreak_total'], [this.save.totalScore]);
			}
		}

		this.saveToStorage();

	}

	saveToStorage()
	{
		if(window.Data)
			{
			this.localStorage.storeObject('save', this.save);
		}
	}
}


GameSaveManager.instance = new GameSaveManager();

module.exports = GameSaveManager;
