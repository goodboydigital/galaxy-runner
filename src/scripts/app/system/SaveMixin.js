// SaveMixin.js

const Cache = require('fido/loader/Cache');

const SaveMixin = function(target) {

	target.addItemInventory = function(item)
	{
		this.object.inventory.push(item);

		return this.object.inventory;
	};

	target.removeItemInventory = function(item)
	{
		for (var i = 0; i < this.object.inventory.length; i++) {
			var s = this.object.inventory[i];
			if(s === item){
				this.object.inventory.splice(i, 1);
				break;
			}
		}

		return this.object.inventory;
	};

	target.setMission = function(mIndex) {
		this.object.mission = mIndex;
	};

	target.setCredits = function(mValue) {
		console.log(this);
		console.log('Setting Credit : ', mValue, this.object.credits);
		this.object.credits = mValue;
	};
};

export default SaveMixin;
