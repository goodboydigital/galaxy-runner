// Save.js

import LocalStorage from 'fido/system/LocalStorage';
import getGMI from 'gmi';

let gmi;


class Save {
	constructor(bundleId) {
		this.debug = true;
		this.waiting = false;

		if(!gmi) gmi = getGMI();

		this.lc = new LocalStorage(bundleId || 'com.goodboydigitalrc6');
	}


	load(object, key)
	{
		key = key || 'game-save-cleany-rc6';

		this.original = JSON.stringify(object);
		console.log('------------------------here');
		//	var save = this.lc.getObject(key);
		let save;

		if(gmi.getAllSettings().gameData)
		{
			save = gmi.getAllSettings().gameData[key];
		}

		if(!save)
		{

			if(gmi.getAllSettings().gameData)
			{
				gmi.setGameData(key, object);
				save = gmi.getAllSettings().gameData[key];
			}
			else
			{
				save = object;
			}
		}

		this.key = key;
		this.object = object;

		// compare structure..
		this.map(object, save);

		const dynamic = this.collectDynamicProperties(object, []);

		this.createGetterSetters(dynamic);

		if(this.debug)
		{
			console.log(object);
		}

	}


	reset()
	{
		const original = JSON.parse(this.original);

		this.map(this.object, original);
	}

	map(object, savedObject)
	{
		for (let i in object)
		{
			const child = object[i];

			if(savedObject[i] == undefined)
			{
				savedObject[i] = child;
			}
			else
			{
				if(child instanceof Array || child instanceof Object)
				{
					this.map(child, savedObject[i]);
				}
			}

			if(i.substr(0, 2) === '__')
			{
				// dynamic save..
				object[i] = savedObject[i];
			}
		}
	}

	collectDynamicProperties(object, list)
	{
		for (let i in object)
		{
			const child = object[i];

			if(child instanceof Array || child instanceof Object)
			{
				this.collectDynamicProperties(child, list);
			}

			if(i.substr(0, 2) === '__')
			{
				list.push({object:object, id:i});
			}
		}

		return list;
	}

	createGetterSetters(list)
	{
		const waitAndSave = this.waitAndSave.bind(this);

		list.forEach(function(data){
			Object.defineProperty(data.object, data.id.substr(2), {
				get: function(){
					return this[data.id];
				},
				set: function(val){
					if(this[data.id] !== val)
					{
						this[data.id] = val;
						waitAndSave();
					}
				}
			});

		});
	}

	waitAndSave()
	{
		if(this.waiting)return;

		this.waiting = true;
		// setTimeout(function(){
		this.waiting = false;
		this.save();
		// }.bind(this), 100);

	}

	save()
	{
		if(this.debug)
		{
			// console.log("Data saved");
		}

		//this.lc.storeObject(this.key, this.object);
		if(gmi.getAllSettings().gameData)
		{
			gmi.setGameData(this.key, this.object);
		}
	}
}


export default Save;
