import Ticker from 'fido/system/Ticker';
import Device from 'fido/system/Device';

/**
 * Manage orientation modes and trigger events
 * @param {String} mode
 */
class Brim
{
    constructor(){
        if(!Device.instance.isMobile || !Device.instance.safari)return;

        if(!Device.instance.webGL)return;

        var spec = this.getSpec();

        if(!spec)return;

        this.activeCache = false;
        this.height = spec[0];

        window.addEventListener('resize', this.onResize.bind(this));

        this.div = document.createElement('div');

        this.div.style.backgroundColor = '#000000';
        this.div.style.position = 'absolute';
        this.div.style.top = '0';
        this.div.style.left = '0';
        this.div.style.width = '100%';
        this.div.style.height = '9999999999999999px';

        this.image = new Image();
        // this.image.src = window.ASSET_URL + 'img/device/brim.jpg';
        this.image.style.position = 'absolute';
        this.image.style.top = '-40px';
        this.image.style.zIndex = '1000';

        this.image.width = 500;
        this.div.appendChild(this.image);

        this.brimMode = true;

        this.p = document.createElement('div');
        this.p.innerHTML = 'oops! swipe up to keep playing!';


        this.p.style.position = 'absolute';
        this.p.style.top = '100px';
        this.p.style.left = 0;
        this.p.style.width = '100%';

        this.p.style.fontSize = '1.5rem';
        this.p.style.fontFamily = 'Exo2, sans-serif';
        this.p.style.color = 'white';
        this.p.style.textAlign = 'center';
        this.p.style.position = 'absolute';
        this.p.style.padding = 0;


//        this.div.appendChild(this.p);

        document.body.appendChild(this.div);


        (function () {
            var firstMove;

            document.addEventListener('touchstart', function (e) {

                if(this.brimMode)
            {

                    firstMove = true;
                    e.preventDefault();
                    e.returnValue = false;
                }
            }, false);

            document.addEventListener('touchmove', function (e) {

                if(this.brimMode)
            {
                    if (firstMove)
                {
                        e.preventDefault();
                        e.returnValue = false;
                    }

                    firstMove = false;
                }
            }, false);

        } ());

        this.onResize();
    }

    reset(img)
    {
      if(this.image)
      {
        this.image.src = img;
      }
    }

    getSpec()
{
        var specs = [
        [320, 480, 2, 'iPhone 4'],
        [320, 568, 2, 'iPhone 5 or 5s'],
        [375, 667, 2, 'iPhone 6'],
        [414, 736, 3, 'iPhone 6 plus']
        ];

        let goodspec = null;

        for (var i = 0; i < specs.length; i++)
    {
            var spec = specs[i];

            var match =  ((window.screen.height == (spec[0]) && window.screen.width  == (spec[1]) ) || (window.screen.width  == (spec[0]) && window.screen.height == (spec[1]) ) );


            if(match){

                goodspec = spec;
                break;
            }
        }

        return goodspec;//[320, 480, 2, 'iPhone 4'];
    }

    showBrim()
{
        this.div.style.visibility = 'visible';
        this.div.style.height = '9999999999999999px';
//     this.div.style.display = "block";
        if(Ticker.game)
    {
            this.activeCache = Ticker.game.active;
        }

        Ticker.game.stop();
    }


    showGame()
{
   //]

        this.div.style.visibility = 'hidden';

        if(Ticker.game)
    {
            if(this.activeCache)
        {
                Ticker.game.start();
            }
        }

    // the +1 is to stop the bars from appearing on ios9
        this.div.style.height = this.height + 1 + 'px';
        window.scrollTo(0, 0);
    }

    onResize()
{
        if( (window.innerHeight > window.innerWidth) || (window.innerHeight === this.height) )
    {
            this.showGame();
        }
        else
    {
            this.showBrim();
        }

        this.image.width = window.innerWidth;

        window.scrollTo(0, 0);
    }
}

export default new Brim();
